This is the entry course for Java developers who need to develop web applications. 
This course teaches you the needed foundations before learning web framework such as JSF, Spring MVC, Struts (1 & 2), Wicket or even Vaadin.

You learn the basics of HTTP and a minimal amount of HTML. 
Then we start with server-side web programming.

During the course, you write Java servlets (which are very simple to learn) in order to practice the notions of request, response, parameter, session, filter, forward, redirect, navigation, deployment, etc.

In a real application you will probably configure the servlet provided by a framework instead of writing one yourself, and you will still need to understand the notions around (request, response, session,...) covered in this course.

During the course of your training, you develop a little book shop application, with a catalog and a shopping cart.

To follow this course, students need solid Java foundations, for example, the Java & OO Fundamentals, then the Java & OO Intermediate courses and exams.

After this Web Java Fundamentals course, students typically learn a web framework as JSF, Struts or Spring MVC.
