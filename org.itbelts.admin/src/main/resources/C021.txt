JavaScript is the scripting language of the web. It is used in millions of pages to provide dynamic effects, client side
functionalities like validation, cookie management, browser detection and server calls through Ajax.
With the new version it has also added inline support for Java (provided its installed on the client machine).

This course will teach you about
<ul>
<li>Lexical structure
<li>Data types
<li>Variables
<li>Operators
<li>Flow Control
<li>Functions
<li>Built-in Obbjects
<li>Event handling
</ul>

Afterwards you are ready for the Javascript exam.
