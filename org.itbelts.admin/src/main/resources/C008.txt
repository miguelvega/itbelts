Did you ever wonder why to use JavaServer page (JSP)?
JSP has been added as a companion to the Servlet specification for a better separation of the part of code that
handles your business logic from the code that builds the UI.
JSPs are similar to HTML files, but provide the ability to display dynamic content within web pages.
JSP technology was developed to separate the development of dynamic web page content from static HTML page design.
JSP combines platform independence with separation of concerns between HTML and Java development.
