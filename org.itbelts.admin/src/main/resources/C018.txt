The GO course will teach you about the Go programming language, developed by Google Inc.
It is an open source project with its first stable version 1 released on 28 March 2012.
Items covered include:
<ul>
<li>Installation instructions
<li>Language basics
<li>Data types
<li>Operators
<li>Formatting and comments
</ul>