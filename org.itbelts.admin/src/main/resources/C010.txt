Welcome to this Java Persistence API course. It will teach you, in a hands-on way, how JPA builds up from SQL and Java's low-level standard libraries to bridge the gap between relational databases and object orientation.

Each chapter has a theory section, points to some online resources for further information and suggests a few practical exercises. We've also created one short exam per chapter to make sure you've assimilated the material.

No prior knowledge of JPA or any other persistence framework is necessary. In fact, we'll walk you through setting up a database and a JPA project with a simple domain model.

If you have not already done so, please create an account on JavaBlackBelt before starting the course. This will allow you to pass the exams and record your progress.

By the end of this course, you should have a solid grasp of what JPA is and why it is useful, how to map your objects to a database using annotations and how to manipulate your persistent objects.

Before starting, feel free to have a look at this video where John Rizzo explains how a BlackBeltFactory course happen.