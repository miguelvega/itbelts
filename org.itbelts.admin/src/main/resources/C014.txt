This course is focused on the internals and advanced features of the selected Vaadin aspects.
After finishing the Vaadin core advanced course you will get deep understanding of the Vaadin internal architecture and data model.
You will also become fluent in laying out the variety of components in order to achieve desired graphical structure of the application.
The last things you will master are the advanced aspects of Vaadin URL mapping.

This course intentionally doesn�t cover topics such as Vaadin themes, GWT widgets, deployment issues and Maven integration.

