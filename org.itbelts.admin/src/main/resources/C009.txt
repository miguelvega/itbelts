This course covers the core part of Spring: dependency injection.

We don't dig into gory details, but instead insist on a strong understanding of the principles, as needed for building a typical business application with Srping. 
We use large and detailed diagrams to help you conceptualize the relationship between Spring's configuration and your object graph.

We cover both annotations and xml configuration.

We also cover the fundamentals of AOP to help understanding the presence of proxies in your Spring application.

To follow this course, students need solid Java foundations, for example, the Java & OO Fundamentals, then the Java & OO Intermediate courses and exams.
