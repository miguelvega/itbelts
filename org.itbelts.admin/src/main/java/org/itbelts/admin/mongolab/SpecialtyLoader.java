package org.itbelts.admin.mongolab;

import java.util.logging.Logger;

import org.itbelts.dao.mongolab.AbstractMongoLabDAO;
import org.itbelts.dao.mongolab.SpecialtyDAOImpl;
import org.itbelts.domain.Specialty;

/**
 * Loader for the set of specialties in the system.
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 19-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      19-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class SpecialtyLoader extends AbstractMongoLabDAO {

    private static final Logger LOGGER = Logger.getLogger( SpecialtyLoader.class.getName() );

    private static final String PATH = "collections/specialties";
    
    /**
     * @param args
     */
    public static void main( String[] args ) throws Exception {
        new SpecialtyLoader().load();
        LOGGER.info( new SpecialtyDAOImpl().getSpecialties().toString() );
    }

    protected void load() {
        sendTo( buildUrl( PATH ), new Specialty( "J", "J01", "Java SE" ) );
        sendTo( buildUrl( PATH ), new Specialty( "J", "J02", "Java EE" ) );
        sendTo( buildUrl( PATH ), new Specialty( "J", "J03", "Spring" ) );
        sendTo( buildUrl( PATH ), new Specialty( "J", "J04", "Persistence" ) );
        sendTo( buildUrl( PATH ), new Specialty( "J", "J05", "Web" ) );
        sendTo( buildUrl( PATH ), new Specialty( "J", "J06", "Building & Code Quality Testing" ) );
        sendTo( buildUrl( PATH ), new Specialty( "J", "J07", "Rich Client" ) );
        sendTo( buildUrl( PATH ), new Specialty( "J", "J08", "Integration & Cross-Cutting" ) );
        
        sendTo( buildUrl( PATH ), new Specialty( "I", "I01", "Scrum" ) );
        sendTo( buildUrl( PATH ), new Specialty( "I", "I02", "Architecture" ) );
        sendTo( buildUrl( PATH ), new Specialty( "I", "I03", "Version Control" ) );
        sendTo( buildUrl( PATH ), new Specialty( "I", "I04", "UNIX" ) );
        sendTo( buildUrl( PATH ), new Specialty( "I", "I05", "Markup Languages" ) );
        sendTo( buildUrl( PATH ), new Specialty( "I", "I06", "Other Languages" ) );
        sendTo( buildUrl( PATH ), new Specialty( "I", "I07", "Corba" ) );

        sendTo( buildUrl( PATH ), new Specialty( "M", "M01", "Mobile" ) );

        sendTo( buildUrl( PATH ), new Specialty( "D", "D01", "Database" ) );

        sendTo( buildUrl( PATH ), new Specialty( ".", ".01", "C#" ) );

        sendTo( buildUrl( PATH ), new Specialty( "U", "U01", "ITBelts Usage" ) );
        
    }

}
