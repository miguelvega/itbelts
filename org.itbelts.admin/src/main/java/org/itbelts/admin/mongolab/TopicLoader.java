package org.itbelts.admin.mongolab;

import java.util.logging.Logger;

import org.itbelts.dao.mongolab.AbstractMongoLabDAO;
import org.itbelts.dao.mongolab.TopicDAOImpl;
import org.itbelts.domain.Topic;

/**
 * Loader for the set of topics in the system.
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 19-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      19-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class TopicLoader extends AbstractMongoLabDAO {

    private static final Logger LOGGER = Logger.getLogger( TopicLoader.class.getName() );

    private static final String PATH = "collections/topics";
    
    /**
     * @param args
     */
    public static void main( String[] args ) throws Exception {
        new TopicLoader().load();
        LOGGER.info( new TopicDAOImpl().getTopics().toString() );
    }

    protected void load() {

        // Specialty "J01"  "Java SE"
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0101", "Java & OO Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0102", "Java & OO Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0103", "Java Advanced" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0104", "SCJP 6" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0105", "Java SE Collections" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0106", "Java SE Concurrency - High-level" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0107", "Java SE Concurrency - Low-level" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0108", "Java SE Base API - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0109", "Java SE Base API - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0110", "Java SE IO" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0111", "Java SE NIO" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0112", "Java SE Networking" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0113", "Java SE Tools" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0114", "Java SE Regular Expressions" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0115", "Java SE Reflection API" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0116", "Java SE Security - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0117", "Java 5 New Language Features" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0118", "Java 5 New Base Library Features" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0119", "Java 5 New Tools and Tool Features" ) );
        sendTo( buildUrl( PATH ), new Topic( "J01", "J0120", "Java 7 New Language Features" ) );

        // new Specialty "J02"  "Java EE" 
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0201", "J2EE Design Patterns" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0202", "EJB 2 - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0203", "EJB 2 - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0204", "EJB 3 - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0205", "EJB 3 - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0206", "EJB 3.1 - Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0207", "EJB 3.1 - Advanced" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0208", "Servlets - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0209", "Servlets 2 - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0210", "Servlets 3" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0211", "Servlets 3 - Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0212", "Servlets 3 - Advanced" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0213", "JSP and Expression Language" ) );
        sendTo( buildUrl( PATH ), new Topic( "J02", "J0214", "JSP - Intermediate" ) );

        
        // Specialty "J03"  "Spring"
        sendTo( buildUrl( PATH ), new Topic( "J03", "J0301", "Spring Core Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "J03", "J0302", "Spring Core" ) );
        sendTo( buildUrl( PATH ), new Topic( "J03", "J0303", "Spring MVC" ) );
        sendTo( buildUrl( PATH ), new Topic( "J03", "J0304", "Spring Velocity" ) );
        sendTo( buildUrl( PATH ), new Topic( "J03", "J0305", "Spring Web Flow" ) );
        sendTo( buildUrl( PATH ), new Topic( "J03", "J0306", "Spring DAO JDBC" ) );
        sendTo( buildUrl( PATH ), new Topic( "J03", "J0307", "Spring Transactions" ) );
        sendTo( buildUrl( PATH ), new Topic( "J03", "J0308", "Spring 3 Certification Mock" ) );
        
        // Specialty "J04"  "Persistence" 
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0401", "JDBC - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0402", "JDBC - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0403", "JDO" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0404", "JPA/Hibernate Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0405", "JPA/Hibernate Lifecycle" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0406", "JPA/Hibernate Mapping" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0407", "JPA" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0408", "Hibernate 3 Core - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0409", "Hibernate 3 Core" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0410", "Hibernate 3 Entity Associations and Relationships" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0411", "Hibernate 3 Inheritance Strategies" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0412", "Hibernate 3 Persistent Objects Retrieval" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0413", "Hibernate 3 Caching" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0414", "Hibernate 3 Advanced Lifecycle" ) );
        sendTo( buildUrl( PATH ), new Topic( "J04", "J0415", "Hibernate 3 Transcation & Concurrency" ) );

        // Specialty "J05"  "Web"
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0501", "Vaadin Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0502", "Vaadin Core Advanced" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0503", "Vaadin UI Components" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0504", "Struts 1 - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0505", "Struts 1 - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0506", "Struts 2 - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0507", "Struts Tiles" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0508", "Web Services - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0509", "JSF 1.1 - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0510", "Seam - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0511", "GWT - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0512", "Tapestry 4 - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0513", "Portlet - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0514", "ZK Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "J05", "J0515", "ADF Business Components" ) );
        
        // Specialty "J06"  "Building & Code Quality Testing"
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0601", "EasyMock" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0602", "Mockito Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0603", "Ant - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0604", "JUnit 3 - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0605", "JUnit 3 - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0606", "JUnit 4" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0607", "Maven 2 Standard" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0608", "Maven 2 J2EE" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0609", "Maven 2 Developer" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0610", "PMD - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0611", "TestNG - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0612", "Code Coverage" ) );
        sendTo( buildUrl( PATH ), new Topic( "J06", "J0613", "Continuous Integration" ) );
        
        // Specialty "J07"  "Rich Client"
        sendTo( buildUrl( PATH ), new Topic( "J07", "J0701", "Swing - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J07", "J0702", "Swing Layout Managers" ) );
        sendTo( buildUrl( PATH ), new Topic( "J07", "J0703", "JGoodies Forms" ) );
        sendTo( buildUrl( PATH ), new Topic( "J07", "J0704", "JGoodies Bindings" ) );
        sendTo( buildUrl( PATH ), new Topic( "J07", "J0705", "Eclipse RCP - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J07", "J0706", "Java 3D - Basic" ) );
        
        // Specialty "J08"  "Integration & Cross-Cutting"
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0801", "JMS" ) );
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0802", "Log4J" ) );
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0803", "Velocity - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0804", "XML Programming - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0805", "OSGI - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0806", "JAX-B" ) );
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0807", "AspectJ - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0808", "Apache Camel - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "J08", "J0809", "JavaMail - Basic" ) );

        
        // Specialty "I01"  "Scrum"
        sendTo( buildUrl( PATH ), new Topic( "I01", "I0101", "Scrum Fundamentals" ) );
        
        // Specialty "I02"  "Architecture" 
        sendTo( buildUrl( PATH ), new Topic( "I02", "I0201", "Programming Aptitude" ) );
        sendTo( buildUrl( PATH ), new Topic( "I02", "I0202", "Algorithms - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "I02", "I0203", "OO for Java - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "I02", "I0204", "OO - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "I02", "I0205", "Design Patterns (GoF)" ) );
        sendTo( buildUrl( PATH ), new Topic( "I02", "I0206", "UML for Java - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "I02", "I0207", "Refactoring" ) );
        
        // Specialty "I03"  "Version Control"
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0301", "SVN - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0302", "SVN - Administration" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0303", "SVN - Usage - Command Line" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0304", "SVN - Usage - Eclipse SubVersive" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0305", "SVN - Usage - TortoiseSVN" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0306", "CVS - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0307", "CVS - Usage - Command Line" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0308", "CVS - Usage - Eclipse Plugin" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0309", "CVS - Usage - TortoiseCVS" ) );
        sendTo( buildUrl( PATH ), new Topic( "I03", "I0310", "CVS - Usage - WinCVS" ) );
        
        // Specialty "I04"  "UNIX"
        sendTo( buildUrl( PATH ), new Topic( "I04", "I0401", "Unix and Shell Scripting" ) );
        sendTo( buildUrl( PATH ), new Topic( "I04", "I0402", "Unix Administration" ) );
        
        // Specialty "I05"  "Markup Languages"
        sendTo( buildUrl( PATH ), new Topic( "I05", "I0501", "HTML" ) );
        sendTo( buildUrl( PATH ), new Topic( "I05", "I0502", "CSS" ) );
        sendTo( buildUrl( PATH ), new Topic( "I05", "I0503", "XML Core" ) );
        sendTo( buildUrl( PATH ), new Topic( "I05", "I0504", "XML Schema" ) );
        sendTo( buildUrl( PATH ), new Topic( "I05", "I0505", "XML DTD" ) );
        sendTo( buildUrl( PATH ), new Topic( "I05", "I0506", "XPath v1" ) );
        sendTo( buildUrl( PATH ), new Topic( "I05", "I0507", "XSLT" ) );
        
        // Specialty "I06"  "Other Languages"
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0601", "Python" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0602", "Go" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0603", "Clojure" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0604", "Groovy" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0605", "Grails" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0606", "PHP" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0607", "Ruby" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0608", "Javascript" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0609", "jQuery" ) );
        sendTo( buildUrl( PATH ), new Topic( "I06", "I0610", "Scala" ) );
        
        // Specialty "I07"  "Corba"
        sendTo( buildUrl( PATH ), new Topic( "I07", "I0701", "Corba - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "I07", "I0702", "Corba - Intermediate" ) );
        sendTo( buildUrl( PATH ), new Topic( "I07", "I0703", "Corba - Advanced" ) );
        
        // Specialty "M01"  "Mobile"
        sendTo( buildUrl( PATH ), new Topic( "M01", "M0101", "iPhone Programming Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "M01", "M0102", "Android Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "M01", "M0103", "Java ME" ) );

        // Specialty "D01"  "Database"
        sendTo( buildUrl( PATH ), new Topic( "D01", "D0101", "PL/SQL" ) );
        sendTo( buildUrl( PATH ), new Topic( "D01", "D0102", "SQL & RDB - Basic" ) );
        sendTo( buildUrl( PATH ), new Topic( "D01", "D0103", "Oracle Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( "D01", "D0104", "Oracle Administration" ) );
        
        // Specialty ".01"  "C#"
        sendTo( buildUrl( PATH ), new Topic( ".01", ".0101", "C# & OO Fundamentals" ) );
        sendTo( buildUrl( PATH ), new Topic( ".01", ".0102", "C# - Intermediate" ) );

        // Specialty "U01"  "ITBelts Usage"
        sendTo( buildUrl( PATH ), new Topic( "U01", "U0101", "Reference Guide" ) );
        sendTo( buildUrl( PATH ), new Topic( "U01", "U0102", "Site Contribution" ) );
        sendTo( buildUrl( PATH ), new Topic( "U01", "U0103", "Being Coach" ) );
        sendTo( buildUrl( PATH ), new Topic( "U01", "U0104", "Site Internationalisation" ) );
        
    }

}
