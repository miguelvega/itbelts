package org.itbelts.admin.mongolab;

import java.util.UUID;
import java.util.logging.Logger;

import org.itbelts.dao.mongolab.AbstractMongoLabDAO;
import org.itbelts.dao.mongolab.UserExamDAOImpl;
import org.itbelts.domain.UserExam;

public class UserExamLoader extends AbstractMongoLabDAO {

    private static final Logger LOGGER = Logger.getLogger( UserExamLoader.class.getName() );

    private static final String USEREXAM_PATH   = "collections/userexams";
    
    private static final String MRBROWN_KOEN = "110450052212046395375"; 
    
    /**
     * @param args
     */
    public static void main( String[] args ) throws Exception {
        new UserExamLoader().load();
        LOGGER.info( "" + new UserExamDAOImpl().getExamsForUser( MRBROWN_KOEN ) );
    }

    private void load() {
        UserExam u = new UserExam();
        
        u.set_id( UUID.randomUUID().toString() );
        u.setExamId( "J0807-E1" );  // ApectJ Basic
        u.setUserId( MRBROWN_KOEN );  // koenbruyndonckx@gmail.com
        
        u.start(20);
        u.setPercentage( 100 );
        
        sendTo( buildUrl( USEREXAM_PATH ), u );
    }

}
