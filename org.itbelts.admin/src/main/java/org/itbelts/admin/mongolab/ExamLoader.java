package org.itbelts.admin.mongolab;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.itbelts.dao.mongolab.AbstractMongoLabDAO;
import org.itbelts.dao.mongolab.ExamDAOImpl;
import org.itbelts.dao.mongolab.TopicDAOImpl;
import org.itbelts.domain.Exam;
import org.itbelts.domain.QuestionCategory;
import org.itbelts.domain.Topic;
import org.itbelts.domain.extract.ExamCategory;
import org.itbelts.domain.extract.ExamFromImport;

import com.thoughtworks.xstream.XStream;

public class ExamLoader extends AbstractMongoLabDAO {

    private static final Logger LOGGER = Logger.getLogger( ExamLoader.class.getName() );

    private static final String EXAM_PATH   = "collections/exams";
    private static final String EXAM_QUESTION_CATEGORY_PATH   = "collections/questioncategories";
    
    /**
     * Mappings from original exam names (from extract) towards our new topic id's
     */
    private static final String[][] MAPPINGS = new String[][] {
    {"ADF Business Components - Basic                                         " , "J0515"},
    {"Algorithms - Basic                                                      " , "I0202"},
    {"Android Fundamentals                                                    " , "M0102"},
    {"Ant - Basic                                                             " , "J0603"},
    {"Apache Camel - Basic                                                    " , "J0808"},
    {"AspectJ - Basic                                                         " , "J0807"},
    {"BlackBeltFactory Usage - Being Coach.                                   " , "U0103"},
    {"C# 2 - Intermediate                                                     " , ".0102"},
    {"CSS                                                                     " , "I0502"},
    {"CVS - Basic                                                             " , "I0306"},
    {"CVS Usage - Command Line                                                " , "I0307"},
    {"CVS Usage - Eclipse Plugin                                              " , "I0308"},
    {"CVS Usage - TortoiseCVS                                                 " , "I0309"},
    {"CVS Usage - WinCVS                                                      " , "I0310"},
    {"Clojure                                                                 " , "I0603"},
    {"Code Coverage                                                           " , "J0612"},
    {"Continuous Integration                                                  " , "J0613"},
    {"Corba - Advanced                                                        " , "I0703"},
    {"Corba - Basic                                                           " , "I0701"},
    {"Corba - Intermediate                                                    " , "I0702"},
    {"Design Patterns (GoF)                                                   " , "I0205"},
    {"EJB 2 - Basic                                                           " , "J0202"},
    {"EJB 2 - Intermed                                                        " , "J0203"},
    {"EJB 3 - Basic                                                           " , "J0204"},
    {"EJB 3 - Intermediate                                                    " , "J0205"},
    {"EasyMock                                                                " , "J0601"},
    {"Eclipse RCP - Basic                                                     " , "J0705"},
    {"GWT - Basic                                                             " , "J0511"},
    {"Go                                                                      " , "I0602"},
    {"Grails Fundamentals                                                     " , "I0605"},
    {"Groovy - Basic                                                          " , "I0604"},
    {"HTML                                                                    " , "I0501"},
    {"Hibernate 3 - Advanced Lifecycle                                        " , "J0414"},
    {"Hibernate 3 - Caching                                                   " , "J0413"},
    {"Hibernate 3 Core - Basic                                                " , "J0408"},
    {"Hibernate 3 - Core                                                      " , "J0409"},
    {"Hibernate 3 - Entity Associations and Relationships                     " , "J0410"},
    {"Hibernate 3 - Inheritance Strategies                                    " , "J0411"},
    {"Hibernate 3 - Persistent Objects Retrieval                              " , "J0412"},
    {"Hibernate 3 - Transaction & Concurrency                                 " , "J0415"},
    {"J2EE Design Patterns                                                    " , "J0201"},
    {"JAX-B                                                                   " , "J0806"},
    {"JDBC - Basic                                                            " , "J0401"},
    {"JDBC - Intermed                                                         " , "J0402"},
    {"JDO                                                                     " , "J0403"},
    {"JGoodies Bindings                                                       " , "J0704"},
    {"JGoodies Forms                                                          " , "J0703"},
    {"JMS                                                                     " , "J0801"},
    {"JPA                                                                     " , "J0407"},
    {"JPA_Hibernate Fundamentals                                              " , "J0404"},
    {"JSF 1.1 - Basic                                                         " , "J0509"},                                                                         
    {"JSP - Intermed                                                          " , "J0214"},                                                                         
    {"JSP and Expression Language                                             " , "J0213"},                                                                         
    {"JUnit - Basic                                                           " , "J0604"},                                                                         
    {"JUnit - Intermed                                                        " , "J0605"},                                                                         
    {"JUnit 4                                                                 " , "J0606"},                                                                         
    {"Java & OO Fundamentals                                                  " , "J0101"},                                                                         
    {"Java & OO Intermediate - part 1                                         " , "J0102"},                                                                         
    {"Java & OO Intermediate - part 2                                         " , "J0102"},                                                                         
    {"Java 3D - Basic                                                         " , "J0706"},                                                                         
    {"Java 5 New Base Library Features                                        " , "J0118"},                                                                         
    {"Java 5 New Language Features                                            " , "J0117"},                                                                         
    {"Java 5 New Tools and Tool Features                                      " , "J0119"},                                                                         
    {"Java 7 New Language Features                                            " , "J0120"},                                                                         
    {"Java Advanced                                                           " , "J0103"},                                                                         
    {"Java ME                                                                 " , "M0103"},                                                                         
    {"Java SE Base API - Basic                                                " , "J0108"},                                                                         
    {"Java SE Base API - Intermed                                             " , "J0109"},                                                                         
    {"Java SE Collections                                                     " , "J0105"},                                                                         
    {"Java SE Concurrency - High-Level                                        " , "J0106"},                                                                         
    {"Java SE Concurrency - Low-Level                                         " , "J0107"},                                                                         
    {"Java SE IO                                                              " , "J0110"},                                                                         
    {"Java SE NIO                                                             " , "J0111"},                                                                         
    {"Java SE Networking                                                      " , "J0112"},                                                                         
    {"Java SE Reflection API                                                  " , "J0115"},                                                                         
    {"Java SE RegEx                                                           " , "J0114"},                                                                         
    {"Java SE Security - Basic                                                " , "J0116"},                                                                         
    {"Java SE Tools                                                           " , "J0113"},                                                                         
    {"JavaMail - basic                                                        " , "J0809"},                                                                         
    {"Log4J                                                                   " , "J0802"},                                                                         
    {"Maven2 Developer                                                        " , "J0609"},                                                                         
    {"Maven2 J2EE                                                             " , "J0608"},                                                                         
    {"Maven2 Standard                                                         " , "J0607"},                                                                         
    {"Mockito Fundamentals                                                    " , "J0602"},                                                                         
    {"OO - Intermed                                                           " , "I0204"},                                                                         
    {"OO for Java - Basic                                                     " , "I0203"},                                                                         
    {"OSGI basic                                                              " , "J0805"},                                                                         
    {"Oracle Administration                                                   " , "D0104"},                                                                         
    {"Oracle Fundamentals                                                     " , "D0103"},                                                                         
    {"PHP Fundamentals                                                        " , "I0606"},                                                                         
    {"PL_SQL                                                                  " , "D0101"},                                                                         
    {"PMD - Basic                                                             " , "J0610"},                                                                         
    {"Portlet Exam - Basic                                                    " , "J0513"},                                                                         
    {"Programming Aptitude                                                    " , "I0201"},                                                                         
    {"Python                                                                  " , "I0601"},                                                                         
    {"Refactoring                                                             " , "I0207"},                                                                         
    {"Ruby - Basic                                                            " , "I0607"},                                                                         
    {"SCJP 6                                                                  " , "J0104"},                                                                         
    {"SQL & RDB - Basic                                                       " , "D0102"},                                                                         
    {"SVN - Admin                                                             " , "I0302"},                                                                         
    {"SVN - Basic                                                             " , "I0301"},                                                                         
    {"SVN Usage - Command Line                                                " , "I0303"},                                                                         
    {"SVN Usage - Eclipse SubVersive                                          " , "I0304"},                                                                         
    {"SVN Usage - TortoiseSVN                                                 " , "I0305"},                                                                         
    {"Scala Basic                                                             " , "I0610"},                                                                         
    {"Scrum Fundamentals                                                      " , "I0101"},                                                                         
    {"Seam - Basic                                                            " , "J0510"},                                                                         
    {"Servlet - Basic                                                         " , "J0208"},                                                                         
    {"Servlets 2 Intermediate                                                 " , "J0209"},                                                                         
    {"Servlets 3                                                              " , "J0210"},                                                                         
    {"Servlets 3 Advanced                                                     " , "J0212"},
    {"Servlets 3 Fundamentals OLD                                             " , "J0211"},                                                          
    {"Spring 3 Certification Mock                                             " , "J0308"},                                                                         
    {"Spring Core                                                             " , "J0302"},                                                                         
    {"Spring Core Fundamentals                                                " , "J0301"},                                                                         
    {"Spring DAO JDBC                                                         " , "J0306"},                                                                         
    {"Spring MVC                                                              " , "J0303"},                                                                         
    {"Spring Transactions                                                     " , "J0307"},                                                                         
    {"Spring Velocity integration                                             " , "J0304"},                                                                         
    {"Struts 1 - Basic                                                        " , "J0504"},
    {"Struts 1 - Intermed                                                     " , "J0505"},                                                                         
    {"Struts 2 - Basic                                                        " , "J0506"},                                                                         
    {"Struts Tiles                                                            " , "J0507"},                                                                         
    {"Swing - Basic                                                           " , "J0701"},                                                                         
    {"Swing LayoutManagers                                                    " , "J0702"},                                                                         
    {"Tapestry 4 - Basic                                                      " , "J0512"},                                                                         
    {"TestNG - Basic                                                          " , "J0611"},                                                                         
    {"UML for Java - Basic                                                    " , "I0206"},                                                                         
    {"UNIX Administration                                                     " , "I0402"},                                                                         
    {"UNIX and Shell Scripting                                                " , "I0401"},                                                                         
    {"Vaadin Fundamentals                                                     " , "J0501"},                                                                         
    {"Vaadin UI Components                                                    " , "J0503"},
    {"Velocity - Basic                                                        " , "J0803"},                                                                         
    {"Web Services - Basic                                                    " , "J0508"},                                                                         
    {"XML Core - Basic                                                        " , "I0503"},                                                                         
    {"XML DTD                                                                 " , "I0505"},                                                                         
    {"XML Programming - Basic                                                 " , "J0804"},                                                                         
    {"XML Schema                                                              " , "I0504"},                                                                        
    {"XPath v1                                                                " , "I0506"},                                                                        
    {"XSLT                                                                    " , "I0507"},                                                                         
    {"ZK Fundamentals                                                         " , "J0514"},                                                                         
    {"KnowledgeBlackBelt Contributor Exam                                     " , "U0102"},                                                                                                                                   
    {"BlackBeltFactory Usage - Internationalization                           " , "U0104"}
    };
    
    /**
     * @param args
     */
    public static void main( String[] args ) throws Exception {
        new ExamLoader().load();
        LOGGER.info( "" + new ExamDAOImpl().getExams() );
    }

    /**
     * Create a map of "mappings" from the original exam names towards the current topics
     * @return Map<String,String>
     */
    public static Map<String,String> getTopicMappings() {
        // Create a map of "mappings" from the original exam names towards the current topics
        Map<String,String> theTopicMapping = new HashMap<String,String>();
        for ( String[] t : MAPPINGS ) {
            theTopicMapping.put( t[0].trim(), t[1].trim() );
        }
        return theTopicMapping;
    }
    
    /**
     * Create a map of topic-id -> topic-name
     * @return Map<String,String>
     */
    public static Map<String,String> getTopicMap() {
        // Create a map of topics from the database
        List<Topic> theTopicList = new TopicDAOImpl().getTopics();
        Map<String,String> theTopicMap = new HashMap<String,String>();
        for ( Topic t : theTopicList ) {
            theTopicMap.put( t.get_id(), t.getName() );
        }
        return theTopicMap;
    }
    
    public static String getExamIdForName( Map<String,String> aTopicMapping, String anExamName ) {
        // For every question, map the information and create a DB entry
        String theTopicId = aTopicMapping.get( anExamName );
        String theExamId;
        if ( anExamName.indexOf( "part 1" ) > 0 ) {
            theExamId = theTopicId + "-E1";
        } else if ( anExamName.indexOf( "part 2" ) > 0 ) {
            theExamId = theTopicId + "-E2";
        } else {
            theExamId = theTopicId + "-E1";
        }
        return theExamId;
    }
    
    private void load() {
        
        // Create a map of topics from the database
        Map<String,String> theTopicMap = getTopicMap();
        
        // Create a map of "mappings" from the original exam names towards the current topics
        Map<String,String> theTopicMapping = getTopicMappings();
        
        // Load the exam xml from the extract
        @SuppressWarnings( "unchecked" )
        List<ExamFromImport> theExams = (List<ExamFromImport>) new XStream().fromXML( getClass().getResourceAsStream( "/exams.xml" ) );
        String theTopicId, theExamId, theName;
        
        for ( ExamFromImport e : theExams ) {
            // For every exam, map the information and create a DB entry
            theTopicId = theTopicMapping.get( e.getId() );
            theName = theTopicMap.get( theTopicId );
            if ( e.getId().indexOf( "part 1" ) > 0 ) {
                theExamId = theTopicId + "-E1";
                theName += " (part 1)";
            } else if ( e.getId().indexOf( "part 2" ) > 0 ) {
                    theExamId = theTopicId + "-E2";
                    theName += " (part 2)";
            } else {
                theExamId = theTopicId + "-E1";
            }
            
            Exam theExam = new Exam();
            theExam.set_id( theExamId );
            theExam.setDescription( e.getObjectives() );
            theExam.setMaxTime( e.getMinutes() );
            theExam.setName( theName );
            theExam.setKnowledgePoints( e.getKnowledgePoints() );
            theExam.setContributionPoints( e.getContributionPoints() == 0 ? 5 : e.getContributionPoints() );
            theExam.setStatus( e.getStatus() );
            theExam.setSuccessPercentage( e.getPercentage() );
            theExam.setTopicId( theTopicId );
            
            sendTo( buildUrl( EXAM_PATH ), theExam );

            // For every question-category in the exam, map the information and create a DB entry
            int i = 1;
            for ( ExamCategory ec : e.getCategories() ) {
                QuestionCategory qc = new QuestionCategory();
                
                qc.set_id( theExamId + String.format(  "-C%02d", i++ ) );
                qc.setExamId( theExamId );
                qc.setName( ec.getName() );
                qc.setDescription( ec.getObjectives() );
                qc.setNumberOfQuestions( ec.getQuestions() );
                
                sendTo( buildUrl( EXAM_QUESTION_CATEGORY_PATH ), qc );
            }
        }
    }

}
