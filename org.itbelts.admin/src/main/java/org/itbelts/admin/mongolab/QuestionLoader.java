package org.itbelts.admin.mongolab;

import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.itbelts.dao.IQuestionDAO;
import org.itbelts.dao.mongolab.AbstractMongoLabDAO;
import org.itbelts.dao.mongolab.QuestionDAOImpl;
import org.itbelts.domain.Question;
import org.itbelts.domain.QuestionCategory;
import org.itbelts.domain.QuestionStatus;
import org.itbelts.domain.extract.QuestionFromImport;

import com.thoughtworks.xstream.XStream;

public class QuestionLoader extends AbstractMongoLabDAO {

    private static final String QUESTION_PATH   = "collections/questions";
    
    
    /**
     * @param args
     */
    public static void main( String[] args ) throws Exception {
        new QuestionLoader().load();
    }

    @SuppressWarnings( "unchecked" )
    private void load() {

        IQuestionDAO theQuestionDAO = new QuestionDAOImpl();
        
        // Create a map of "mappings" from the original exam names towards the current topics
        Map<String,String> theTopicMapping = ExamLoader.getTopicMappings();
        
        File theInputFolder = new File( "d:\\data\\kbb\\_extract\\" );
        for ( String theFile : theInputFolder.list() ) {
        
            if ( !theFile.endsWith( "__questions.xml" ) ) {
                continue;
            }
            
            String theExamName = theFile.substring( 0, theFile.indexOf( "__" ) );
            theExamName = theExamName.replace(  "_",  " " );
            String theExamId = ExamLoader.getExamIdForName( theTopicMapping, theExamName );
            
            // Load a question xml from the extract
            List<QuestionFromImport> theQuestions;
            try {
                theQuestions = (List<QuestionFromImport>) new XStream().fromXML( new FileInputStream( new File( theInputFolder, theFile ) ) );
            } catch ( IOException e ) {
                e.printStackTrace();
                continue;
            }
        
            List<QuestionCategory> theCategories = theQuestionDAO.getQuestionsCategoriesForExam( theExamId );
            
            int i = 1;
            for ( QuestionFromImport q : theQuestions ) {
            
                Question theQuestion = new Question();
            
                theQuestion.set_id( theExamId + String.format( "-Q%04d", i++ ) );
                theQuestion.setName( q.getTitle() );
                theQuestion.setDescription( q.getText() );
                theQuestion.setExplanation( q.getExplanation() );
                
                theQuestion.setQuestionCategoryId( null );
                for ( QuestionCategory qc : theCategories ) {
                    if ( q.getCategory().endsWith( "/" + qc.getName() ) ) {
                        theQuestion.setQuestionCategoryId( qc.get_id() );
                        break;
                    }
                }
                if ( theQuestion.getQuestionCategoryId() == null ) {
                    System.out.println( "NO CATEGORY ID FOUND FOR QUESTION " + theQuestion.get_id() + " with category " + q.getCategory() );
                }
                
                if ( q.getStatus() == org.itbelts.domain.extract.QuestionStatus.B ) {
                    theQuestion.setStatus( QuestionStatus.Beta );
                } else if ( q.getStatus() == org.itbelts.domain.extract.QuestionStatus.F ) {
                    theQuestion.setStatus( QuestionStatus.Frozen );
                } else {
                    theQuestion.setStatus( QuestionStatus.Released );
                }
                
                theQuestion.setPossibleAnswers( q.getAnswers() );

                sendTo( buildUrl( QUESTION_PATH ), theQuestion );
            }
            
            // Now move the xml into the _DONE subfolder so it is not used for the next run
            if ( new File( theInputFolder, theFile ).renameTo( new File( theInputFolder + "\\_DONE", theFile ))){
                 System.out.println( "Question file has been moved" );
            } else {
                Toolkit.getDefaultToolkit().beep();
                Toolkit.getDefaultToolkit().beep();
                Toolkit.getDefaultToolkit().beep();
                throw new RuntimeException( "Question File " + theFile + " could not be moved" );
            }
        }
    }

}
