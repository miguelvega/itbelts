package org.itbelts.exception;

/**
 * Base class for the ITBelts exceptions.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ITBeltsException extends RuntimeException {

    private static final long serialVersionUID = -6041297253593110978L;

    /**
     * @param aMessage
     * @param aCause
     */
    public ITBeltsException( String aMessage, Throwable aCause ) {
        super( aMessage, aCause );
    }

    /**
     * @param aMessage
     */
    public ITBeltsException( String aMessage ) {
        super( aMessage );
    }

    
}
