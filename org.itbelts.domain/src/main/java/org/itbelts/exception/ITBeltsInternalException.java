package org.itbelts.exception;

/**
 * Internal exception.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ITBeltsInternalException extends ITBeltsException {

    private static final long serialVersionUID = -1114279830950191022L;

    /**
     * @param aMessage
     * @param aCause
     */
    public ITBeltsInternalException( String aMessage, Throwable aCause ) {
        super( aMessage, aCause );
    }

    /**
     * @param aMessage
     */
    public ITBeltsInternalException( String aMessage ) {
        super( aMessage );
    }

}
