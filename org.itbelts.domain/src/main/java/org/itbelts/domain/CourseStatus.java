package org.itbelts.domain;

/**
 *  Status of a course.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 28-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      28-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public enum CourseStatus {

    beta,
    released;
}
