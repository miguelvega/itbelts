package org.itbelts.domain;

import java.util.HashMap;

/**
 * Role types used in the itbelts application. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 12 Oct 2013 - initial release
 * 
 * </pre>
 * 
 * @version 12 Oct 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public enum RoleType {

    ADMIN("Z"),
    USER("U"),
    COACH_LEVEL_1("1"),
    COACH_LEVEL_2("2"),
    COACH_LEVEL_3("3");

    private String code;

    private static HashMap<String,RoleType> codeValueMap = new HashMap<String,RoleType>( 5 );

    static {
        for ( RoleType type : RoleType.values() ) {
            codeValueMap.put( type.getCode(), type );
        }
    }

    /**
     * Constructor to capture the code.
     * @param aCode
     */
    RoleType(String aCode) {
        code = aCode;
    }

    /**
     * Return the code for this enum.
     * @return String
     */
    public String getCode() {
        return code;
    }

    /**
     * Return the enum for the given code.
     * @param aCode
     * @return {@link RoleType}
     */
    public static RoleType getInstanceFromCode( String aCode ) {
        return codeValueMap.get( aCode );
    }
}