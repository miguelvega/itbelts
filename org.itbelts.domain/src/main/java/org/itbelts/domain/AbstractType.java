package org.itbelts.domain;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

/**
 *  Base class for all ITBelts domain classes.<br>
 * <br>
 * <pre>
 * <u><i>Version History</i></u>
 * 12 oct 2013 - Add serialisation to all types (for persistence)
 * </pre>
 *
 * @version 12 Oct 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public abstract class AbstractType implements Serializable {

    private static final long serialVersionUID = 1560844900100533407L;

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString( this );
    }
}
