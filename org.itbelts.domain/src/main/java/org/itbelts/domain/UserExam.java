package org.itbelts.domain;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;


/**
 * Exam in the site. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class UserExam extends AbstractType {

    private static final long serialVersionUID = -6135393023674676308L;

    public static final int ONGOING            = 0;
    public static final int PASSED             = 1;
    public static final int FAILED_OUT_OF_TIME = 2;
    public static final int FAILED_LOW_SCORE   = 3;
    
    private String   _id;
    private String   userId;
    private String   examId;
    private String   start;    // yyyy-MM-dd HH:mm:ss
    private String   end;      // yyyy-MM-dd HH:mm:ss
    private int      result;   // implemented as int to conserve DB storage space
    private int      score;
    private int      percentage;
    
    private List<AnswerContainer> answers = new ArrayList<>();  // They key is the question id
    
    /**
     * Simple constructur that sets the ID to a random value
     */
    public UserExam() {
        _id = UUID.randomUUID().toString();
    }
    
    /**
     * Start the clock
     * 
     * @param aMaximumDuration    The maxiumum duration in minutes (taken from the Exam)
     */
    public void start( int aMaximumDuration ) {
        Calendar c = getCurrentTime();
        start = formatCalendarToString( c );

        // Just add the given amount of minutes
        c.add( Calendar.MINUTE, aMaximumDuration );
        end = formatCalendarToString( c );
    }

    /**
     * Stop the clock
     */
    public void stop() {
        end = formatCalendarToString( getCurrentTime() );
    }

    /**
     * Get the current time.
     * @return Calendar
     */
    private Calendar getCurrentTime() {
        // Set the current date (we use UTC to match the logic of the counter.js Javascript)
        return new GregorianCalendar( TimeZone.getTimeZone( "UTC" ) );
    }
    
    /**
     * Format the given calendar as a string.
     * @return String
     */
    private String formatCalendarToString( Calendar aCalendar ) {
        SimpleDateFormat f = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        return f.format( aCalendar.getTime() );
    }
    
    
    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }

    /**
     * @return Returns the userId.
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param aUserId The userId to set.
     */
    public void setUserId( String aUserId ) {
        userId = aUserId;
    }

    /**
     * @return Returns the examId.
     */
    public String getExamId() {
        return examId;
    }

    /**
     * @param aExamId The examId to set.
     */
    public void setExamId( String aExamId ) {
        examId = aExamId;
    }

    /**
     * @return Returns the start.
     */
    public String getStart() {
        return start;
    }

    /**
     * @param aStart The start to set.
     */
    public void setStart( String aStart ) {
        start = aStart;
    }

    /**
     * @return Returns the end.
     */
    public String getEnd() {
        return end;
    }

    /**
     * @param aEnd The end to set.
     */
    public void setEnd( String aEnd ) {
        end = aEnd;
    }

    /**
     * <pre>
     * 1 = passed
     * 2 = failed (out of time)
     * 3 = failed (percentage too low)
     * </pre>
     * 
     * @return Returns the result.
     */
    public int getResult() {
        return result;
    }

    /**
     * @param aResult The result to set.
     */
    public void setResult( int aResult ) {
        result = aResult;
    }

    /**
     * @return Returns the score.
     */
    public int getScore() {
        return score;
    }

    /**
     * @param aScore The score to set.
     */
    public void setScore( int aScore ) {
        score = aScore;
    }

    /**
     * @return Returns the percentage.
     */
    public int getPercentage() {
        return percentage;
    }

    /**
     * @param aPercentage The percentage to set.
     */
    public void setPercentage( int aPercentage ) {
        percentage = aPercentage;
    }

    /**
     * @return Returns the answers.
     */
    public List<AnswerContainer> getAnswers() {
        return answers;
    }

    /**
     * @param aAnswers The answers to set.
     */
    public void setAnswers( List<AnswerContainer> aAnswers ) {
        answers = aAnswers;
    }

}
