package org.itbelts.domain;

/**
 *  Answer types.<br>
 * <br>
 * <pre>
 * Text        = Textual answer.  Free text to be introduced by the student.
 * RadioButton = 1 Choice answer.
 * CheckBox    = Multiple choice answer.
 * 
 * <u><i>Version History</i></u>
 * 14 Jan 2013 - initial release
 * 12 oct 2013 - Fixed Javadoc
 * </pre>
 *
 * @version 12 Oct 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public enum AnswerType {

    Text,
    RadioButton,
    CheckBox;
}
