package org.itbelts.domain;

/**
 *  Defines a topic within a specialty.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 19-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      19-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class Topic extends AbstractType {
    
    private static final long serialVersionUID = 7500748616637238080L;

    private String specialtyId;
    private String _id;
    private String name;
    
    /**
     * Constructor.
     */
    public Topic() {
    }

    /**
     * Constructor.
     * @param aSpecialtyId
     * @param anId
     * @param aName
     */
    public Topic( String aSpecialtyId, String anId, String aName ) {
        specialtyId = aSpecialtyId;
        _id   = anId;
        name = aName;
    }

    /**
     * @return Returns the specialtyId.
     */
    public String getSpecialtyId() {
        return specialtyId;
    }

    /**
     * @param aSpecialtyId The specialtyId to set.
     */
    public void setSpecialtyId( String aSpecialtyId ) {
        specialtyId = aSpecialtyId;
    }

    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }

    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }
    
    
}
