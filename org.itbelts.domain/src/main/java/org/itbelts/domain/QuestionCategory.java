package org.itbelts.domain;

import org.itbelts.domain.AbstractType;


/**
 * Exam question category.<br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 2013.12.0  01 Nov 2013 - initial release
 *
 * </pre>
 *
 * @version 01 Nov 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class QuestionCategory extends AbstractType {

    private static final long serialVersionUID = 1238493088858566840L;

    private String     _id;
    private String     examId;
    private String     name;
    private int        numberOfQuestions;
    private String     description;
    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }
    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }
    /**
     * @return Returns the examId.
     */
    public String getExamId() {
        return examId;
    }
    /**
     * @param aExamId The examId to set.
     */
    public void setExamId( String aExamId ) {
        examId = aExamId;
    }
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }
    /**
     * @return Returns the numberOfQuestions.
     */
    public int getNumberOfQuestions() {
        return numberOfQuestions;
    }
    /**
     * @param aNumberOfQuestions The numberOfQuestions to set.
     */
    public void setNumberOfQuestions( int aNumberOfQuestions ) {
        numberOfQuestions = aNumberOfQuestions;
    }
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param aDescription The description to set.
     */
    public void setDescription( String aDescription ) {
        description = aDescription;
    }
    
}