package org.itbelts.domain;


/**
 * Exam in the site. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class Exam extends AbstractType {

    private static final long serialVersionUID = 1070352772499640096L;

    /**
     * The identifier is called _id to let MongoDB reuse it.<br>
     */
    private String     _id;
    /**
     * Name for the exam.  Useful when there is more than 1 exam for a topic.
     */
    private String     name;
    /**
     * Foreign key towards the topics collection.
     */
    private String     topicId;
    /**
     * Maximum duration for the exam (in minutes).
     */
    private int        maxTime;
    /**
     * Percentage of questions that need to be answered correctly to pass the exam.
     */
    private int        successPercentage;
    /**
     * Points earned for passing this exam.
     */
    private int        knowledgePoints;
    /**
     * Points to be paid for taking this exam.
     */
    private int        contributionPoints;
    /**
     * Description of this exam (objectives, study sources, ...)  (can contains HTML tags)
     */
    private String     description;
    /**
     * Status of the exam
     */
    private ExamStatus status;
    
    /**
     * Return a nice description of the status of this content
     * @return String  (used in exam.jsp)
     */
    public String getStatusDescription() {
        if ( status == ExamStatus.Beta ) {
            return "still in BETA";
        }
        if ( status == ExamStatus.ObjectiveWriting ) {
            return "awaiting objectives";
        }
        if ( status == ExamStatus.QuestionFilling ) {
            return "awaiting new questions";
        }
        
        return "RELEASED";
    }

    
    
    
    
    
    
    /**
     * @return Returns the _id.
     */
    public String get_id() {
        return _id;
    }
    /**
     * @param a_id The _id to set.
     */
    public void set_id( String a_id ) {
        _id = a_id;
    }
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }
    /**
     * @return Returns the topicId.
     */
    public String getTopicId() {
        return topicId;
    }
    /**
     * @param aTopicId The topicId to set.
     */
    public void setTopicId( String aTopicId ) {
        topicId = aTopicId;
    }
    /**
     * @return Returns the maxTime.
     */
    public int getMaxTime() {
        return maxTime;
    }
    /**
     * @param aMaxTime The maxTime to set.
     */
    public void setMaxTime( int aMaxTime ) {
        maxTime = aMaxTime;
    }
    /**
     * @return Returns the successPercentage.
     */
    public int getSuccessPercentage() {
        return successPercentage;
    }
    /**
     * @param aSuccessPercentage The successPercentage to set.
     */
    public void setSuccessPercentage( int aSuccessPercentage ) {
        successPercentage = aSuccessPercentage;
    }
    
    /**
     * @return Returns the knowledgePoints.
     */
    public int getKnowledgePoints() {
        return knowledgePoints;
    }

    /**
     * @param aKnowledgePoints The knowledgePoints to set.
     */
    public void setKnowledgePoints( int aKnowledgePoints ) {
        knowledgePoints = aKnowledgePoints;
    }

    /**
     * @return Returns the contributionPoints.
     */
    public int getContributionPoints() {
        return contributionPoints;
    }

    /**
     * @param aContributionPoints The contributionPoints to set.
     */
    public void setContributionPoints( int aContributionPoints ) {
        contributionPoints = aContributionPoints;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param aDescription The description to set.
     */
    public void setDescription( String aDescription ) {
        description = aDescription;
    }
    /**
     * @return Returns the status.
     */
    public ExamStatus getStatus() {
        return status;
    }
    /**
     * @param aStatus The status to set.
     */
    public void setStatus( ExamStatus aStatus ) {
        status = aStatus;
    }

}
