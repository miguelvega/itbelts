package org.itbelts.domain.extract;

import org.itbelts.domain.AbstractType;


/**
 * Exam category. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ExamCategory extends AbstractType {

    private static final long serialVersionUID = -8261126665487676782L;

    private String     name;
    private int        questions;
    private String     objectives;
    
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }

    /**
     * @return Returns the questions.
     */
    public int getQuestions() {
        return questions;
    }

    /**
     * @param aQuestions The questions to set.
     */
    public void setQuestions( int aQuestions ) {
        questions = aQuestions;
    }

    /**
     * @return Returns the objectives.
     */
    public String getObjectives() {
        return objectives;
    }

    /**
     * @param aObjectives The objectives to set.
     */
    public void setObjectives( String aObjectives ) {
        objectives = aObjectives;
    }

}