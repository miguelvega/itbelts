package org.itbelts.domain.extract;

/**
 *  Question status information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public enum QuestionStatus {

    R,  // Released
    B,  // Beta
    F;  // Frozen
}
