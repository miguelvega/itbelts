package org.itbelts.domain.extract;

import org.itbelts.domain.AbstractType;


/**
 * Exam category. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ExamContributorFromImport extends AbstractType {

    private static final long serialVersionUID = 4251476026472095407L;

    private String     id;
    private String     name;
    private double     points;
    private double     percentage;

    /**
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }

    /**
     * @param aId The id to set.
     */
    public void setId( String aId ) {
        id = aId;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param aName The name to set.
     */
    public void setName( String aName ) {
        name = aName;
    }

    /**
     * @return Returns the points.
     */
    public double getPoints() {
        return points;
    }

    /**
     * @param aPoints The points to set.
     */
    public void setPoints( double aPoints ) {
        points = aPoints;
    }

    /**
     * @return Returns the percentage.
     */
    public double getPercentage() {
        return percentage;
    }

    /**
     * @param aPercentage The percentage to set.
     */
    public void setPercentage( double aPercentage ) {
        percentage = aPercentage;
    }
}
