package org.itbelts.dao;

import java.util.List;
import java.util.Map;

import org.itbelts.domain.Question;
import org.itbelts.domain.QuestionCategory;
import org.itbelts.domain.UserExam;

/**
 * Interface that describes how to manage question information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 01-nov.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      01-nov.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface IQuestionDAO {

    /**
     * Get all question categories for the given exam.
     * @param anExamId
     * @return List<QuestionCategory>
     */
    public List<QuestionCategory> getQuestionsCategoriesForExam( String anExamId );

    /**
     * Get all questions for the given question category.
     * @param aCategoryId
     * @return List<Question>
     */
    public List<Question> getQuestionsForCategory( String aCategoryId );

    /**
     * Get all questions for the given question user exam.  (for viewing exam results)
     * @param aUserExam
     * @return Map<String,Question>
     */
    public Map<String,Question> getQuestionsForUserExam( UserExam aUserExam );

    /**
     * Compose a list of random questions for the given exam.
     * @param anExamId
     * @return List<Question>
     */
    public List<Question> buildExam( String anExamId );

    /**
     * Get a specific question.
     * @param aQuestionId
     * @return {@link Question}
     */
    public Question getQuestion( String aQuestionId );
    
    
}
