package org.itbelts.dao.mongolab;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

import org.itbelts.dao.ITopicDAO;
import org.itbelts.domain.Topic;

import com.google.gson.reflect.TypeToken;

/**
 *  MongoDB implementation for the ITopicDAO.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class TopicDAOImpl extends AbstractMongoLabDAO implements ITopicDAO {

    private static final String PATH = "collections/topics";
    
    @Override
    public List<Topic> getTopics() {
        URL url = buildUrl( PATH );
        Type theListType = new TypeToken<List<Topic>>(){}.getType();

        return readFrom( url, theListType );
    }

    @Override
    public List<Topic> getTopicsForSpecialty( String aSpecialtyId ) {
        URL url = buildUrl( PATH, "q={\"specialtyId\":\"" + aSpecialtyId + "\"}" );
        Type theListType = new TypeToken<List<Topic>>(){}.getType();
        
        return readFrom( url, theListType );
    }

    @Override
    public List<Topic> getTopicsForCommunity( String aCommunityId ) {
        URL url = buildUrl( PATH, "q={\"specialtyId\":{$regex:\"^" + aCommunityId + ".*\"}}" );
        Type theListType = new TypeToken<List<Topic>>(){}.getType();
        
        return readFrom( url, theListType );
    }

    @Override
    public Topic getTopic( String aTopicId ) {
        URL url = buildUrl( PATH, "q={\"_id\":\"" + aTopicId + "\"}}" );
        Type theListType = new TypeToken<List<Topic>>(){}.getType();
        
        List<Topic> theList = readFrom( url, theListType );
        if ( theList == null ) {
            return null;
        }
        return theList.get( 0 );
    }
}
