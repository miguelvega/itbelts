package org.itbelts.dao.mongolab;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

import org.itbelts.dao.IUserExamDAO;
import org.itbelts.domain.UserExam;

import com.google.gson.reflect.TypeToken;

/**
 * MongoDB implementation for the IUserExamDAO. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.12.0 05-nov.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.12.0 05-nov.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class UserExamDAOImpl extends AbstractMongoLabDAO implements IUserExamDAO {

    private static final String PATH = "collections/userexams"; 

    @Override
    public List<UserExam> getExamsForUser( String aUserId ) {
        
        URL url = buildUrl( PATH, "q={\"userId\":\"" + aUserId + "\"}" );
        Type theListType = new TypeToken<List<UserExam>>(){}.getType();
        
        return readFrom( url, theListType );
    }
    
    @Override
    public UserExam getUserExam( String aUserExamId ) {
        URL url = buildUrl( PATH, "q={\"_id\":\"" + aUserExamId + "\"}" );
        Type theListType = new TypeToken<List<UserExam>>(){}.getType();
        
        List<UserExam> theList = readFrom( url, theListType );
        
        return theList.get( 0 );
    }

    @Override
    public UserExam saveUserExam( UserExam aUserExam ) {
        sendTo( buildUrl( PATH ), aUserExam );
        
        return aUserExam;
    }

}
