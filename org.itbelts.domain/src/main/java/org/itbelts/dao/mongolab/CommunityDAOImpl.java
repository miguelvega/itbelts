package org.itbelts.dao.mongolab;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

import org.itbelts.dao.ICommunityDAO;
import org.itbelts.domain.Community;

import com.google.gson.reflect.TypeToken;

/**
 *  MongoDB implementation for the ICommunityDAO.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class CommunityDAOImpl extends AbstractMongoLabDAO implements ICommunityDAO {

    private static final String PATH = "collections/communities";
    
    /* (non-Javadoc)
     * @see org.itbelts.dao.ISpecialtyDAO#getSpecialties()
     */
    @Override
    public List<Community> getCommunities() {
        URL url = buildUrl( PATH );
        Type theListType = new TypeToken<List<Community>>(){}.getType();

        return readFrom( url, theListType );
    }

    @Override
    public Community getCommunity( String aCommunityId ) {
        URL url = buildUrl( PATH, "q={\"_id\":\"" + aCommunityId + "\"}" );
        Type theListType = new TypeToken<List<Community>>(){}.getType();
        
        List<Community> theList = readFrom( url, theListType );
        
        return theList.get( 0 );
    }

}
