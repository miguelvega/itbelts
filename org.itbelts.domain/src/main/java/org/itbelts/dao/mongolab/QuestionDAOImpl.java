package org.itbelts.dao.mongolab;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.itbelts.dao.IQuestionDAO;
import org.itbelts.domain.AnswerContainer;
import org.itbelts.domain.Question;
import org.itbelts.domain.QuestionCategory;
import org.itbelts.domain.UserExam;

import com.google.gson.reflect.TypeToken;

/**
 * MongoDB implementation for the IQuestionDAO. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.12.0 01-nov.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.12.0 01-nov.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class QuestionDAOImpl extends AbstractMongoLabDAO implements IQuestionDAO {

    private static final String PATH = "collections/questions"; 
    private static final String CATEGORY_PATH = "collections/questioncategories"; 

    @Override
    public List<QuestionCategory> getQuestionsCategoriesForExam( String anExamId ) {
        URL url = buildUrl( CATEGORY_PATH, "q={\"examId\":\"" + anExamId + "\"}" );
        Type theListType = new TypeToken<List<QuestionCategory>>(){}.getType();
        
        return readFrom( url, theListType );
    }

    @Override
    public List<Question> getQuestionsForCategory( String aCategoryId ) {
        URL url = buildUrl( PATH, "q={\"questionCategoryId\":\"" + aCategoryId + "\"}" );
        Type theListType = new TypeToken<List<Question>>(){}.getType();
        
        return readFrom( url, theListType );
    }

    @Override
    public Map<String,Question> getQuestionsForUserExam( UserExam aUserExam ) {
        
        Map<String,Question> theMap = new HashMap<>();
        if ( aUserExam.getAnswers() == null || aUserExam.getAnswers().size() == 0 ) {
            return theMap;
        }
        
        try {
            for ( AnswerContainer ac : aUserExam.getAnswers() ) {
                theMap.put( ac.getQuestionId(), getQuestion( ac.getQuestionId() ) );
            }
        } catch ( Exception e ) {
            // This could fail if the userExam points to questions that are no longer available.  We'll deal with this in the view controller of the exam
        }
        return theMap;
    }
    
    @Override
    public Question getQuestion( String aQuestionId ) {
        URL url = buildUrl( PATH, "q={\"_id\":\"" + aQuestionId + "\"}" );
        Type theListType = new TypeToken<List<Question>>(){}.getType();
        
        List<Question> theList = readFrom( url, theListType );
        
        return theList.get( 0 );
    }

    @Override
    public List<Question> buildExam( String anExamId ) {
        
        List<Question> theExam = new ArrayList<Question>();
        
        // the list of questions needs to come from all question categories.  So we first read all categories for the exam.
        List<QuestionCategory> theCategories = getQuestionsCategoriesForExam( anExamId );
        
        // Then, for every category, according to its number of questions, we compose a random set
        Random r = new Random();
        for ( QuestionCategory c : theCategories ) {

            List<Question> theQuestions = getQuestionsForCategory( c.get_id() );
            
            int theAmount = c.getNumberOfQuestions();
            while ( theAmount > 0 ) {
                int theChosenQuestionIndex = r.nextInt( theQuestions.size() );
                theExam.add( theQuestions.get( theChosenQuestionIndex ) );
                theQuestions.remove( theChosenQuestionIndex );
                theAmount--;
            }
        }
        return theExam;
    }

}
