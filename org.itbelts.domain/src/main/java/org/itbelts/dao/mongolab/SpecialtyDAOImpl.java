package org.itbelts.dao.mongolab;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

import org.itbelts.dao.ISpecialtyDAO;
import org.itbelts.domain.Specialty;

import com.google.gson.reflect.TypeToken;

/**
 *  MongoDB implementation for the ISpecialtyDAO.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class SpecialtyDAOImpl extends AbstractMongoLabDAO implements ISpecialtyDAO {

    private static final String PATH = "collections/specialties";
    
    @Override
    public List<Specialty> getSpecialties() {
        URL url = buildUrl( PATH );
        Type theListType = new TypeToken<List<Specialty>>(){}.getType();

        return readFrom( url, theListType );
    }

    
    @Override
    public List<Specialty> getSpecialtiesForCommunity( String aCommunityId ) {
        URL url = buildUrl( PATH, "q={\"communityId\":\"" + aCommunityId + "\"}" );
        Type theListType = new TypeToken<List<Specialty>>(){}.getType();
        
        return readFrom( url, theListType );
    }

    
    @Override
    public Specialty getSpecialty( String aSpecialtyId ) {
        URL url = buildUrl( PATH, "q={\"_id\":\"" + aSpecialtyId + "\"}" );
        Type theListType = new TypeToken<List<Specialty>>(){}.getType();
        
        List<Specialty> theList = readFrom( url, theListType );
        
        return theList.get( 0 );
    }
}
