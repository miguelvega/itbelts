package org.itbelts.dao.mongodb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.itbelts.dao.ICourseDAO;
import org.itbelts.dao.ITopicDAO;
import org.itbelts.domain.Course;
import org.itbelts.domain.Topic;

import com.mongodb.BasicDBObject;

/**
 * MongoDB implementation for the ICourseDAO. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.10.0 20-okt.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class CourseDAOImpl extends AbstractMongoDbDAO implements ICourseDAO {

    private ITopicDAO     myTopicDAO     = new TopicDAOImpl();
    private static final String COLLECTION = "courses"; 

    @Override
    public List<Course> getCourses() {
        return readFrom( COLLECTION, Course.class, null, new BasicDBObject("_id", 1 ).append( "topicId", 1 ).append( "name", 1 ) );
    }

    @Override
    public List<Course> getCoursesForCommunity( String aCommunityId ) {
        
        // First get a complete list of all courses  (we will filter out the ones we do not need later)
        List<Course> theFullList = getCourses();
        
        // Build a small Map of topic-ids for all topics belonging to the given community
        Map<String,Boolean> theTopicMap = new HashMap<String,Boolean>();
        
        List<Topic> theTopics = myTopicDAO.getTopicsForCommunity( aCommunityId );
        
        for ( Topic t : theTopics ) {
            theTopicMap.put( t.get_id(), Boolean.TRUE );
        }
        
        // Then remove all courses that have a topic id that is not in the Map
        for ( int i = theFullList.size() -1 ; i >= 0 ; i-- ) {
            String theTopicId = theFullList.get( i ).getTopicId();
            if ( theTopicMap.get( theTopicId ) == null ) {
                theFullList.remove( i );
            }
        }
        
        return theFullList;
    }
    
    @Override
    public Course getCourse( String aCourseId ) {
        return readFrom( COLLECTION, Course.class, new BasicDBObject("_id", aCourseId ), null ).get( 0 );
    }

}
