package org.itbelts.dao.mongodb;

import java.util.List;

import org.itbelts.dao.ISpecialtyDAO;
import org.itbelts.domain.Specialty;

import com.mongodb.BasicDBObject;

/**
 *  MongoDB implementation for the ISpecialtyDAO.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href=\"mailto:koenbruyndonckx@gmail.com\"> Koen Bruyndonckx </a>
 */
public class SpecialtyDAOImpl extends AbstractMongoDbDAO implements ISpecialtyDAO {

    private static final String COLLECTION = "specialties";
    
    @Override
    public List<Specialty> getSpecialties() {
        return readFrom( COLLECTION, Specialty.class, null, null );
    }

    
    @Override
    public List<Specialty> getSpecialtiesForCommunity( String aCommunityId ) {
        return readFrom( COLLECTION, Specialty.class, new BasicDBObject("communityId", aCommunityId ), null );
    }

    
    @Override
    public Specialty getSpecialty( String aSpecialtyId ) {
        return readFrom( COLLECTION, Specialty.class, new BasicDBObject("_id", aSpecialtyId ), null ).get( 0 );
    }
}
