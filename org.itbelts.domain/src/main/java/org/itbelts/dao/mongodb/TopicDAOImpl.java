package org.itbelts.dao.mongodb;

import java.util.List;

import org.itbelts.dao.ITopicDAO;
import org.itbelts.domain.Topic;

import com.mongodb.BasicDBObject;

/**
 *  MongoDB implementation for the ITopicDAO.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2014.6.0  02-jun-2014 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2014.6.0  02-jun-2014
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class TopicDAOImpl extends AbstractMongoDbDAO implements ITopicDAO {

    private static final String COLLECTION = "topics";
    
    @Override
    public List<Topic> getTopics() {
        return readFrom( COLLECTION, Topic.class, null, null );
    }

    @Override
    public List<Topic> getTopicsForSpecialty( String aSpecialtyId ) {
        return readFrom( COLLECTION, Topic.class, new BasicDBObject("specialtyId", aSpecialtyId ), null );
    }

    @Override
    public List<Topic> getTopicsForCommunity( String aCommunityId ) {
        return readFrom( COLLECTION, Topic.class, new BasicDBObject("specialtyId", java.util.regex.Pattern.compile( "^" + aCommunityId + ".*" ) ), null );
    }
    @Override
    public Topic getTopic( String aTopicId ) {
        return readFrom( COLLECTION, Topic.class, new BasicDBObject("specialtyId", aTopicId ), null ).get( 0 );
    }
}
