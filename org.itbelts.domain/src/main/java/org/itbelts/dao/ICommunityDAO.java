package org.itbelts.dao;

import java.util.List;

import org.itbelts.domain.Community;

/**
 * Interface that describes how to manage community information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface ICommunityDAO {

    /**
     * Get the list of existing communities.
     * @return List<Community>
     */
    public List<Community> getCommunities();

    /**
     * Get a specific community for the given communityId.
     * @param aCommunityId
     * @return {@link Community}
     */
    public Community getCommunity( String aCommunityId );

}
