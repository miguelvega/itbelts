package org.itbelts.dao;

import java.util.List;

import org.itbelts.domain.Specialty;

/**
 * Interface that describes how to manage specialty information.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 20-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      20-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface ISpecialtyDAO {

    public List<Specialty> getSpecialties();

    public List<Specialty> getSpecialtiesForCommunity( String aCommunityId );

    public Specialty getSpecialty( String aSpecialtyId );
    
    
}
