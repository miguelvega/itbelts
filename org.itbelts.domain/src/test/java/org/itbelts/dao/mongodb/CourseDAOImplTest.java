/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.logging.Logger;

import org.itbelts.domain.Course;
import org.junit.Test;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 22 Oct 2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      22 Oct 2013
 * @author <a href="mailto:koen.bruyndonckx@axa.be"> Koen Bruyndonckx </a>
 */
public class CourseDAOImplTest {

    private CourseDAOImpl myUnitUnderTest = new CourseDAOImpl();
    private static final Logger LOGGER = Logger.getLogger( CourseDAOImplTest.class.getName() );

    /**
     * Test method for {@link org.itbelts.dao.mongolab.CourseDAOImpl#getCourses()}.
     */
    @Test
    public void testGetCourses() {
        List<Course> theList = myUnitUnderTest.getCourses();
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        LOGGER.info( theList.toString() );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.CourseDAOImpl#getCoursesForCommunity(java.lang.String)}.
     */
    @Test
    public void testGetCoursesForCommunity() {
        List<Course> theList = myUnitUnderTest.getCoursesForCommunity( "J" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        assertTrue( theList.get(0).getTopicId().startsWith( "J" ) );
        LOGGER.info( theList.toString() );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.CourseDAOImpl#getCourse(java.lang.String)}.
     */
    @Test
    public void testGetCourse() {
        Course theCourse = myUnitUnderTest.getCourse( "C001" );
        assertNotNull( theCourse );
        assertEquals( "Java and OO Fundamentals", theCourse.getName() );
        LOGGER.info( theCourse.toString() );
    }

}
