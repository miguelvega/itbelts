package org.itbelts.dao.mongodb;

import static org.junit.Assert.assertEquals;

import java.rmi.server.UID;

import org.itbelts.domain.User;
import org.junit.Test;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 24-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      24-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class UserDAOImplTest {

    private UserDAOImpl myUnitUnderTest = new UserDAOImpl();

    private static final String UNIT_TEST_KEY = "_BLABLA_";
    
    /**
     * Test method for {@link org.itbelts.dao.mongolab.UserDAOImpl#saveUser(org.itbelts.domain.User)}.
     */
    @Test
    public void testScenario() {
        User theUser = new User();
        theUser.set_id( UNIT_TEST_KEY );
        theUser.setName( new UID().toString() );
        
        // Save a user
        myUnitUnderTest.saveUser( theUser );
        
        // read it back
        User theReadUser = myUnitUnderTest.getUser( UNIT_TEST_KEY );
        assertEquals( theUser.getName(), theReadUser.getName() );
        
    }

}
