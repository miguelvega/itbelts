/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.logging.Logger;

import org.itbelts.domain.Specialty;
import org.junit.Test;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 22 Oct 2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      22 Oct 2013
 * @author <a href="mailto:koen.bruyndonckx@axa.be"> Koen Bruyndonckx </a>
 */
public class SpecialtyDAOImplTest {

    private SpecialtyDAOImpl myUnitUnderTest = new SpecialtyDAOImpl();
    private static final Logger LOGGER = Logger.getLogger( SpecialtyDAOImplTest.class.getName() );

    /**
     * Test method for {@link org.itbelts.dao.mongolab.SpecialtyDAOImpl#getSpecialties()}.
     */
    @Test
    public void testGetSpecialties() {
        List<Specialty> theList = myUnitUnderTest.getSpecialties();
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        LOGGER.info( theList.toString() );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.SpecialtyDAOImpl#getSpecialtiesForCommunity(java.lang.String)}.
     */
    @Test
    public void testGetSpecialtiesForCommunity() {
        List<Specialty> theList = myUnitUnderTest.getSpecialtiesForCommunity( "J" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        for ( Specialty s : theList ) {
            assertEquals( "J", s.getCommunityId() );
        }
        LOGGER.info( theList.toString() );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.SpecialtyDAOImpl#getSpecialty(java.lang.String)}.
     */
    @Test
    public void testGetSpecialty() {
        Specialty theSpecialty = myUnitUnderTest.getSpecialty( "J01" );
        assertNotNull( theSpecialty );
        assertEquals( "Java SE", theSpecialty.getName() );
        LOGGER.info( theSpecialty.toString() );
    }

}
