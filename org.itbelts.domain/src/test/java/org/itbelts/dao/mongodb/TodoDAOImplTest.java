/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongodb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.logging.Logger;

import org.itbelts.domain.Todo;
import org.junit.Test;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 22 Oct 2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      22 Oct 2013
 * @author <a href="mailto:koen.bruyndonckx@axa.be"> Koen Bruyndonckx </a>
 */
public class TodoDAOImplTest {

    private TodoDAOImpl myUnitUnderTest = new TodoDAOImpl();
    private static final Logger LOGGER = Logger.getLogger( TodoDAOImplTest.class.getName() );

    @Test
    public void testSaveTodo() {
        Todo theTodo = new Todo();
        theTodo.setDetails( "someDetails" );
        theTodo.setCourseId( "someCourse" );
        theTodo.setExamId( "someExam" );
        myUnitUnderTest.saveTodo( theTodo );

        List<Todo> theList = myUnitUnderTest.getTodosForCourse( "someCourse" );
        LOGGER.info( theList.toString() );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        assertEquals( "someCourse", theList.get(0).getCourseId() );

        theList = myUnitUnderTest.getTodosForExam( "someExam" );
        LOGGER.info( theList.toString() );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        assertEquals( "someExam", theList.get(0).getExamId() );
    }
    
}
