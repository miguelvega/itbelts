/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongolab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.itbelts.dao.mongolab.ExamDAOImpl;
import org.itbelts.domain.Exam;
import org.junit.Test;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 28 Oct 2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      28 Oct 2013
 * @author <a href="mailto:koen.bruyndonckx@axa.be"> Koen Bruyndonckx </a>
 */
public class ExamDAOImplTest {

    private ExamDAOImpl myUnitUnderTest = new ExamDAOImpl();

    /**
     * Test method for {@link org.itbelts.dao.mongolab.ExamDAOImpl#getExams()}.
     */
    @Test
    public void testGetExams() {
        List<Exam> theList = myUnitUnderTest.getExams();
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        System.out.println( theList );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.ExamDAOImpl#getExamsForCommunity(java.lang.String)}.
     */
    @Test
    public void testGetExamsForCommunity() {
        List<Exam> theList = myUnitUnderTest.getExamsForCommunity( "J" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        assertTrue( theList.get(0).getTopicId().startsWith( "J" ) );
        System.out.println( theList );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.ExamDAOImpl#getExam(java.lang.String)}.
     */
    @Test
    public void testGetExam() {
        Exam theExam = myUnitUnderTest.getExam( "J0603-E1" );
        assertNotNull( theExam );
        assertEquals( "Ant - Basic", theExam.getName() );
        System.out.println( theExam );
    }

}
