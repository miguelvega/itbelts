/**
 * Copyright (c) 2013 Axa Holding Belgium, SA. All rights reserved.
 * This software is the confidential and proprietary information of the AXA Group.
 */
package org.itbelts.dao.mongolab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.itbelts.dao.mongolab.SpecialtyDAOImpl;
import org.itbelts.domain.Specialty;
import org.junit.Test;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 22 Oct 2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      22 Oct 2013
 * @author <a href="mailto:koen.bruyndonckx@axa.be"> Koen Bruyndonckx </a>
 */
public class SpecialtyDAOImplTest {

    private SpecialtyDAOImpl myUnitUnderTest = new SpecialtyDAOImpl();

    /**
     * Test method for {@link org.itbelts.dao.mongolab.SpecialtyDAOImpl#getSpecialties()}.
     */
    @Test
    public void testGetSpecialties() {
        List<Specialty> theList = myUnitUnderTest.getSpecialties();
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        System.out.println( theList );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.SpecialtyDAOImpl#getSpecialtiesForCommunity(java.lang.String)}.
     */
    @Test
    public void testGetSpecialtiesForCommunity() {
        List<Specialty> theList = myUnitUnderTest.getSpecialtiesForCommunity( "J" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        for ( Specialty s : theList ) {
            assertEquals( "J", s.getCommunityId() );
        }
        System.out.println( theList );
    }

    /**
     * Test method for {@link org.itbelts.dao.mongolab.SpecialtyDAOImpl#getSpecialty(java.lang.String)}.
     */
    @Test
    public void testGetSpecialty() {
        Specialty theSpecialty = myUnitUnderTest.getSpecialty( "J01" );
        assertNotNull( theSpecialty );
        assertEquals( "Java SE", theSpecialty.getName() );
        System.out.println( theSpecialty );
    }

}
