package org.itbelts.dao.mongolab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.itbelts.dao.mongolab.QuestionDAOImpl;
import org.itbelts.domain.Question;
import org.itbelts.domain.QuestionCategory;
import org.junit.Test;

public class QuestionDAOImplTest {

    private QuestionDAOImpl myUnitUnderTest = new QuestionDAOImpl();
    
    @Test
    public void testGetQuestionsCategoriesForExam() {
        List<QuestionCategory> theList = myUnitUnderTest.getQuestionsCategoriesForExam( "J0515-E1" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        assertEquals( "J0515-E1", theList.get(0).getExamId() );
        System.out.println( theList );
    }

    @Test
    public void testGetQuestionsForCategory() {
        List<Question> theList = myUnitUnderTest.getQuestionsForCategory( "J0515-E1-C01" );
        assertNotNull( theList );
        assertTrue( theList.size() > 0 );
        assertEquals( "J0515-E1-C01", theList.get(0).getQuestionCategoryId() );
        System.out.println( theList );
    }

    @Test
    public void testGetQuestion() {
        Question theQuestion= myUnitUnderTest.getQuestion( "J0515-E1-Q0001" );
        assertNotNull( theQuestion );
        assertEquals( "Primary Key Attributes", theQuestion.getName() );
        System.out.println( theQuestion );
    }

    @Test
    public void testBuildExam() {
        List<Question> theList = myUnitUnderTest.buildExam( "J0515-E1" );
        assertNotNull( theList );
        assertEquals( 20, theList.size() );
        assertTrue( theList.get(0).getQuestionCategoryId().startsWith( "J0515-E1" ) );
        System.out.println( theList );
    }
}
