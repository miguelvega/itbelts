package org.itbelts.migrator;

import static org.junit.Assert.*;

import org.itbelts.migrator.BeltCrawler;
import org.junit.Test;

/**
 * Unit test for the AbstractCrawler class.
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 25 Jan 2013 - Koen Bruyndonckx - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      25 Jan 2013
 * @author <a href="mailto:koen.bruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class AbstractCrawlerTest {

    @Test
    public void testGetElement() {
        assertEquals( "someValue", new BeltCrawler().getElement( "blablabla <someId>someValue</someId>", "<someId>", "</" ) );
    }

}
