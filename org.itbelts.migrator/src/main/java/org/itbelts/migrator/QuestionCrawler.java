package org.itbelts.migrator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.itbelts.domain.AnswerType;
import org.itbelts.domain.PossibleAnswer;
import org.itbelts.domain.extract.QuestionFromImport;
import org.itbelts.domain.extract.QuestionStatus;

import com.thoughtworks.xstream.XStream;

/**
 * Parser for the raw "question html" files <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 * 
 * </pre>
 * 
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class QuestionCrawler extends AbstractCrawler {

    /**
     * Find all .htm files containing "_ contribute" and parse them.
     * 
     * @param aBaseFolder
     * @param aDestinationFolder
     */
    public void doProcessing(File aBaseFolder, String aDestinationFolder) {


        Map<String,List<String>> theExamFiles = new TreeMap<String,List<String>>();

        int theTotal = 0;

        for ( String theFile : aBaseFolder.list() ) {
            int theExamNameEndPos = theFile.indexOf( "_ contribute" );
            if ( theExamNameEndPos == -1 || !theFile.endsWith( ".htm" ) ) {
                continue;
            }

            String theExamName = theFile.substring( 0, theExamNameEndPos );

            List<String> theFiles = theExamFiles.get( theExamName );
            if ( theFiles == null ) {
                theExamFiles.put( theExamName, new ArrayList<String>() );
            }
            theExamFiles.get( theExamName ).add( theFile );
        }

        System.out.println( "Found " + theExamFiles.size() + " exams." );

        for ( String theExamName : theExamFiles.keySet() ) {

            System.out.println( "\nProcessing files for exam " + theExamName + "..." );

            List<String> theFiles = theExamFiles.get( theExamName );

            Map<String,QuestionFromImport> theQuestionMap = new TreeMap<String,QuestionFromImport>();

            int theQuestionCounter = 0;
            int theFileCounter = 0;
            for ( String theFile : theFiles ) {
                StringBuilder theBuffer = loadFile( aBaseFolder, theFile );

                // First skip some of the header stuff
                String theQuestionStartMarker = "doubleBorderTop doubleBorderTop\">";
                int theContentPos = theBuffer.indexOf( theQuestionStartMarker );
                if ( theContentPos == -1 ) {
                    continue;
                }

                theFileCounter++;

                String theHtml = theBuffer.substring( theContentPos );

                String[] theQuestions = theHtml.split( theQuestionStartMarker );

                for ( String theQuestion : theQuestions ) {
                    if ( theQuestion.trim().length() == 0 ) {
                        continue;
                    }

                    theQuestionCounter++;

                    QuestionFromImport q = new QuestionFromImport();

                    q.setTitle( getElement( theQuestion, "<span class=\"v-button-caption\">", "</span>" ) );
                    theQuestion = skip( theQuestion, "<span class=\"v-button-caption\">", "</span>" );

                    String theStatus = getVariableElement( theQuestion, " smallText", "</div>" ).substring( 0, 1 );
                    try {
                        q.setStatus( QuestionStatus.valueOf( theStatus ) );
                    } catch ( IllegalArgumentException e ) {
                        System.out.println( "Skipping question " + q.getTitle() + " because of unsupported status " + theStatus );
                        continue;
                    }
                    theQuestion = skip( theQuestion, " smallText", "</div>" );

                    q.setCategory( getElement( theQuestion, " smallText\">", "</div>" ) );
                    theQuestion = skip( theQuestion, " smallText\">", "</div>" );

                    q.setId( getElement( theQuestion, "<a href=\"#Question/", "\">" ) );
                    theQuestion = skip( theQuestion, "<a href=\"#Question/", "\">" );

                    q.setText( getElement( theQuestion, "<div style=\"width: 465px;\" class=\"v-label\">", "</div>" ) );

                    processImages( q, theExamName, aBaseFolder, aDestinationFolder );

                    theQuestion = skip( theQuestion, "<div style=\"width: 465px;\" class=\"v-label\">", "</div>" );

                    theQuestion = skip( theQuestion, "<b>Possible Correct Answer(s)</b>", "</div>" );

                    String theTextAnswer = getElement( theQuestion, "smallArrowLayout\">", "</div" );
                    while ( theTextAnswer.length() > 0 ) {
                        PossibleAnswer p = new PossibleAnswer();
                        p.setText( removeParagraph( theTextAnswer ) );
                        p.setType( AnswerType.Text );
                        p.setCorrect( true );

                        q.getAnswers().add( p );

                        theQuestion = skip( theQuestion, "smallArrowLayout\">", "</div>" );
                        theTextAnswer = getElement( theQuestion, "smallArrowLayout\">", "</div" );
                    }

                    int theCbbPos = theQuestion.indexOf( "questionPanelAnswerLine\">" );
                    while ( theCbbPos > 0 ) {
                        theQuestion = skip( theQuestion, "questionPanelAnswerLine\">", CRLF + "<div>" );

                        String theCompleteAnswer = getElement( theQuestion, "<div style=\"width: 20px;\"",
                                "</div></div></div></div></div></div>" );
                        if ( theCompleteAnswer.length() == 0 ) {
                            break;
                        }
                        theQuestion = skip( theQuestion, "<div style=\"width: 20px;\"", "</div></div></div></div></div></div>" );

                        PossibleAnswer p = new PossibleAnswer();

                        int theTextPos = theCompleteAnswer.indexOf( "questionPanelAnswerText\">" );
                        if ( theTextPos > 0 ) {
                            p.setText( removeParagraph( theCompleteAnswer.substring( theTextPos + 25 ) ) );
                        } else {
                            theTextPos = theCompleteAnswer.indexOf( "floatLeftLayoutNoMargin\">" );
                            if ( theTextPos > 0 ) {
                                p.setText( removeParagraph( theCompleteAnswer.substring( theTextPos + 25 ) ) );
                            } else {
                                throw new IllegalStateException( "Answer text for question " + q + " could not be extracted from "
                                        + theCompleteAnswer );
                            }
                        }
                        if ( theCompleteAnswer.indexOf( "/box.gif" ) > 0 ) {
                            p.setType( AnswerType.CheckBox );
                        } else {
                            p.setType( AnswerType.RadioButton );
                        }
                        if ( theCompleteAnswer.indexOf( "/correct_blue.gif" ) > 0 ) {
                            p.setCorrect( true );
                        } else {
                            p.setCorrect( false );
                        }

                        q.getAnswers().add( p );

                        theCbbPos = theQuestion.indexOf( "questionPanelAnswerLine\">" );
                    }

                    theQuestion = skip( theQuestion, "<b>Explanation:</b>", "</div>" );

                    q.setExplanation( getElement( theQuestion, "<div style=\"width: 465px;\" class=\"v-label\">", "</div>" ) );
                    theQuestion = skip( theQuestion, "<div style=\"width: 465px;\" class=\"v-label\">", "</div>" );

                    if ( q.getAnswers().size() == 0 ) {
                        System.out.println( q );
                    }

                    theQuestionMap.put( q.getId(), q );
                }
            }
            System.out.println( "Processed " + theQuestionCounter + " questions from " + theFileCounter + " files." );
            theTotal += theQuestionCounter;

            XStream x = new XStream();
            try {
                BufferedWriter w = new BufferedWriter( new FileWriter( new File( aDestinationFolder
                        + theExamName.replace( " ", "_" ) + "_questions.xml" ) ) );
                w.write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" );
                w.write( CRLF );
                w.write( "<java.util.ArrayList>" );
                w.write( CRLF );

                for ( Map.Entry<String,QuestionFromImport> e : theQuestionMap.entrySet() ) {
                    w.write( x.toXML( e.getValue() ) );
                    w.write( CRLF );
                }

                w.write( "</java.util.ArrayList>" );
                w.write( CRLF );

                w.flush();
                w.close();
            } catch ( IOException exc ) {
                exc.printStackTrace( System.out );
            }
        }
        System.out.println( "Processed " + theTotal + " questions." );
    }

    private String removeParagraph( String aText ) {
        if ( aText.startsWith( "<p>" ) ) {
            aText = aText.substring( 3 );
            if ( aText.endsWith( "</p>" ) ) {
                aText = aText.substring( 0, aText.length() - 4 );
            }
        }
        return aText;
    }

    /**
     * Try to see if there are image links in the question text. If there are,
     * try to find the images and save them.
     * 
     * @param aQuestion
     *            The question
     * @param anExamName
     *            The name of the exam to use in the output image name
     * @param aBaseFolder
     *            The base folder with the site pages
     * @param aDestinationFolder
     *            The destination folder for the images
     */
    private void processImages( QuestionFromImport aQuestion, String anExamName, File aBaseFolder, String aDestinationFolder) {

        int theCount = 0;

        String theImgLinkTxt = "<div style=\"overflow:auto\" align=\"center\"><img src=\"";
        String theImgEndTxt = "\" align=\"middle\">";
        
        String theQuestion = aQuestion.getText();
        int thePos = theQuestion.indexOf( theImgLinkTxt );
        while ( thePos >= 0 ) {
            int theEnd = theQuestion.indexOf( theImgEndTxt, thePos );
            String theImgName = theQuestion.substring( thePos + theImgLinkTxt.length(), theEnd );
            theImgName = theImgName.replace( "%20", " " );
            theImgName = theImgName.replace( "%28", "(" );
            theImgName = theImgName.replace( "%29", ")" );
            theImgName = theImgName.replace( "&amp;", "&" );

            String theNewImgName = anExamName + "_" + aQuestion.getId() + "_" + theCount + theImgName.substring( theImgName.indexOf( "." ) );
            File theImgFile = new File( aBaseFolder + File.separator + theImgName );
            
            if ( !theImgFile.exists() ) {
                System.out.println( theImgName + " DOES NOT EXIST !!" );
            } else {
                theCount++;
                try {
                    Path source = Paths.get( theImgFile.getAbsolutePath() );
                    Path target = Paths.get( aDestinationFolder + theNewImgName );
                    Files.copy( source, target, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES );
                } catch ( IOException e ) {
                    System.out.println( theImgFile + " COULD NOT BE COPIED" );
                }
            }

            theEnd += theImgEndTxt.length(); 
            theQuestion = theQuestion.substring( 0, thePos ) + "<img src=\"http://users.skynet.be/am285330/itbelts/img/" + theNewImgName + "\" />" + theQuestion.substring( theEnd );
            thePos = theQuestion.indexOf( theImgLinkTxt );
        }
        aQuestion.setText( theQuestion );
    }

}
