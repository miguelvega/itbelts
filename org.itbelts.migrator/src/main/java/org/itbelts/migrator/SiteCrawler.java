package org.itbelts.migrator;

import java.io.File;

/**
 *  This is the main crawler for the KBB site info.<br>
 *  These classes are expecting the files to be in the following structure:<br>
 * <pre>
 * someBaseFolder  ( currently d:/data/KBB/ )
 *     /Site        all of the saved exam pages, question pages and belt pages.
 *     /Users       all of the saved user pages.
 * </pre>
 * Output will be stored in:
 * <pre>
 * someBaseFolder  ( currently d:/data/KBB/ )
 *     /_Extract    all of generated XML files with the raw domain data
 * </pre>
 *  
 * <br>
 * <u><i>Version History</i></u>
 * <pre>
 * 14 Jan 2013 - initial release
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class SiteCrawler {

    /**
     * @param args
     */
    public static void main( String[] args ) {
        String theBaseFolder = "D:/data/KBB/";
        String theDestinationFolder = theBaseFolder + "_Extract/";
        
        new File( theDestinationFolder ).mkdirs();

        String theSiteFolder = theBaseFolder + "Site";
//        new BeltCrawler().process( theSiteFolder, theDestinationFolder );
        new QuestionCrawler().process( theSiteFolder, theDestinationFolder );
//        new ExamCrawler().process( theSiteFolder, theDestinationFolder );
        
//        String theUserFolder = theBaseFolder + "Users";
//        new UserCrawler().process( theUserFolder, theDestinationFolder );
    }

}
