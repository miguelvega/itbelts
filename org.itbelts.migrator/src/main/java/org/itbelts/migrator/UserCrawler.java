package org.itbelts.migrator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Parser for user information. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 25 Jan 2013 - initial release
 * 
 * </pre>
 * 
 * @version 25 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class UserCrawler extends AbstractCrawler {

    /**
     * Find all files ending in "Belts.htm" and parse them.
     * 
     * @param aBaseFolder
     * @param aDestinationFolder
     */
    public void doProcessing( File aBaseFolder, String aDestinationFolder ) {

        try {
            BufferedWriter w = new BufferedWriter( new FileWriter( new File( aDestinationFolder + "userinfo.csv" ) ) );

            for ( String theFile : aBaseFolder.list() ) {
                if ( !theFile.endsWith( ".htm" ) ) {
                    continue;
                }
    
                String theName = theFile.substring( 0, theFile.indexOf( "." ) );
                System.out.println( "Processing " + theName + "..." );
    
                String theBuffer = loadFile( aBaseFolder, theFile ).toString();
    
                String theUserId = getElement( theBuffer, "Username: ", "<" );
                String theEmail = getElement( theBuffer, "e-mail: ", "<" );
                String theSkypeId = getElement( theBuffer, "skype-id: ", "<" );
                String theLastLogin = getElement( theBuffer, "Last Login ", " ago" );
            
                w.write( theUserId );
                w.write( ";" );
                w.write( theEmail );
                w.write( ";" );
                w.write( theSkypeId );
                w.write( ";" );
                w.write( theLastLogin );
                w.write( ";" );
                w.write( CRLF );
    
            }

            w.flush();
            w.close();
        } catch ( IOException exc ) {
            exc.printStackTrace( System.err );
        }
    }

}
