package org.itbelts.migrator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.itbelts.domain.ExamStatus;
import org.itbelts.domain.extract.ExamCategory;
import org.itbelts.domain.extract.ExamContributorFromImport;
import org.itbelts.domain.extract.ExamFromImport;

import com.thoughtworks.xstream.XStream;

/**
 * Parser for the raw "exam html" files <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 14 Jan 2013 - initial release
 * 
 * </pre>
 * 
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ExamCrawler extends AbstractCrawler {

    /**
     * Find all .htm files containing "_ exam content details" and parse them.
     * 
     * @param aBaseFolder
     * @param aDestinationFolder
     */
    public void doProcessing( File aBaseFolder, String aDestinationFolder ) {

        Map<String, ExamFromImport> theExamMap = new TreeMap<String, ExamFromImport>();

        Map<String, List<String>> theExamFiles = new TreeMap<String, List<String>>();

        for ( String theFile : aBaseFolder.list() ) {
            int theExamNameEndPos = theFile.indexOf( " _ exam content details" );
            if ( theExamNameEndPos == -1 || !theFile.endsWith( ".htm" ) ) {
                continue;
            }

            String theExamName = theFile.substring( 0, theExamNameEndPos );

            List<String> theFiles = theExamFiles.get( theExamName );
            if ( theFiles == null ) {
                theExamFiles.put( theExamName, new ArrayList<String>() );
            }
            theExamFiles.get( theExamName ).add( theFile );
        }

        System.out.println( "Found " + theExamFiles.size() + " exams." );

        for ( String theExamName : theExamFiles.keySet() ) {

            System.out.println( "\nProcessing files for exam " + theExamName + "..." );

            List<String> theFiles = theExamFiles.get( theExamName );

            for ( String theFile : theFiles ) {
                StringBuilder theBuffer = loadFile( aBaseFolder, theFile );

                // First skip some of the header stuff
                String theExamStartMarker = "<div style=\"width: 316px;\" class=\"v-label\">";
                int theContentPos = theBuffer.indexOf( theExamStartMarker );
                if ( theContentPos == -1 ) {
                    continue;
                }

                String theHtml = theBuffer.substring( theContentPos + theExamStartMarker.length() );

                ExamFromImport theExam = new ExamFromImport();
                theExam.setId( theExamName );
                
                theExam.setObjectives( theHtml.substring( 0, theHtml.indexOf( "</div>" ) ) );

                theHtml = theHtml.substring( theHtml.indexOf( "</div>" ) );
                theHtml = theHtml.substring( theHtml.indexOf( "Categories" ) );

                String theCategoryKey = "div style=\"width: 316px;\" class=\"v-label\">";
                String theCategory = getElement( theHtml, theCategoryKey, "<span" );
                while ( theCategory.length() > 0 ) {
                    ExamCategory cat = new ExamCategory();
                    cat.setName( theCategory );
                    cat.setQuestions( Integer.valueOf( getElement( theHtml, "smallText\"> (", " question" ) ) );
                    theHtml = skip( theHtml, "smallText\"> (", " question" );

                    cat.setObjectives( readObjectivesFromFile( aBaseFolder, theFile, theCategory ) );
                    theExam.getCategories().add( cat );

                    theCategory = getElement( theHtml, theCategoryKey, "<span" );
                }

                theHtml = theHtml.substring( theHtml.indexOf( "Exam Details" ) );

                String theDetailKey = "<div style=\"width: 286px;\" class=\"v-label\">";
                // Skip the first detail (=number of questions)
                theHtml = skip( theHtml, theDetailKey, "</div>" );

                theExam.setMinutes( Integer.valueOf( getElement( theHtml, theDetailKey, " minutes" ) ) );
                theHtml = skip( theHtml, theDetailKey, " minutes" );

                theExam.setPercentage( Integer.valueOf( getElement( theHtml, theDetailKey, "%" ) ) );
                theHtml = skip( theHtml, theDetailKey, "%" );

                theExam.setKnowledgePoints( Integer.valueOf( getElement( theHtml, theDetailKey + "+", " knowledge" ) ) );
                theHtml = skip( theHtml, theDetailKey + "+", " knowledge" );

                // Skip the cost detail
                theHtml = skip( theHtml, theDetailKey, "</div>" );

                String theStatus = getElement( theHtml, theDetailKey + "Exam status : ", "</div>" );
                if ( "Released".equals( theStatus ) ) {
                    theExam.setStatus( ExamStatus.Released );
                }
                theHtml = skip( theHtml, theDetailKey + "Exam status : ", "</div>" );

                
                String theContribKey = "<a href=\"#%21User/";

                String theContrib = getElement( theHtml, theContribKey, "</a>" );
                while ( theContrib.length() > 0 ) {
                    ExamContributorFromImport con = new ExamContributorFromImport();
                    con.setId( theContrib.substring( 0, theContrib.indexOf( "\">" ) ) );
                    con.setName( theContrib.substring( theContrib.indexOf( "\">" ) + 2 ) );
                    
                    theHtml = skip( theHtml, theContribKey, "</a>" );

                    String thePoints = getElement( theHtml, "line-height:15px\">", " points" );
                    if ( thePoints.trim().length() == 0 ) {
                        break;
                    }
                    con.setPoints( Double.valueOf( thePoints ) );
                    theHtml = skip( theHtml, "line-height:15px\">", " points" );
                    con.setPercentage( Double.valueOf( getElement( theHtml, "<br>", "%" ) ) );

                    theExam.getContributors().add( con );

                    theContrib = getElement( theHtml, theContribKey, "</a>" );
                }
                
                theExamMap.put( theExam.getId(), theExam );
            }

            XStream x = new XStream();
            try {
                BufferedWriter w = new BufferedWriter( new FileWriter( new File( aDestinationFolder + "exams.xml" ) ) );
                w.write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" );
                w.write( CRLF );
                w.write( "<java.util.ArrayList>" );
                w.write( CRLF );

                for ( Map.Entry<String, ExamFromImport> e : theExamMap.entrySet() ) {
                    w.write( x.toXML( e.getValue() ) );
                    w.write( CRLF );
                }

                w.write( "</java.util.ArrayList>" );
                w.write( CRLF );

                w.flush();
                w.close();
            } catch ( IOException exc ) {
                exc.printStackTrace( System.out );
            }
        }
    }

    /**
     * Open the file "XXX _ exam detailed objectives.htm" for the given "XXX _ exam content details.htm" and read the objectives for the given category.
     * @param aBaseFolder
     * @param aFile
     * @param aCategory
     * @return String
     */
    private String readObjectivesFromFile( File aBaseFolder, String aFile, String aCategory ) {
        
        try {
            String theFile = aFile.replace(  "exam content details", "exam detailed objectives" );
            StringBuilder theBuffer = loadFile( aBaseFolder, theFile );

            // First skip to the start of the category
            String theStartMarker = "v-label-undef-w\">" + aCategory;
            int theContentPos = theBuffer.indexOf( theStartMarker );
            if ( theContentPos == -1 ) {
                return null;
            }

            String theHtml = theBuffer.substring( theContentPos + theStartMarker.length() );
            
            String theDetailKey = "<div style=\"width: 980px;\" class=\"v-label\">";
            return getElement( theHtml, theDetailKey, "</div></div></div></div></div></div>" );
            
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        return null;
    }

}
