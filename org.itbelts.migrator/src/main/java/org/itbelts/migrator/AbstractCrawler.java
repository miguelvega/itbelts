package org.itbelts.migrator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Base class for the various parsers.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * 14 Jan 2013 - initial release
 *
 * </pre>
 *
 * @version 14 Jan 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public abstract class AbstractCrawler {

    protected static final String CRLF = System.getProperty( "line.separator" );
    
    /**
     * Override this one in the subclasses.
     * @param aBaseFolder          The base folder that holds the source html files.
     * @param aDestinationFolder   Destination folder that will hold the resuting XML's
     */
    public abstract void doProcessing( File aBaseFolder, String aDestinationFolder );

    
    /**
     * Process a given folder for all relevant files.
     * 
     * @param aBaseFolder
     * @param aDestinationFolder
     */
    public void process( String aBaseFolder, String aDestinationFolder ) {
        
        File theFolder = new File( aBaseFolder );
        if ( !theFolder.exists() ) {
            throw new IllegalArgumentException( "Base folder " + aBaseFolder + " does not exist.  The folder should exist and contain the KBB belt html files." );
        }

        doProcessing( theFolder, aDestinationFolder );
    }

    
    /**
     * Load a given file into a StringBuilder object.
     * 
     * @param aBaseFolder
     * @param aFileName
     * @return StringBuilder
     */
    protected StringBuilder loadFile( File aBaseFolder, String aFileName ) {
        
        File theFile = new File( aBaseFolder + File.separator + aFileName );
        if ( theFile.isDirectory() ) {
            return new StringBuilder();
        }
        
        try {
            BufferedReader r = new BufferedReader( new FileReader( theFile ) );
            StringBuilder theBuffer = new StringBuilder();
            String s;
            while ( ( s = r.readLine() ) != null ) {
                theBuffer.append( s );
            }
            s = null;
            r.close();
            
            return theBuffer;
        } catch ( IOException e ) {
            e.printStackTrace();
            return new StringBuilder();
        }
    }

    /**
     * Get a piece of text between a start and an ending tag.
     * 
     * @param aText
     * @param aStartTag
     * @param anEndTag
     * @return String
     */
    protected String getElement( String aText, String aStartTag, String anEndTag ) {
        
        String thePiece = "";
        
        int thePos = aText.indexOf( aStartTag );
        if ( thePos >=0 ) {
            thePiece = aText.substring( thePos + aStartTag.length(), aText.indexOf( anEndTag, thePos ) );
        }
        return thePiece;
    }

    /**
     * Get a piece of text between a start and an ending tag.  This method will look for the first '>' after the given start token.
     * 
     * @param aText
     * @param aStartTag
     * @param anEndTag
     * @return String
     */
    protected String getVariableElement( String aText, String aStartTag, String anEndTag ) {
        
        String thePiece = "";
        
        int thePos = aText.indexOf( aStartTag );
        if ( thePos >=0 ) {
            thePos = aText.indexOf( ">", thePos ) + 1;
            thePiece = aText.substring( thePos, aText.indexOf( anEndTag, thePos ) );
        }
        return thePiece;
    }
    
    /**
     * Skip the piece of text between a start and an ending tag.
     * 
     * @param aText
     * @param aStartTag
     * @param anEndTag
     * @return String
     */
    protected String skip( String aText, String aStartTag, String anEndTag ) {
        
        int thePos = aText.indexOf( anEndTag, aText.indexOf( aStartTag ) );
        if ( thePos >=0 ) {
            return aText.substring( thePos + anEndTag.length() );
        }
        return aText;
    }

}
