Welcome to the source repository for the ITBelts application.
=============================================================

Refer to the wiki on this site to find all available documentation.


This is a small overview of the folders in this repository:

FOLDER                    CONTENTS
=============================================================

_documentation            An overview image of the application architecture and the database structure.

AppEngine folder          The web application that is deployed in Google App Engine.
                          Development is done using Eclipse and the Google App Engine plugin.

Courses folder            All the original course material.  This cannot be used as-is yet because of the references to KnowledgeBlackBelt

org.itbelts.domain        The java library that describes the domain model and has the DAO's.  (it is used in the web application)

org.itbelts.admin         Small utilities that are used for loading the database with the migrated information.
org.itbelts.migrator 

org.itbelts.ROOT          The root pom with common dependencies and build characteristics

