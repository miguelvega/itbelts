var keepAliveImage = null;
function KeepAlive(){
  if (!keepAliveImage) {
    keepAliveImage = document.createElement("img");
    keepAliveImage.setAttribute("src", "/img/spacer.gif");
    keepAliveImage.style.position = "absolute";
    keepAliveImage.style.height = "1px";
    keepAliveImage.style.width = "1px";
    document.appendChild(keepAliveImage);
  } else {
    var d = new Date();
    var rand = d.getTime();
    keepAliveImage.src = "/img/spacer.gif?d=" + rand;
  }
}
setInterval('KeepAlive();', '300000');