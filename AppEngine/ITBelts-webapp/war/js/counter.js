function timer(id, e) {
	var n = new Date();
	display(+n, id, e);
	setTimeout("if(typeof timer=='function'){timer('"+id+"',"+e+")}",1100-n.getMilliseconds())
};
function display(n, id, e) {
	var ms = e - n;
	if (ms <= 0) ms *= -1;
	var d = Math.floor(ms / 864E5);
	ms -= d * 864E5;
	var h = Math.floor(ms / 36E5);
	ms -= h * 36E5;
	var m = Math.floor(ms / 6E4);
	ms -= m * 6E4;
	var s = Math.floor(ms / 1E3);
	if (counter_elements[id]) {
		counter_elements[id].innerHTML = m + "m " + padZero(s) + "s";
		if (m<1) {
			counter_elements[id].style.backgroundColor="red";
			counter_elements[id].style.color="white";
			counter_elements[id].style.fontWeight="bold";
			counter_elements[id].style.textDecoration="blink";
		}
	}
};
function padZero(i) {
	return (i < 10 ? "0" + i : i)
};
function init() {
	var pref = "counter";
	var objH = 1;
	if (document.getElementById || document.all) {
		for ( var i = 1; objH; ++i) {
			var id = pref + i;
			objH = document.getElementById ? document.getElementById(id)
					: document.all[id];
			if (objH && (typeof objH.innerHTML) != 'undefined') {
				var s = objH.innerHTML;
				var dt = parse(s);
				if (!isNaN(dt)) {
					counter_elements[id] = objH;
					timer(id, dt.valueOf());
					if (objH.style) {
						objH.style.visibility = "visible"
					}
				} else {
					objH.innerHTML = s
				}
			}
		}
	}
};
function parse(strDate) {
	var objReDte = /(\d{4})\-(\d{1,2})\-(\d{1,2})\s+(\d{1,2}):(\d{1,2}):(\d{0,2})/;
	if (strDate.match(objReDte)) {
		var d = new Date(0);
		d.setUTCFullYear(+RegExp.$1, +RegExp.$2 - 1, +RegExp.$3);
		d.setUTCHours(+RegExp.$4, +RegExp.$5, +RegExp.$6);
		;
		return d
	} else {
		return NaN
	}
};
var counter_elements = new Object();
if (window.attachEvent) {
	window.attachEvent('onload', init)
} else if (window.addEventListener) {
	window.addEventListener("load", init, false)
} else {
	window.onload = init
};
