<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="/WEB-INF/jsp/exception.jsp" %>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="utf-8">
<title><%= request.getParameter("title") %></title>
<link rel="icon" type="image/png" href="/img/favicon.png">

<link rel="stylesheet" href="/css/jquery.css" type="text/css" />
<link rel="stylesheet" href="/css/itbelts.css" type="text/css" />
<link rel="stylesheet" href="/css/jhighlight.css" type="text/css" />

<!--[if lt IE 9]><link rel="stylesheet" href="/css/aui-ie.css" media="all"><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="/css/aui-ie9.css" media="all"><![endif]-->
<!--[if IE]><link rel="stylesheet" href="/css/aui-overrides-ie.css" media="all"><![endif]-->
<meta name="description"
	content="ITBelts is a free IT-related education and certification site. Take courses and exams to hone your skills." />
</head>
