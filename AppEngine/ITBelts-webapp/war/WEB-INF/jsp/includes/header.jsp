<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<header id="header">
	<nav class="aui-header aui-dropdown2-trigger-group">
		<div class="aui-header-inner">
			<div class="aui-header-primary">
				<h1 class="aui-header-logo aui-header-logo-itbelts">
					<a href="/" class="aui-nav-imagelink" id="logo-link"><span class="aui-header-logo-device">ITBelts</span>&nbsp;I.T. Belts</a>
				</h1>
			</div>
			<div class="aui-header-secondary menu-link">
				<ul class="aui-nav">
					<li><a href="/content-index.htm"> Courses and Exams</a>
<%
    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    String theAvatarUrl = (String) session.getAttribute("avatar-url");
    if ( user == null || theAvatarUrl == null ) {
%>
                    <li><a href="<%= userService.createLoginURL("/loggedin.htm") %>">Log in</a>
<%
    } else {
%>
                    <li><a href="/profile.htm">Profile</a>

                    <li><a href="/logout.htm">Log out</a>
                    <li><img class="avatar32" src="<%= theAvatarUrl %>&s=32"/>
<%
    }
%>
				</ul>
			</div>
		</div>
	</nav>
</header>
