<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<footer id="footer">
	<section class="footer-body">
	
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="4FP2H84ECURHL">
			<input type="image" src="https://www.paypalobjects.com/en_US/BE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
		</form>

		<ul>
			<li><a href="/support.htm">Support</a>
			<li><a href="/documentation.htm">Documentation</a>
			<li><a href="/terms-of-service.htm">Terms of service</a>
			<li><a href="/privacy-policy.htm">Privacy policy</a>
		</ul>
		<%
		    if ( request.getParameter( "tm" ) != null && request.getParameter( "tm" ).equals( "true" ) ) {
		%>
		<br />
		<div class="copyright">Oracle and Java are registered trademarks of Oracle and/or its affiliates. Other names may be trademarks of their respective owners.</div>
		<%
    }
%>
	</section>
</footer>

