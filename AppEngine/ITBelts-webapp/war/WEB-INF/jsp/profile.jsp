<%@ page import="org.itbelts.app.controller.form.ProfileForm" %>
<%@ page import="org.itbelts.domain.RoleType" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
	<jsp:param name="title" value="ITBelts Profile" />
</jsp:include>

<body>

	<div id="page">
		<div id="wrapper">

			<jsp:include page="includes/header.jsp" />

		    <%
		    ProfileForm theFormData = (ProfileForm) request.getAttribute("profileForm");
		    %>
			<div id="content">

				<div id="features">
				     
					<form:form id="profile" commandname="profile" method="post" modelAttribute="profileForm">
						<fieldset>
						    <table>
							    <tr height="20px">
									<td><b>Email address from google account:</b></td>
									<td><%= theFormData.getEmail() %></td>
							    </tr>
							    <tr>
									<td><b>Your avatar image (provided by <%= session.getAttribute("avatar-url-provider") %>):</b></td>
									<td><img src="<%= session.getAttribute("avatar-url") %>"/></td>
							    </tr>
							    <tr>
									<td><b><form:label path="name">Name:</form:label></b></td>
									<td><form:input path="name" size="100%"/></td>
							    </tr>
							    <tr>
									<td><b><form:label path="country">Country:</form:label></b></td>
									<td><form:select path="country" items="${countryMap}"></form:select></td>
							    </tr>
							    <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
							    </tr>
							    <tr>
									<td>&nbsp;</td>
									<td><button type="submit" name="action" value="save">Save information</button></td>
							    </tr>
							</table>
						</fieldset>
						
						<%
						if ( theFormData.getRoles().contains( RoleType.ADMIN )) {
						%>
						    <br>
							<br>
							<div>
							    <table>
								    <tr height="20px">
										<th colspan="99" align="left">Administrative actions</th>
								    </tr>
								    <tr height="20px">
										<td><button type="submit" name="action" value="todo-list">Manage todo's</button></td>
										<td><button type="submit" name="action" value="roles-list">Manage roles</button></td>
								    </tr>
								</table>
							</div>
						<%
						}
						%>
					</form:form>
				</div>

			</div>
		</div>

		<jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true" />
		</jsp:include>
	</div>


</body>
</html>


