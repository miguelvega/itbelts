<%@ page import="org.itbelts.domain.Course" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Course"/>
</jsp:include>

<body>
	<div id="page">
		<div id="wrapper">
            <jsp:include page="includes/header.jsp"/>
<%
    Course theCourse = (Course) request.getAttribute( "course" );
    String theType = theCourse.get_id().startsWith( "C" ) ? "course" : "workshop";
%>
			<div id="content">
			    <div class="col75">
					<div class="plain-content">
						<p></p>
						<h1><%= theCourse.getName() %></h1>
						<p></p>
					    <%= theCourse.getDescription() %>
					</div>
				</div>
			    <div class="col25">
			    	<div class="launch-box">
						<form action="/course-content.htm?id=<%= theCourse.get_id() %>" method="post">
							<input type="submit" value="Access this <%= theType %>" style="width:100%">
						</form>
						<p></p>
						<p>This <%= theType %> is <%= theCourse.getStatusDescription() %></p>
						<p><a href="/documentation.htm">What is a <%= theType %>?</a></p>
						<p></p>
			    		<a href="#" onclick="history.back();">Back to overview</a>
			    	</div>
				</div>
			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>
</body>
</html>
