
<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="Your IT education and certification site"/>
</jsp:include>

<body class="homepage">
	<div id="page">
		<div id="wrapper">
            <jsp:include page="includes/header.jsp"/>

			<div id="content">
				<div id="homepage" class="plain-content">
					<div class="intro">
						<h1>Test your knowledge and certify your skills</h1>
						<ul class="feature-list">
							<li>Access course material
							<li>Hire a coach
							<li>Pass your exams
							<li>Earn your belts
						</ul>
					</div>
				</div>
			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="false"/>
		</jsp:include>
	</div>
</body>
</html>
