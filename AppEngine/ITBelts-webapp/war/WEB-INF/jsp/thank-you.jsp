
<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Thanks You"/>
</jsp:include>

<body>

	<div id="page">
		<div id="wrapper">

            <jsp:include page="includes/header.jsp"/>

			<div id="content">

				<div class="plain-content">
					<p></p>

					<h1>Thank You for your support to this community.</h1>

					<h3>Your contribution is being put to use to help cover the hosting fees for this site.</h3>

					<p></p>

				</div>

			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>


</body>
</html>

