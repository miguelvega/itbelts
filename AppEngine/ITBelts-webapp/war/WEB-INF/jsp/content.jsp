<%@ page import="java.util.List" %>
<%@ page import="org.itbelts.domain.Community" %>
<%@ page import="org.itbelts.domain.Course" %>
<%@ page import="org.itbelts.domain.Specialty" %>
<%@ page import="org.itbelts.app.view.TopicView" %>
<%@ page import="org.itbelts.app.view.ItemLink" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Content"/>
</jsp:include>

<body>

	<div id="page">
		<div id="wrapper">

            <jsp:include page="includes/header.jsp"/>

<%
    Community theCommunity = (Community) request.getAttribute( "community" );
%>

			<div id="content">

				<div class="plain-content">
					<p></p>
					<h1>Specialties for <%= theCommunity.getName() %></h1>
					<p></p>

					<table id="topics">
		<%
		    List<TopicView> theData = (List<TopicView>) request.getAttribute( "topics" );
			String thePreviousSpecialty = "---";
		    for ( TopicView v : theData ) {
		        if ( !v.getSpecialty().getName().equals( thePreviousSpecialty ) ) {
		            thePreviousSpecialty = v.getSpecialty().getName();
		%>
		                    <tr>
		                        <td><img src="/img/green-arrow.jpg"/></td>
		                        <td colspan="99" class="specialty"><%= v.getSpecialty().getName() %></td>
		                    </tr>
		<%            
		        }
		%>
							<tr>
							    <td>&nbsp;</td>
							    <td><img src="/img/blue-arrow.jpg"/></td>
							    <td><%= v.getTopic().getName() %></td>
		<%
		    	for ( ItemLink l : v.getLinks() ) {
		%>
							    <td><div class="pad10"><span class="<%= l.getLinkType() %>"><a href="<%= l.getUrl() %>"><%= l.getName() %></a></span></div></td>
        <%            
		    	}
		%>
							</tr>
		<%            
		    }
		%>
					</table>
				</div>

			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>


</body>
</html>
