
<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="Hiring a coach"/>
</jsp:include>

<body>

	<div id="page">
		<div id="wrapper">

            <jsp:include page="includes/header.jsp"/>

			<div id="content">

				<div class="plain-content">
					<p></p>

					<h2>Hiring a coach</h2>

					<p>Didn't get access to the course or workshop of your choice?</p>
					<p>It's time to hire a coach</p>

				</div>

			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="false"/>
		</jsp:include>
	</div>


</body>
</html>

