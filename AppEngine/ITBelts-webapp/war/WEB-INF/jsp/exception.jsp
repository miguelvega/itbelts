<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isErrorPage="true" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>

<!DOCTYPE html>
<html lang="en">
<!--  THIS section is copied from the head.jsp  For some reason JSP stuff is not working in this special exception jsp -->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="utf-8">
<title>Oops...</title>
<link rel="icon" type="image/png" href="/img/favicon.png">

<link rel="stylesheet" href="/css/jquery.css" type="text/css" />
<link rel="stylesheet" href="/css/itbelts.css" type="text/css" />

<!--[if lt IE 9]><link rel="stylesheet" href="/css/aui-ie.css" media="all"><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="/css/aui-ie9.css" media="all"><![endif]-->
<!--[if IE]><link rel="stylesheet" href="/css/aui-overrides-ie.css" media="all"><![endif]-->
<meta name="description"
	content="ITBelts is a free IT-related education and certification site. Take courses and exams to hone your skills." />
</head>

<body>

	<div id="page">
		<div id="wrapper">

			<header id="header">
				<nav class="aui-header aui-dropdown2-trigger-group">
					<div class="aui-header-inner">
						<div class="aui-header-primary">
							<h1 class="aui-header-logo aui-header-logo-itbelts">
								<a href="./" class="aui-nav-imagelink" id="logo-link"> <span
									class="aui-header-logo-device">ITBelts</span>&nbsp;I.T. Belts
								</a>
							</h1>
						</div>
					</div>
				</nav>
			</header>

			<div id="content">

				<div id="features">
					<ul class="feature-list">
						<li class="feature-item clearfix">
							<div class="feature-image">
								<img src="/img/404.jpg">
							</div>
							<div class="feature-text">
								<h2>
									This page could not be shown
								</h2>

								<h3>This might be because:</h3>
								<ul>
									<li>You have typed the web address incorrectly, or
									<li>the page you were looking for may have been moved, updated or deleted.
								</ul>
								<h3>You could just go back or <a href="/">go to our home page</a>.</h3>

								<h3>If you feel this is a serious problem we need to look into:</h3>
								<ul>
									<li>Feel free to request support using the link at the bottom of this page.
								</ul>

							</div>
							<div id="exception-details">
								<div id="exception-text">${pageContext.exception.message}</div>
<%
							    if ( UserServiceFactory.getUserService().isUserLoggedIn() && UserServiceFactory.getUserService().isUserAdmin() ) {
%>
									<div id="exception-trace">
										<c:forEach var="trace" items="${pageContext.exception.stackTrace}">
											<p>${trace}</p>
										</c:forEach>
									</div>	
<%
							    }
%>
							</div>
					</ul>
				</div>

			</div>
		</div>

		<footer id="footer">
			<section class="footer-body">
				<ul>
					<li><a href="/support.htm">Support</a>
					<li><a href="/documentation.htm">Documentation</a>
					<li><a href="/terms-of-service.htm">Terms of service</a>
					<li><a href="/privacy-policy.htm">Privacy policy</a>
				</ul>
			</section>
		</footer>
	</div>


</body>
</html>
