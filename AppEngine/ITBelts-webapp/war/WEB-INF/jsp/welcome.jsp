
<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="Welcome to the ITBelts community"/>
</jsp:include>

<body>

	<div id="page">
		<div id="wrapper">

            <jsp:include page="includes/header.jsp"/>

			<div id="content">

				<div class="plain-content">
					<p></p>

					<h1>Welcome to the ITBelts community.</h1>

					<h3>You have received 40 contribution points so you can take a number of free exams and start earning knowledge points to earn your belts.</h3>

					<h3>Click on the "Courses and Exams" button at the top of the screen to start your exploration.</h3>

					<p></p>

				</div>

			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>


</body>
</html>

