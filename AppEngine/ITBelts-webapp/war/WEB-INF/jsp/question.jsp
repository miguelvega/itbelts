<%@ page import="org.itbelts.domain.Question" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Question"/>
</jsp:include>

<body>
	<div id="page">
		<div id="wrapper">
            <jsp:include page="includes/header.jsp"/>
			<%
			    Question theQuestion = (Question) request.getAttribute( "question" );
			%>
			<div id="content">
				<div class="plain-content">
					<p></p>
					<h1>Question <%= theQuestion.get_id() %></h1>
					<p></p>
					<p><b>Name:</b><br><%= theQuestion.getName() %></p>
					<p></p>
					<p><b>Description:</b><br><%= theQuestion.getDescription() %></p>
				    <br>
				    <br>
				    TODO --> Add the rest of the question content and make it editable
				</div>
			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>
</body>
</html>
