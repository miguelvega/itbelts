<%@ page import="java.util.Map"%>
<%@ page import="org.itbelts.domain.AnswerContainer"%>
<%@ page import="org.itbelts.domain.UserExam" %>
<%@ page import="org.itbelts.domain.Exam" %>
<%@ page import="org.itbelts.domain.Question" %>
<%@ page import="org.itbelts.domain.PossibleAnswer" %>
<%@ page import="org.itbelts.domain.AnswerType" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts Exam Results"/>
</jsp:include>
<body>
	<div id="page">
		<div id="wrapper">
            <jsp:include page="includes/header.jsp"/>
			<div id="content">
				<div class="plain-content">
<%
    UserExam theUserExam = (UserExam) request.getAttribute( "userExam" );
    Exam theExam = (Exam) request.getAttribute( "exam" );
    Map<String,Question> theQuestionMap = (Map<String,Question>) request.getAttribute( "questions" );
%>
				
					<p></p>
<%
	    if ( theUserExam.getResult() == UserExam.PASSED ) {
%>
					<h1>Congratulations!!!&nbsp;&nbsp;&nbsp;You PASSED this exam on <%= theUserExam.getEnd() %></h1>
                    <p>&nbsp;</p>
                    <p>You earned <%= theExam.getKnowledgePoints() %> points.</p>
<%
	    } else {
%>
					<h1>Sorry.&nbsp;&nbsp;&nbsp;You FAILED this exam on <%= theUserExam.getEnd() %></h1>
                    <p>&nbsp;</p>
                    <p>You can retry this exam free of charge.</p>
<%
	    }
%>
                    <p>&nbsp;</p>
                    <p>Score: <%= theUserExam.getScore() %> out of <%= theUserExam.getAnswers().size() %>, or <%= theUserExam.getPercentage() %>%  (Required: <%= theExam.getSuccessPercentage() %>%)</p>
                    <p>&nbsp;</p>
                    <hr>
<%
                		for ( AnswerContainer ac : theUserExam.getAnswers() ) {
                		    Question q = theQuestionMap.get( ac.getQuestionId() );
%>
							<h3><div class="question-title"><img src="/img/result_<%= ac.isCorrect() ? "correct" : "wrong" %>.gif" />&nbsp;&nbsp;<%= q.getName() %></div></h3>
							<div class="question-content">    
								<div class="question-description"><%=q.getDescription()%></div>    
								
								<div class="question-answers">
<%
    								boolean isFirst = true;
    								int a_index = 0;
			                        for ( PossibleAnswer pa : q.getPossibleAnswers() ) {
			                            if ( pa.getType() != AnswerType.Text ) {
%>
					    					<div class="question-answer<%= isFirst?"-first":"" %>">
<%
			                        	}
                        					if ( pa.getType() == AnswerType.Text ) {
                        					    if ( a_index == 0 ) {
						                            if ( ac.getGivenAnswers().size() > 0 ) {
%>
														<p>Your answer: <input type="text" size="75" value="<%= ac.getGivenAnswers().get(0) %>" disabled="disabled"></input><br>&nbsp;</p>
<%
							                        } else {
%>
														<p>Your answer: <input type="text" disabled="disabled"></input><br>&nbsp;</p>
<%
							                        }
  					                        	}
					                            if ( isFirst ) {
%>
													Possible answer(s):
<%
						                        } else {
%>
													,&nbsp;
<%
						                        }
%>
												 <%= pa.getText() %>
<%
					                        } else if ( pa.getType() == AnswerType.RadioButton ) {
%>
					                            <div class="answer-placeholder">
<%
					                            if ( ac.getGivenAnswers().size() > 0 && a_index == Integer.parseInt( ac.getGivenAnswers().get( 0 ) ) ) {
%>
													<img src="/img/answer_radio_chk.gif"/>
<%
						                        } else {
%>
													<img src="/img/answer_radio.gif"/>
<%
						                        }
%>
												</div>
<%
	                        					if ( pa.isCorrect() ) {
%>
													<div class="correct-placeholder"><img src="/img/answer_correct.gif"/></div>
<%
						                        } else {
%>
													<div class="correct-placeholder">&nbsp;</div>
<%
						                        }
%>
												<%= pa.getText() %>
<%
					                        } else if ( pa.getType() == AnswerType.CheckBox ) {
%>
					                            <div class="answer-placeholder">
<%
					                            if ( ac.getResults().size() > a_index ) {
%>
													<img src="/img/answer_box<%= ac.getResults().get(a_index) ? "_chk" : "" %>.gif"/>
<%
					                            } else {
%>
													<img src="/img/answer_box.gif"/>
<%
					                            }
%>
												</div>
<%
	                        					if ( pa.isCorrect() ) {
%>
													<div class="correct-placeholder"><img src="/img/answer_correct.gif"/></div>
<%
						                        } else {
%>
													<div class="correct-placeholder">&nbsp;</div>
<%
						                        }
%>
												<%= pa.getText() %>
<%
					                        }
			                            if ( pa.getType() != AnswerType.Text ) {
%>
						    				</div>
					    			
<%
			                            }
    									isFirst = false;
										a_index++;
			                        }
%>
					    		</div>
							</div>
<%
                        }
%>
				</div>
			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>
</body>
</html>
