<%@ page import="java.util.List" %>
<%@ page import="org.itbelts.domain.Todo" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/head.jsp">
<jsp:param name="title" value="ITBelts TODO list"/>
</jsp:include>

<body>

	<div id="page">
		<div id="wrapper">

            <jsp:include page="includes/header.jsp"/>

			<%
			    List<Todo> theData = (List<Todo>) request.getAttribute( "todos" );
			    String theListType = (String) request.getAttribute( "listType" );
			    String theListName = (String) request.getAttribute( "listName" );
			%>

			<div id="content">

				<div class="plain-content">
					<p></p>
					
					<%
					    if ( theListType.equals( "ALL" ) ) {
					%>
							<h1>ALL todo's</h1>
							<p></p>
							<p>If the list is too big for good performance, you can browse to it via a specific topic, course, exam or question.</p>
					<%            
				        } else {
					%>
							<h1>Todo's for <%= theListName %></h1>
							<p></p>
					<%            
				        }
					%>
		
					<p></p>

					<table id="todos" border="1" width="100%">
		                <tr>
		                    <th align="left">Details</td>
		                    <th align="left">Topic</td>
		                    <th align="left">Course</td>
		                    <th align="left">Exam</td>
		                    <th align="left">Question</td>
		                    <th align="left">Assigned to</td>
		                    <th align="left">Actions</td>
		                </tr>
						<%
						    for ( Todo t : theData ) {
						%>
			                <tr>
			                    <td><%= t.getDetails() %></td>
								<%
								    if ( t.getTopicId() != null ) {
								%>
				                    <td><a href="/topic.htm?id=<%= t.getTopicId() %>"><%= t.getTopicId() %></a></td>
								<%            
							        } else {
								%>
				                    <td>n/a</td>
								<%            
							        }
								%>
								<%
								    if ( t.getCourseId() != null ) {
								%>
				                    <td><a href="/course.htm?id=<%= t.getCourseId() %>"><%= t.getCourseId() %></a></td>
								<%            
							        } else {
								%>
				                    <td>n/a</td>
								<%            
							        }
								%>
								<%
								    if ( t.getExamId() != null ) {
								%>
				                    <td><a href="/exam.htm?id=<%= t.getExamId() %>"><%= t.getExamId() %></a></td>
								<%            
							        } else {
								%>
				                    <td>n/a</td>
								<%            
							        }
								%>
								<%
								    if ( t.getQuestionId() != null ) {
								%>
				                    <td><a href="/question.htm?id=<%= t.getQuestionId() %>"><%= t.getQuestionId() %></a></td>
								<%            
							        } else {
								%>
				                    <td>n/a</td>
								<%            
							        }
								%>
								<%
								    if ( t.getAssignedTo() != null ) {
								%>
				                    <td><%= t.getAssignedTo() %></td>
				                    <td><a href="/todo-unassign.htm?id=<%= t.get_id() %>&listType=<%= theListType %>">Unassign</a></td>
								<%            
							        } else {
								%>
				                    <td>Open</td>
				                    <td><a href="/todo-solve.htm?id=<%= t.get_id() %>">Solve</a></td>
								<%            
							        }
								%>
			                </tr>
						<%            
					        }
						%>
					</table>
				</div>

			</div>
		</div>

        <jsp:include page="includes/footer.jsp">
			<jsp:param name="tm" value="true"/>
		</jsp:include>
	</div>


</body>
</html>
