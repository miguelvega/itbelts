package org.itbelts.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 24-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      24-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class Gravatar implements IAvatar {

    private static final String GRAVATAR_URL = "https://secure.gravatar.com/avatar/";
    private static final String GRAVATAR_ACTION = "?d=mm";  // Force the mystery man if no image found 
    
    
    /**
     * Build a Gravatar url for downloading an image.  The default image is the mystery man. 
     * @param anEmailAddress
     * @return
     */
    @Override
    public String buildUrl( String anEmailAddress ) {
        // Calculate the gravatar URL from the user email
        return GRAVATAR_URL + md5Hex( anEmailAddress.toLowerCase().trim() ) + GRAVATAR_ACTION;
    }
    
    /* (non-Javadoc)
     * @see org.itbelts.util.IAvatar#getName()
     */
    @Override
    public String getName() {
        return "Gravatar";
    }

    /**
     * Calculate an MD5 hash value for a given String.
     * @param message
     * @return String
     */
    private String md5Hex( String message ) {
        try {
            return hex( MessageDigest.getInstance( "MD5" ).digest( message.getBytes( "CP1252" ) ) );
        } catch ( NoSuchAlgorithmException ignore ) {
        } catch ( UnsupportedEncodingException ignore ) {
        }
        return "none";
    }

    /**
     * Convert a byte array into a hex string
     * @param array
     * @return String
     */
    private String hex( byte[] array ) {
        StringBuffer sb = new StringBuffer();
        for ( int i = 0; i < array.length; ++i ) {
            sb.append( Integer.toHexString( ( array[ i ] & 0xFF ) | 0x100 ).substring( 1, 3 ) );
        }
        return sb.toString();
    }

}
