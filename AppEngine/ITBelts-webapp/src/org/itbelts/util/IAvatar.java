package org.itbelts.util;

/**
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 27-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      27-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public interface IAvatar {

    /**
     *  Compose an avatar URL from the given email address
     * @param anEmailAddress
     * @return String
     */
    public String buildUrl( String anEmailAddress );

    /**
     *  Return the friendly name of this provider so it can be shown on the profile screen
     * @return String
     */
    public String getName();
    
}
