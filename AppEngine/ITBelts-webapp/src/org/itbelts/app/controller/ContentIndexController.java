package org.itbelts.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ContentIndexController {
 
    @RequestMapping( value="/content-index.htm", method = RequestMethod.GET)
    public ModelAndView showIndex() {
        ModelAndView model = new ModelAndView("content-index");
 
        return model;
    }
}
