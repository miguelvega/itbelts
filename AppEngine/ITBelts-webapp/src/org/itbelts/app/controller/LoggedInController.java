package org.itbelts.app.controller;

import javax.servlet.http.HttpSession;

import org.itbelts.dao.IUserDAO;
import org.itbelts.domain.RoleType;
import org.itbelts.domain.User;
import org.itbelts.util.IAvatar;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.appengine.api.users.UserServiceFactory;

@Controller
public class LoggedInController {

    private IUserDAO userDAO;
    private IAvatar  avatarProvider;

    @RequestMapping( value = "/loggedin.htm", method = RequestMethod.GET )
    public String loggedIn( HttpSession aSession ) {

        com.google.appengine.api.users.User googleUser = UserServiceFactory.getUserService().getCurrentUser();
        User theUser = userDAO.getUser( googleUser.getUserId() );
        
        String theRedirectUrl = "redirect:/";
        
        if ( theUser == null ) {
            // No user was found, create a new one with the values from the google account and some defaults
            theUser = new User();

            theUser.set_id( googleUser.getUserId() );
            theUser.setEmail( googleUser.getEmail() );
            theUser.getRoles().add( RoleType.USER );
            theUser.setContributionPoints( 40 );  // Make sure the user can start taking some exams
            
            theRedirectUrl += "welcome.htm";
        }
        // If the user became an admin user lately --> update this in the profile
        if ( UserServiceFactory.getUserService().isUserAdmin() && !theUser.getRoles().contains( RoleType.ADMIN ) ) {
            theUser.getRoles().add( RoleType.ADMIN );
        } else {
            // If the user is not an admin anymore --> remove the role from the profile
            if ( !UserServiceFactory.getUserService().isUserAdmin() && theUser.getRoles().contains( RoleType.ADMIN ) ) {
                theUser.getRoles().remove( RoleType.ADMIN );
            }            
        }
        
        userDAO.saveUser( theUser );
        
        if ( theUser.getEmail() != null ) {
            // Calculate the avatar URL from the user email
            aSession.setAttribute( "avatar-url", avatarProvider.buildUrl( theUser.getEmail() ) );
            aSession.setAttribute( "avatar-url-provider", avatarProvider.getName() );
        }
        


        return theRedirectUrl;
    }
    
    /**
     * Called by dependency injection to set the user DAO on this instance.
     * @param aUserDAO the userDAO to set
     */
    public void setUserDAO(IUserDAO aUserDAO) {
        userDAO = aUserDAO;
    }

    /**
     * Called by dependency injection to set the avatar provider on this instance.
     * @param aAvatarProvider The avatarProvider to set.
     */
    public void setAvatarProvider( IAvatar aAvatarProvider ) {
        avatarProvider = aAvatarProvider;
    }
    
}
