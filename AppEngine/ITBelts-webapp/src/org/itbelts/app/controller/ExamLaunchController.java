package org.itbelts.app.controller;

import org.itbelts.app.controller.business.ExamAccess;
import org.itbelts.app.controller.business.UserLoggedOn;
import org.itbelts.dao.IExamDAO;
import org.itbelts.domain.Exam;
import org.itbelts.domain.User;
import org.itbelts.exception.UnknownResourceException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@Controller
public class ExamLaunchController {
 
    private IExamDAO            examDAO;
    private UserLoggedOn        securityChecker;
    private ExamAccess          examAccessChecker;

    @RequestMapping( value = "/exam-launch.htm", method = RequestMethod.POST )
    public ModelAndView showExamLaunch( @RequestParam( value="id" ) String anExamId ) {

        // Will throw exception when the user is not logged on
        try {
            User theUser = securityChecker.getLoggedOnUser();
            return forwardToContent( theUser, anExamId );
        
        } catch ( UnknownResourceException e ) {
            UserService userService = UserServiceFactory.getUserService();
            ModelAndView model = new ModelAndView("exam-launch");
            model.setViewName( "redirect:" + userService.createLoginURL("/exam-launch.htm?id=" + anExamId ) );
            return model;
        }
    }

    @RequestMapping( value = "/exam-launch.htm", method = RequestMethod.GET )
    public ModelAndView showExamLaunchForLoggedOnUser( @RequestParam( value="id" ) String anExamId ) {
        
        // Will throw exception when the user is not logged on
        User theUser = securityChecker.getLoggedOnUser();

        return forwardToContent( theUser, anExamId );
    }
    
    private ModelAndView forwardToContent( User aUser, String anExamId ) {
        Exam theExam = examDAO.getExam( anExamId );
        
        ModelAndView model = new ModelAndView( "exam-launch", "exam", theExam );

        int theExamAccess = examAccessChecker.calculateExamAccess( aUser, theExam );
        model.addObject( "examAccess", theExamAccess );
            
        return model;
    }
    
    /**
     * @param aExamDAO The examDAO to set.
     */
    public void setExamDAO( IExamDAO aExamDAO ) {
        examDAO = aExamDAO;
    }

    /**
     * @param aSecurityChecker The securityChecker to set.
     */
    public void setSecurityChecker( UserLoggedOn aSecurityChecker ) {
        securityChecker = aSecurityChecker;
    }

    /**
     * @param aExamAccessChecker The examAccessChecker to set.
     */
    public void setExamAccessChecker( ExamAccess aExamAccessChecker ) {
        examAccessChecker = aExamAccessChecker;
    }

}
