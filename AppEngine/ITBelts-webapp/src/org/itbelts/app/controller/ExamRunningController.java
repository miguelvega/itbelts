package org.itbelts.app.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.itbelts.app.controller.business.UserLoggedOn;
import org.itbelts.app.controller.form.ExamForm;
import org.itbelts.dao.IExamDAO;
import org.itbelts.dao.IQuestionDAO;
import org.itbelts.dao.ITodoDAO;
import org.itbelts.dao.IUserDAO;
import org.itbelts.dao.IUserExamDAO;
import org.itbelts.domain.AnswerContainer;
import org.itbelts.domain.AnswerType;
import org.itbelts.domain.Exam;
import org.itbelts.domain.PossibleAnswer;
import org.itbelts.domain.Question;
import org.itbelts.domain.RoleType;
import org.itbelts.domain.Todo;
import org.itbelts.domain.User;
import org.itbelts.domain.UserExam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * MVC Controller for the exam-running jsp.<br>
 * This controller takes care of starting an exam (fetching the questions)<br>
 * This controller also takes care of parsing the answers and calculating the score
 *
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 17-nov.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      17-nov.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
@Controller
public class ExamRunningController {

    private static final String USER_EXAM_ID_KEY = "ue";

    private IExamDAO            examDAO;
    private IQuestionDAO        questionDAO;
    private IUserDAO            userDAO;
    private IUserExamDAO        userExamDAO;
    private ITodoDAO            todoDAO;

    private UserLoggedOn        securityChecker;

    @RequestMapping( value = "/exam-running.htm", method = RequestMethod.GET )
    public ModelAndView showExamDetails( @RequestParam( value = "id" ) String anExamId, HttpSession aSession ) {

        // Will throw exception when logon was not OK
        User currentUser = securityChecker.getLoggedOnUser();

        ModelAndView model = new ModelAndView( "exam-running" );

        // Attach the exam itself to the model
        Exam theExam = examDAO.getExam( anExamId );
        
        // Pay for this exam
        if ( currentUser.getContributionPoints() >= theExam.getContributionPoints() ) {
            currentUser.setContributionPoints( currentUser.getContributionPoints() - theExam.getContributionPoints() );
            userDAO.saveUser( currentUser );
        } else {
            if ( currentUser.hasRole( RoleType.ADMIN ) ) {
                // That's ok, an admin can access an exam without points
            } else {
                throw new IllegalStateException( "User does not have enough points to pay for this exam.  Please log off and try again." );
            }
        }
        
        model.addObject( "exam", theExam );

        // Attach all the questions to the model
        List<Question> theQuestions = questionDAO.buildExam( anExamId );

        // Create a new userExam, store it in the DB and add it to the session
        UserExam theUserExam = new UserExam();
        theUserExam.setUserId( currentUser.get_id() );
        theUserExam.setExamId( theExam.get_id() );
        theUserExam.setResult( UserExam.ONGOING );
        theUserExam.start( theExam.getMaxTime() ); // Sets both start and end-time.

        userExamDAO.saveUserExam( theUserExam );

        aSession.setAttribute( USER_EXAM_ID_KEY, theUserExam.get_id() );

        // Create the form container
        ExamForm theExamForm = new ExamForm();
        theExamForm.setUserExamId( theUserExam.get_id() );
        theExamForm.setEndTime( theUserExam.getEnd() );
        theExamForm.setQuestions( theQuestions );

        // Add 1 answer container for each question in the exam
        for ( Question q : theQuestions ) {
            theExamForm.getAnswers().add( new AnswerContainer( q.get_id() ) );
        }

        model.addObject( "examForm", theExamForm );

        return model;
    }

    @RequestMapping( value = "/exam-running.htm", method = RequestMethod.POST )
    public ModelAndView finishExam( @ModelAttribute( value = "examForm" ) ExamForm aForm, @RequestParam( value = USER_EXAM_ID_KEY ) String aUserExamId, HttpSession aSession ) {

        // Will throw exception when logon was not OK
        User currentUser = securityChecker.getLoggedOnUser();

        String theUserExamId = (String) aSession.getAttribute( USER_EXAM_ID_KEY );
        if ( theUserExamId == null ) {
            throw new IllegalStateException( "Session info corrupted.  No userExam ID available in session.  Please log off and try again." );
        }

        if ( theUserExamId.equals( aUserExamId ) ) {
            // OK
        } else {
            throw new IllegalStateException( "Session info corrupted.  No matching userExam ID.  Please log off and try again." );
        }

        UserExam theUserExam = userExamDAO.getUserExam( aUserExamId );

        if ( theUserExam == null ) {
            throw new IllegalStateException( "Session info corrupted.  Incorrect userExam ID.  Please log off and try again." );
        }

        if ( theUserExam.getUserId().equals( currentUser.get_id() ) ) {
            // OK
        } else {
            throw new IllegalStateException( "Session info corrupted.  Current user does not match DB userExam.  Please log off and try again." );
        }

        // After the first validations (basic session integrity) we STOP the clock
        theUserExam.stop();
        theUserExam.setAnswers( aForm.getAnswers() );

        Exam theExam = examDAO.getExam( theUserExam.getExamId() );

        Map<String,Question> theQuestionMap = questionDAO.getQuestionsForUserExam( theUserExam );

        Date d1, d2;
        SimpleDateFormat f = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        try {
            d1 = f.parse( theUserExam.getStart() );
            d2 = f.parse( theUserExam.getEnd() );
        } catch ( ParseException e ) {
            throw new IllegalStateException( "Session info corrupted.  Bad dates.  Please log off and try again.", e );
        }

        int theDurationInMinutes = (int) ( ( d2.getTime() - d1.getTime() ) / 60000L );
        if ( theDurationInMinutes > theExam.getMaxTime() ) {
            theUserExam.setResult( UserExam.FAILED_OUT_OF_TIME );
        } else {
            // Answered within the maximum duration --> review the answers and calculate the score

            scoreExam( theExam.getSuccessPercentage(), theUserExam, theQuestionMap );

            createTodos( theUserExam );
        }

        // Save the exam with the given answers and the score
        userExamDAO.saveUserExam( theUserExam );

        // Remove the exam id from the session.
//        aSession.removeAttribute( USER_EXAM_ID_KEY );

        // Proceed to the exam result screen to show the details
        ModelAndView model = new ModelAndView( "exam-result", "userExam", theUserExam );
        model.addObject( "exam",  theExam );
        model.addObject( "questions",  theQuestionMap );
        
        return model;
    }

    /**
     * Store a todo in the DB for every question the user has marked
     * 
     * @param aUserExam
     */
    private void createTodos( UserExam aUserExam ) {
        for ( AnswerContainer a : aUserExam.getAnswers() ) { // Each answercontainer should match 1 question from the userExam
            if ( a.isReportLayoutProblem() ) {
                Todo t = new Todo();
                t.setQuestionId( a.getQuestionId() );
                t.setDetails( "Fix layout issue(s)" );
                todoDAO.saveTodo( t );
            }
        }
    }

    /**
     * Calculate the exam score.
     * 
     * @param aSuccessPercentage
     * @param aUserExam
     * @param aQuestionMap
     */
    private void scoreExam( int aSuccessPercentage, UserExam aUserExam, Map<String,Question> aQuestionMap ) {
        int theScore = 0;

        
        for ( AnswerContainer a : aUserExam.getAnswers() ) { // Each answercontainer should match 1 question from the userExam

            
            if ( a.getGivenAnswers() == null || a.getGivenAnswers().size() == 0 ) {
                continue; // incomplete answer in some way --> probably a manipulated page
            }
            boolean isAnswered = false;
            for ( String s : a.getGivenAnswers() ) {
                if ( s != null ) {
                    isAnswered = true;
                }
            }
            if ( !isAnswered ) {
                continue; // No answer was given --> proceed to next question without adding to the score
            }

            // Get the question (it has the possible correct answers in it)
            Question q = aQuestionMap.get( a.getQuestionId() );

            // We decide on the validation logic depending on the type of answer that can be given.
            AnswerType theType = q.getPossibleAnswers().get( 0 ).getType();

            if ( theType == AnswerType.Text ) {
                a.getResults().add( Boolean.FALSE );

                // A text answer is compared to all possible correct answers (exact match is required)
                for ( PossibleAnswer p : q.getPossibleAnswers() ) {
                    if ( a.getGivenAnswers().get( 0 ) != null && a.getGivenAnswers().get( 0 ).equals( p.getText() ) ) {
                        theScore++;
                        a.setCorrect( true );
                        a.getResults().set( 0, Boolean.TRUE );
                        break;
                    }
                }

            } else if ( theType == AnswerType.RadioButton ) {

                // For a radiobutton answer, there will be only 1 non null value. It has the index of the possible answer in it
                for ( String s : a.getGivenAnswers() ) {
                    a.getResults().add( Boolean.FALSE );
                    if ( s != null ) {
                        // This is an actual answer e.g. "1" or "2"
                        int theIndex = Integer.parseInt( s );
                        if ( q.getPossibleAnswers().get( theIndex ).isCorrect() ) {
                            theScore++;
                            a.setCorrect( true );
                            a.getResults().set( a.getResults().size() - 1, Boolean.TRUE );
                        }
                    }
                }

            } else if ( theType == AnswerType.CheckBox ) {

                // Checkbox answers need to match all possible answers. (either "not null vs correct" OR "null vs incorrect")
                boolean isOK = true;
                for ( int i = 0; i < q.getPossibleAnswers().size(); i++ ) {
                    PossibleAnswer p = q.getPossibleAnswers().get( i );
                    a.getResults().add( Boolean.FALSE );
                    String s = a.getGivenAnswers().get( i );
                    if ( p.isCorrect() && s == null ) {
                        // NOT ok, user forgot to check a correct option
                        isOK = false;
                    } else if ( !p.isCorrect() && s != null ) {
                        // NOT ok, user did check an incorrect option
                        isOK = false;
                    } else if ( p.isCorrect() ) {
                        a.getResults().set( a.getResults().size() - 1, Boolean.TRUE );
                    }
                }
                if ( isOK ) {
                    theScore++;
                    a.setCorrect( true );
                }
            }

        }
        int theActualPercentage = ( 100 * theScore ) / aUserExam.getAnswers().size();

        if ( theActualPercentage < aSuccessPercentage ) {
            aUserExam.setResult( UserExam.FAILED_LOW_SCORE );
        } else {
            aUserExam.setResult( UserExam.PASSED );
        }
        aUserExam.setScore( theScore );
        aUserExam.setPercentage( theActualPercentage );
    }

    /**
     * @param anExamDAO
     *            The examDAO to set.
     */
    public void setExamDAO( IExamDAO anExamDAO ) {
        examDAO = anExamDAO;
    }

    /**
     * @param aTodoDAO
     *            The todoDAO to set.
     */
    public void setTodoDAO( ITodoDAO aTodoDAO ) {
        todoDAO = aTodoDAO;
    }

    /**
     * @param aUserExamDAO
     *            The userExamDAO to set.
     */
    public void setUserExamDAO( IUserExamDAO aUserExamDAO ) {
        userExamDAO = aUserExamDAO;
    }

    /**
     * @param aQuestionDAO
     *            The questionDAO to set.
     */
    public void setQuestionDAO( IQuestionDAO aQuestionDAO ) {
        questionDAO = aQuestionDAO;
    }

    /**
     * Called by dependency injection to set the user DAO on this instance.
     * 
     * @param aUserDAO
     *            the userDAO to set
     */
    public void setUserDAO( IUserDAO aUserDAO ) {
        userDAO = aUserDAO;
    }

    /**
     * @param aSecurityChecker
     *            The securityChecker to set.
     */
    public void setSecurityChecker( UserLoggedOn aSecurityChecker ) {
        securityChecker = aSecurityChecker;
    }

}
