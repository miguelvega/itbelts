package org.itbelts.app.controller.business;

import java.util.List;

import org.itbelts.dao.IUserExamDAO;
import org.itbelts.domain.Exam;
import org.itbelts.domain.RoleType;
import org.itbelts.domain.User;
import org.itbelts.domain.UserExam;

/**
 * Class that has business logic related to exam access. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * v2013.12.0 28-okt.-2013 - DKBR813 - initial release
 * 
 * </pre>
 * 
 * @version v2013.12.0 28-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ExamAccess {

    private IUserExamDAO    userExamDAO;

    public static final int OK                = -1;
    public static final int ALREADY_PASSED    = 1;
    public static final int NOT_ENOUGH_POINTS = 2;
    public static final int CAN_RETRY         = 3;

    /**
     * Determine if this user can access the given exam.<br>
     * Access is granted to:<br>
     * <ul>
     * <li>admins
     * <li>level 3 coaches
     * <li>Anyone that has already taken this exam before and not passed it (retry attempt)
     * <li>Anyone that has passed all the pre-requisite exams AND has enough points
     * <li>free exams (currently java basic and java intermediate)
     * </ul>
     * 
     * @param aUser
     * @param anExam
     * @return boolean
     */
    public int calculateExamAccess( User aUser, Exam anExam ) {
        
        // SPecial users can take any exam
        if ( aUser.hasRole( RoleType.ADMIN ) || aUser.hasRole( RoleType.COACH_LEVEL_3 ) ) {
            return OK;
        }

        List<UserExam> theList = userExamDAO.getExamsForUser( aUser.get_id() );
        
        if ( hasPassed( aUser, anExam, theList ) ) {
            return ALREADY_PASSED;
        }
        if ( canRetry( aUser, anExam, theList ) ) {
            return CAN_RETRY;
        }

        // TODO implement remaining business rule.  (pre-requisite exams)

        if ( anExam.getTopicId().equals( "J0101" ) || anExam.getTopicId().equals( "J0102" ) ) {
            return OK;
        }

        if ( aUser.getContributionPoints() < anExam.getContributionPoints() ) {
            return NOT_ENOUGH_POINTS;
        }

        return OK;
    }

    /**
     * Check if this user has the right to retry an exam.<br>
     * Returns true if the user has already taken this exam before and not passed it.
     * 
     * @param aUser
     * @param anExam
     * @param aUserExamList
     * @return boolean
     */
    public boolean canRetry( User aUser, Exam anExam, List<UserExam> aUserExamList ) {
        if ( aUserExamList == null || aUserExamList.size() == 0 ) {
            return false; // this is his first exam
        }
        
        boolean canRetry = false;
        for ( UserExam ue : aUserExamList ) {
            if ( ue.getExamId().equals( anExam.get_id() ) ) {
                if ( ue.getResult() == UserExam.PASSED ) {
                    return false; // he passed it already
                }
                canRetry = true; // we found at least one previous attempt (so he paid already)
            }
        }
        return canRetry;
    }

    /**
     * Check if this user has already passed the given exam.<br>
     * 
     * @param aUser
     * @param anExam
     * @param aUserExamList
     * @return boolean
     */
    public boolean hasPassed( User aUser, Exam anExam, List<UserExam> aUserExamList ) {
        if ( aUserExamList == null || aUserExamList.size() == 0 ) {
            return false; // this is his first exam
        }
        for ( UserExam ue : aUserExamList ) {
            if ( ue.getExamId().equals( anExam.get_id() ) ) {
                if ( ue.getResult() == UserExam.PASSED ) {
                    return true; // he passed it already
                }
            }
        }
        return false;
    }

    /**
     * @param aUserExamDAO
     *            The userExamDAO to set.
     */
    public void setUserExamDAO( IUserExamDAO aUserExamDAO ) {
        userExamDAO = aUserExamDAO;
    }
}
