package org.itbelts.app.controller.business;

import org.itbelts.domain.Course;
import org.itbelts.domain.RoleType;
import org.itbelts.domain.User;

/**
 *  Class that has business logic related to course access.
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.12.0 27-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.12.0      27-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class CourseAccess {

    /**
     * Check if this user can access the content of the given course.<br>
     * Access is granted to:<br>
     * <ul>
     *    <li>admins
     *    <li>level 3 coaches
     *    <li>Anyone that has passed all the exams related to the course topic
     *    <li>users with an ongoing course registration for this course
     * </ul>
     * @param aUser
     * @param aCourse
     * @return boolean
     */
    public boolean hasAccessToCourse( User aUser, Course aCourse ) {
        if ( aUser.hasRole( RoleType.ADMIN ) ) {
            return true;
        }
        if ( aUser.hasRole( RoleType.COACH_LEVEL_3 ) ) {
            return true;
        }
        //TODO implement remaining 2 business rules.
//        List<Exam> theExamsFor
        
        return false;
    }
    
    
    


}
