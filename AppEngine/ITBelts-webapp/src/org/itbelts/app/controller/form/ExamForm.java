package org.itbelts.app.controller.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.itbelts.domain.AnswerContainer;
import org.itbelts.domain.Question;

/**
 * Representation for the input form for the questions of an exam. <br>
 * <br>
 * <u><i>Version History</i></u>
 * 
 * <pre>
 * 02 Nov 2013 - initial release
 *
 * </pre>
 *
 * @version 02 Nov 2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class ExamForm implements Serializable {
    
    private static final long serialVersionUID = 4707614011781706478L;

    private String userExamId;
    private String endTime;  // Calculated by the UserExam class.
    private List<AnswerContainer> answers = new ArrayList<AnswerContainer>();
    private List<Question> questions = new ArrayList<Question>();

    /**
     * @return Returns the userExamId.
     */
    public String getUserExamId() {
        return userExamId;
    }

    /**
     * @param aUserExamId The userExamId to set.
     */
    public void setUserExamId( String aUserExamId ) {
        userExamId = aUserExamId;
    }

    /**
     * @return Returns the endTime.
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param aEndTime The endTime to set.
     */
    public void setEndTime( String aEndTime ) {
        endTime = aEndTime;
    }

    /**
     * @return Returns the answers.
     */
    public List<AnswerContainer> getAnswers() {
        return answers;
    }

    /**
     * @param aAnswers The answers to set.
     */
    public void setAnswers( List<AnswerContainer> aAnswers ) {
        answers = aAnswers;
    }

    /**
     * @return Returns the questions.
     */
    public List<Question> getQuestions() {
        return questions;
    }

    /**
     * @param aQuestions The questions to set.
     */
    public void setQuestions( List<Question> aQuestions ) {
        questions = aQuestions;
    }
    
}