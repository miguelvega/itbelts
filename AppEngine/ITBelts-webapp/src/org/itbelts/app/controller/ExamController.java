package org.itbelts.app.controller;

import java.util.List;

import org.itbelts.app.controller.business.ExamAccess;
import org.itbelts.app.controller.business.UserLoggedOn;
import org.itbelts.dao.IExamDAO;
import org.itbelts.dao.ITodoDAO;
import org.itbelts.domain.Exam;
import org.itbelts.domain.Todo;
import org.itbelts.domain.User;
import org.itbelts.exception.UnknownResourceException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ExamController {
 
    private IExamDAO            examDAO;
    private ITodoDAO            todoDAO;
    private UserLoggedOn        securityChecker;
    private ExamAccess          examAccessChecker;

    @RequestMapping( value = "/exam.htm", method = RequestMethod.GET )
    public ModelAndView showExamDetails( @RequestParam( value="id" ) String anExamId ) {

        ModelAndView model = new ModelAndView("exam");

        Exam theExam = examDAO.getExam( anExamId );
        model.addObject( "exam", theExam );

        try {
            User theUser = securityChecker.getLoggedOnUser();
            model.addObject( "user", theUser );
            
            int theExamAccess = examAccessChecker.calculateExamAccess( theUser, theExam );
            model.addObject( "examAccess", theExamAccess );
            
            List<Todo> theTodosForThisExam = todoDAO.getTodosForExam( theExam.get_id() );
            model.addObject( "todosAvailable", ( theTodosForThisExam != null ) ? theTodosForThisExam.size() : 0 );
            
        } catch ( UnknownResourceException e ) {
            // Not logged on --> is mentioned in the screen
        }
        
        return model;
    }

    /**
     * @param anExamDAO The examDAO to set.
     */
    public void setExamDAO( IExamDAO anExamDAO ) {
        examDAO = anExamDAO;
    }
    /**
     * @param aTodoDAO
     *            The todoDAO to set.
     */
    public void setTodoDAO( ITodoDAO aTodoDAO ) {
        todoDAO = aTodoDAO;
    }
    /**
     * @param aSecurityChecker
     *            The securityChecker to set.
     */
    public void setSecurityChecker( UserLoggedOn aSecurityChecker ) {
        securityChecker = aSecurityChecker;
    }
    /**
     * @param aExamAccessChecker The examAccessChecker to set.
     */
    public void setExamAccessChecker( ExamAccess aExamAccessChecker ) {
        examAccessChecker = aExamAccessChecker;
    }
}
