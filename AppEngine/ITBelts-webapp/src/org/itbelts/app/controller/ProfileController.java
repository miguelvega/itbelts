package org.itbelts.app.controller;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.itbelts.app.controller.business.UserLoggedOn;
import org.itbelts.app.controller.form.ProfileForm;
import org.itbelts.dao.IUserDAO;
import org.itbelts.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping( "/profile.htm" )
public class ProfileController {

    private IUserDAO            userDAO;
    private UserLoggedOn        securityChecker;

    private Map<String, String> myCountriesViaCode; // Used for translating code from DB into actual country name
    private Map<String, String> myCountriesViaName; // Used in the JSP to get an alphabetic country list

    /**
     * Constructor for this controller. It reads a resource file with the country list for the input form.
     */
    @SuppressWarnings( { "unchecked", "rawtypes" } )
    public ProfileController() {
        Properties properties = new Properties();
        try {
            InputStream in = getClass().getResourceAsStream( "/countries.properties" );
            properties.load( in );
            in.close();
            myCountriesViaCode = new HashMap<String, String>( (Map) properties );
            myCountriesViaName = new TreeMap<String, String>();
            for ( Map.Entry<String, String> e : myCountriesViaCode.entrySet() ) {
                myCountriesViaName.put( e.getValue(), e.getValue() );
            }
        } catch ( Exception e ) {
            myCountriesViaCode = new TreeMap<String, String>();
            myCountriesViaName = new TreeMap<String, String>();
        }
    }

    /**
     * Create the input form and forward to the JSP.
     * 
     * @return {@link ModelAndView}
     */
    @RequestMapping( method = RequestMethod.GET )
    public ModelAndView profileForm() {

        // Will throw exception when logon was not OK
        User currentUser = securityChecker.getLoggedOnUser();
        
        ModelAndView model = new ModelAndView( "profile" );
        model.addObject( "countryMap", myCountriesViaName );

        ProfileForm f = new ProfileForm();
        model.addObject( "profileForm", f );

        // We have found persisted state so we transfer it to the input form for modification
        f.setName( currentUser.getName() );
        f.setEmail( currentUser.getEmail() );
        f.setCountry( myCountriesViaCode.get( currentUser.getCountryCd() ) );
        f.setRoles( currentUser.getRoles() );

        return model;
    }

    /**
     * Update the profile information with the content of the input form. (Afterwards redirect to home page)
     * 
     * @param form
     *            The profile input form
     * @param result
     *            The binding result
     * @return String "redirect:/" or null in case of validation errors
     */
    @RequestMapping( method = RequestMethod.POST )
    public String updateProfile( ProfileForm form, BindingResult result, @RequestParam String action ) {

        if ( action.equals( "save" ) ) {
            if ( result.hasErrors() ) {
                return null;
            }
    
            // Will throw exception when logon was not OK
            User theUser = securityChecker.getLoggedOnUser();
    
            // Move the info from the input form over to the user
            theUser.setName( form.getName() );
    
            // The country selected in the combobox = the full name.
            for ( Map.Entry<String, String> e : myCountriesViaCode.entrySet() ) {
                if ( e.getValue().equals( form.getCountry() ) ) {
                    theUser.setCountryCd( e.getKey() );
                    break;
                }
            }
    
            // Save the user in persistent storage
            userDAO.saveUser( theUser );
    
        } else {
            if ( action.equals( "todo-list" ) ) {
                // Redirect to the todo-list
                return "redirect:/todo-list.htm";
            }
            if ( action.equals( "roles-list" ) ) {
                // Redirect to the roles-list
                return "redirect:/roles-list.htm";
            }
        }
        
        // Redirect back to the home page
        return "redirect:/";
    }

    /**
     * Called by dependency injection to set the user DAO on this instance.
     * 
     * @param aUserDAO
     *            the userDAO to set
     */
    public void setUserDAO( IUserDAO aUserDAO ) {
        userDAO = aUserDAO;
    }

    /**
     * @param aSecurityChecker The securityChecker to set.
     */
    public void setSecurityChecker( UserLoggedOn aSecurityChecker ) {
        securityChecker = aSecurityChecker;
    }

}
