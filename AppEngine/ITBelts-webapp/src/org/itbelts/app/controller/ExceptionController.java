package org.itbelts.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.itbelts.exception.UnknownResourceException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

@Controller
public class ExceptionController extends AbstractController {

    @RequestMapping( value = "/**", method = RequestMethod.GET )
    protected ModelAndView handleRequestInternal( HttpServletRequest request, HttpServletResponse response ) throws Exception {
        throw new UnknownResourceException( "There is no resource for path " + request.getRequestURI() );
    }

    @RequestMapping( value = "/exception.htm", method = RequestMethod.POST )
    protected ModelAndView handlePostedException() throws Exception {
        return new ModelAndView( "exception" );
    }

    @RequestMapping( value = "/exception.htm", method = RequestMethod.GET )
    protected ModelAndView handleGetException() throws Exception {
        return new ModelAndView( "exception" );
    }
}
