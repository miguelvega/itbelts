package org.itbelts.app.view;

import java.util.ArrayList;
import java.util.List;

import org.itbelts.domain.AbstractType;
import org.itbelts.domain.Specialty;
import org.itbelts.domain.Topic;

/**
 * Class that contains specific Topic information suitable for showing on the screen.
 * The class is basic Topic information extended with extra information from Specialty, Exams...
 * <br><br>
 * <u><i>Version History</i></u>
 * <pre>
 * v2013.10.0 21-okt.-2013 - DKBR813 - initial release
 *
 * </pre>
 *
 * @version v2013.10.0      21-okt.-2013
 * @author <a href="mailto:koenbruyndonckx@gmail.com"> Koen Bruyndonckx </a>
 */
public class TopicView extends AbstractType {

    private static final long serialVersionUID = -8956987413647692373L;

    private Topic topic;
    private Specialty specialty;
    private List<ItemLink> links = new ArrayList<ItemLink>();
    
    /**
     * Simple Constructor
     */
    public TopicView() {
    }

    /**
     * @return Returns the topic.
     */
    public Topic getTopic() {
        return topic;
    }

    /**
     * @param aTopic The topic to set.
     */
    public void setTopic( Topic aTopic ) {
        topic = aTopic;
    }

    /**
     * @return Returns the specialty.
     */
    public Specialty getSpecialty() {
        return specialty;
    }

    /**
     * @param aSpecialty The specialty to set.
     */
    public void setSpecialty( Specialty aSpecialty ) {
        specialty = aSpecialty;
    }

    /**
     * @return Returns the links.
     */
    public List<ItemLink> getLinks() {
        return links;
    }

    /**
     * @param aLinks The links to set.
     */
    public void setLinks( List<ItemLink> aLinks ) {
        links = aLinks;
    }
    
}
