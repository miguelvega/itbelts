<topic>
<title>Web Java Fundamentals - Workshop</title>
<topic>
<title>Forum</title>
<para>During this exercise, we develop a small forum site.</para>
<topic>
<title>Data</title>
<para>Like any forum, we have users, topics and posts. </para>
<para>Users and topics (test data) are created by your program, there is no user interface for managing them. On the contrary, posts are created through a form of the web site.</para>
<para>The lists of topics and Users are stored in the servlet context and their population (data creation) triggered by when the application starts.</para>
</topic>
<topic>
<title>Pages</title>
<para>The site contains the following pages:</para>
<ul>
<li>Home: with the list of threads.</li>
<li>Topic: displays the list of the posts of a specific topic.</li>
<li>PostEdit: Form to add a new post to a specific topic.</li>
<li>Login: form with username/password fields.</li>
<li>UserList: list of all users.</li>
</ul>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/BBForum.JPG'/></div>
<para><i>See below for larger and more images</i></para>
</topic>
<topic>
<title>Topics and Posts</title>
<para>On the home page, each topic is a link to the corresponding topic page (with the list of posts).</para>
<para>The topic page contains a "reply" button. When the user presses the button, he goes to the PostEdit page to create a new post for that topic. That PostEdit page contains a form with a large text area (multi line field) and ok/cancel buttons. After ok or cancel has been pressed, the thread page is displayed with the list of all posts for that topic (including our the new post).</para>
</topic>
<topic>
<title>Login</title>
<para>Users cannot be created through the UI, but users can login/logout. On the home page, there is a login link when no users is logged in. That link is replaced by the user name and a logout link when the user is logged in. The login link shows another page with a classic username/password form and ok/cancel buttons. Once logged in, users return to the home page.</para>
</topic>
<topic>
<title>Last Access</title>
<para>For each user, you store the last time that user accessed a page of the web site (which is usually more recent than its last login). You do that systematically. The UserList screen (linked from the home page) lists all the users in a table, with the last access date/time for each user.</para>
</topic>
<topic>
<title>Screen Shots</title>
<para>This is an example of a solution, on the end-user point of view.</para>
<para>The welcome page:</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/index.JPG'/></div>
<para>The Home page with the list of the topics :</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/HomeServlet.JPG'/></div>
<para>The Topic page who appears by clicking on one topic : </para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/TopicServlet.JPG'/></div>
<para>There's is no way to reply without login so here comes the login page :</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/LoginServlet.JPG'/></div><div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/LoginServlet2.JPG'/></div><div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/TopicServlet2.JPG'/></div>
<para>Reply link appears to add a post with the edit page :</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/PostEditServlet.JPG'/></div>
<para>There's your post on the topic page :</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/TopicServlet3.JPG'/></div>
<para>Finaly, from the welcome page, you can go to the access page to consult the last user access :</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/AccessServlet.JPG'/></div>
</topic>
</topic>
<topic>
<title>Auction</title>
<para>For this exercise, we build an auction site.</para>
<topic>
<title>Data</title>
<para>There are items for sale. Users bids on items. Each bid is made by only one user.</para>
<para>Items and users are automatically create by your application. There is no page to add/modify/remove them.</para>
<para>Each item has an associated picture. In a normal site, such a picture is part of the DB, or a part of the file system that users modify (through upload pages). We do not provide such a dynamic system in our exercise. For this exercise, just put the pictures somewhere in the WebContent directory when programming; they will be deployed with your application.</para>
<para>Items have also a closing date (and time). After that date, it is no more possible to bid on the item.</para>
</topic>
<topic>
<title>Pages</title>
<ul>
<li>ItemList : list all the available items (clickable).</li>
<li>Item: details one item, with picture and bids.</li>
<li>Login: enables a user to enter user and password.</li>
</ul>
<para>The ItemList contains the list of all items of the system. Each item is clickable and links to the Item page.</para>
<para>The Item page contains the name of an item and a picture.<br/>
It also contains the list of the bids that users made on that item, with on the top of them, a form for the visitor to make a new bid.</para>
</topic>
<topic>
<title>Place a Bid</title>
<para>When the user makes a bid from the item page, the following cases can happen:</para>
<ul>
<li>No user is logged in. The application redirects to the login page. When login succeeds, the pending bid is done. This feature is not the easiest of the exercise to implement. Then the Item page is displayed again, with a success message.</li>
<li>The bid is too low (a higher bid exists). The item page is re-displayed with the error message.</li>
<li>The bid is too late (item close date reached). The item page is re-displayed with the error message. This may happen if the item page is displayed while the date is not yet reached, and the button of the form is pressed too late.</li>
<li>The string input by the user is no legal amount (not a number for example). The item page is re-displayed with the error message.</li>
</ul>
</topic>
<topic>
<title>No Cache</title>
<para>No page of the site may be cached by a browser or a firewall.</para>
<para>To enable that, you need to set response headers for <b>every</b> request as this:</para>
<para><code type='display'>//Set the date of this response</para>
<para>response.setDateHeader("Date", new Date().getTime());</para>
<para>//Set all the pages to expire at a older date (they are already expired)</para>
<para>response.setDateHeader("Expires", 0);</para>
<para>// Set standard HTTP/1.1 no-cache headers</para>
<para>response.setHeader("Cache-Control","no-cache, must-revalidate, s-maxage=0, proxy-revalidate, private");</para>
<para>// Set standard HTTP/1.0 no-cache header</para>
<para>response.setHeader("Pragma", "no-cache");</para>
<para></code></para>
</topic>
</topic>
<topic>
<title>Solutions</title>
<topic>
<title>Forum</title>
<topic>
<title>Domain Model</title>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/ForumSolution.png'/></div>
</topic>
<topic>
<title>Listener</title>
<para>First, we have to populate the domain and attach the data to the servletContext (so they can be found from the servlets later, when the requests come).</para>
<para>These lists of data would not exist if we had a DB and Dao objects.</para>
<para><code type='display'>ServletContext sc = event.getServletContext();</para>
<para>sc.setAttribute("userList" , new UserList() );</para>
<para>sc.setAttribute("topicList", new TopicList());</para>
<para>sc.setAttribute("postList" , new PostList() );</para>
<para></code></para>
<para>Here, a "___List" is just a class who instantiates the domain model objects and keep them in a Collection.</para>
</topic>
<topic>
<title>WebUtil</title>
<para>Now, we need an easy way to get the lists of object from anywhere.</para>
<para>A class WebUtil with static methods fits perfect for this job.</para>
<para><code type='display'>public static TopicList getTopicList(ServletContext context){</para>
<para>    return (TopicList) context.getAttribute(&quot;topicList&quot;);</para>
<para>}	</para>
<para>public static UserList getUserList(ServletContext context){</para>
<para>    return (UserList) context.getAttribute(&quot;userList&quot;);</para>
<para>}	</para>
<para>public static PostList getPosts(ServletContext context){</para>
<para>    return (PostList) context.getAttribute(&quot;postList&quot;);</para>
<para>}	</para>
<para></code></para>
</topic>
<topic>
<title>Home</title>
<para>To display the topic list, we need a simple loop. The collection used is obtained through one of the static method of WebUtil.</para>
<para><code type='display'>TopicList topicList = WebUtil.getTopicList(this.getServletContext());</para>
<para>for (Topic iTopic : topicList.getTopicList()) {</para>
<para>    out.print(&quot;&lt;p&gt;&lt;table&gt;&quot;);</para>
<para>    out.print(&quot;&lt;tr&gt;&lt;td&gt;&lt;a href=TopicServlet?topicId=&quot; + iTopic.getId() + &quot;&gt;&quot; + iTopic.getTitle() + &quot;&lt;/a&gt;&lt;/td&gt;&quot;);</para>
<para>    out.print(&quot;&lt;td&gt;&quot; + iTopic.getUser().getLogin() + &quot;&lt;/td&gt;&quot;);</para>
<para>    out.print(&quot;&lt;td&gt;&quot; + DateFormat.getInstance().format(iTopic.getDate()) + &quot;&lt;/td&gt;&lt;/tr&gt;&quot;);</para>
<para>    out.print(&quot;&lt;/table&gt;&lt;/p&gt;&quot;);</para>
<para>}</para>
<para></code></para>
</topic>
<topic>
<title>Login</title>
<para>This is a way to get the parametres from the login form and test the fields values</para>
<para>If the Login and password matches, the User is put in the session with the method setAttribute </para>
<para><code type='display'>if(users.get(loginParam)!=null){</para>
<para>	if(users.get(loginParam).getPsw().equals(pswParam)){</para>
<para>		session.setAttribute(&quot;user&quot;, users.get(loginParam));</para>
<para>		out.println(&quot;&lt;p&gt;Welcome &quot; + loginParam + &quot; !&lt;/p&gt;&quot;);</para>
<para>	} else{</para>
<para>		out.println(&quot;&lt;p&gt;Wrong password&lt;/p&gt;&quot;);</para>
<para>	}</para>
<para>	out.println(&quot;&lt;p&gt;User unknown&lt;/p&gt;&quot;);</para>
<para>}</para>
<para></code></para>
<para>And with this little piece of code on each page, you can display the name of the logged users</para>
<para><code type='display'>HttpSession session = request.getSession();</para>
<para>User user = (User)session.getAttribute(&quot;user&quot;);</para>
<para>if((User)session.getAttribute(&quot;user&quot;)!=null){</para>
<para>    out.println(&quot;&lt;p&gt;&lt;i&gt;&lt;a href='LoginServlet'&gt;&quot; + user.getLogin() + &quot;&lt;/a&gt;&lt;/i&gt;&lt;/p&gt;&quot;);</para>
<para>} else {</para>
<para>    out.println(&quot;&lt;p&gt;&lt;a href='LoginServlet'&gt;[Login]&lt;/a&gt;&lt;/p&gt;&quot;);</para>
<para>}</para>
<para></code></para>
<para>It could be placed in a little reusable method.</para>
</topic>
<topic>
<title>Topic</title>
<para>In the TopicServlet.doPost() method, this code will get the value of each field of the Post the user is adding (through a form).</para>
<para>You can test and add the new Post into the postList.</para>
<para><code type='display'>if((request.getParameter(&quot;content&quot;) != null)){</para>
<para>    postList.addPost(new Post(topic,postContent,user));</para>
<para>    out.print(&quot;&lt;p&gt;&lt;i&gt;Post added !&lt;/i&gt;&lt;/p&gt;&quot;);</para>
<para>} else{</para>
<para>    out.print(&quot;&lt;p&gt;&lt;i&gt;!!! Post error !!!&lt;/i&gt;&lt;/p&gt;&quot;);</para>
<para>}</para>
<para></code></para>
<para>Finally, you have all the object needed to perform the Post loop.</para>
<para>You just have to test if the Topic attribute of the Post is the same than the Topic Parameter.</para>
<para><code type='display'>Topic topic       = WebUtil.getTopicList(this.getServletContext()).getTopic(topicId);</para>
<para>User  user        = WebUtil.getUserList(this.getServletContext()).getUser(userLogin);</para>
<para>PostList postList = WebUtil.getPosts(this.getServletContext());</para>
<para>out.print(&quot;&lt;table&gt;&quot;);</para>
<para>out.println(&quot;&lt;tr&gt;&lt;td&gt;&lt;b&gt;&quot; + topic.getTitle() + &quot;&lt;/b&gt;&lt;/td&gt;&quot;);</para>
<para>out.println(&quot;&lt;td style='text-align: right;'&gt;&lt;i&gt;from &quot; + topic.getUser().getLogin() + &quot;&lt;/i&gt;&lt;/td&gt;&lt;/tr&gt;&quot;);</para>
<para>out.println(&quot;&lt;tr&gt;&lt;td&gt;&quot; + topic.getContent() + &quot;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&quot;);</para>
<para>if(postList.getPostList().size()!=0) {</para>
<para>    for(Post iPost : postList.getPostList()) {</para>
<para>        if(topic.getId().equals(iPost.getTopic().getId())) {</para>
<para>       	    out.print(&quot;&lt;p&gt;&lt;table style='border:1px solid grey; width: 350px; -moz-border-radius:7px;'&gt;&quot;);</para>
<para>            out.print(&quot;&lt;tr&gt;&lt;td style='text-align: right;'&gt;&lt;i&gt;from &quot; + iPost.getUser().getLogin()  + &quot;&lt;/i&gt;&lt;/td&gt;&lt;/tr&gt;&quot;);</para>
<para>            out.print(&quot;&lt;tr&gt;&lt;td&gt;&quot; + iPost.getContent() + &quot;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/p&gt;&quot;);</para>
<para>        }</para>
<para>    }</para>
<para>}</para>
<para>out.print(&quot;&lt;/table&gt;&quot;);	</para>
<para></code></para>
</topic>
</topic>
<topic>
<title>Auction</title>
<topic>
<title>Domain Model</title>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/11543557/JBaySolution.png'/></div>
<para>You can also have a relationship between user and item, as if users were placing items on the site (instead of the site administrators).</para>
</topic>
<topic>
<title>Project</title>
<para>Download the zipped Eclipse project and import it in your IDE.</para>
<attachment src='http://knowledgeblackbelt.com/gen/course/11543557/JBaySolution.zip'> <graphic src='http://knowledgeblackbelt.com/gen/course/11543557/zip.png' /> </attachment>

<para>As described in the course, the Spring <i>applicationContext.xml</i> file is in the <i>WebContent/WEB-INF/classes</i> directory, placed by the Java compiler. That way it both in the classpath and in the web directory.</para>
<para>Two programs access that applicationContext.xml file:</para>
<ul>
<li>The web application, via the Spring class <i>ContextLoaderListener</i> declared in the web.xml.</li>
<li>The program DataCreator.main() which can be run to create the DB and load fake data. It uses a ClassPathXmlApplicationContext.</li>
</ul>
<para>If you want to test the project, you will have to use your JPA/Hibernate skills and:</para>
<ul>
    <li>Have a RDB.</li>
    <li>Adapt the persistence.xml file with your JDBC settings (driver and connection strings).</li>
    <li>Adapt the persistance.xml file hbm2ddl settings to create the DB.</li>
    <li>Add the JDBC driver (jar file) of your DB in the WEB-INF/lib directory.</li>
    <li>Run DataCreator</li>
    <li>Adapt the persistance.xml file hbm2ddl settings to <b>not</b> create the DB.</li>
    <li>Start the web application</li>
</ul>
</topic>
</topic>
</topic>
</topic>
