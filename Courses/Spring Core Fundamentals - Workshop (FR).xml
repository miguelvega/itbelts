<topic>
<title>Spring Core Fundamentals - Workshop</title>
<para>Cet atelier va vous guider à travers la création de petites applications pour mettre en pratique toutes les choses que vous avez vues dans le cours Spring Core Fundamentals. De cette façon, vous assimilerez comme tout cela marche ensemble.</para>
<para>Pour chaque application, créez un nouveau projet Eclipse avec la structure appropriée.</para>
<topic>
<title>Gestion des Utilisateurs</title>
<para>Dans ce premier exercice de l'atelier, vous allez créer un système de gestion de User/Status en utilisant le framework Spring.</para>
<topic>
<title>Mise en Place</title>
<para>Commencez par configurer votre environnement :</para>
<ul>
    <li>Configurez un ApplicationContext basique.</li>
    <li>Paramétrez une base de données HSQLDB.</li>
</ul>
</topic>
<topic>
<title>Persistance de Base</title>
<para>Utilisez un Template Jdbc dans un UserDAO pour remplir la base de données avec des objets User. Définissez vous-même les données qui composent un User.</para>
<para>Optionnel : récupérez les données pour créer vos Users depuis la console. Vous pouvez pour cela vous référer à la JavaDoc de <a href="http://java.sun.com/javase/6/docs/api/java/io/Console.html">Console</a> ou <a href="http://java.sun.com/javase/6/docs/api/java/util/Scanner.html">Scanner</a> pour apprendre comment lire des entrées dans la console sous Java 1.5+.</para>
<para>Injectez la classe DAO dans l'instance de Service nouvellement créée.</para>
</topic>
<topic>
<title>I18N</title>
<para>Configurez l'i18n avec un MessageSource et utilisez un i18nService pour formater les données extraites depuis la base de données.</para>
<para>Laissez l'utilisateur choisir une Locale lors de sa création.</para>
</topic>
<topic>
<title>JPA</title>
<para>Créez une classe métier StatusUpdate et un StatusUpdateDAO qui utilise l'EntityManager. Chaque StatusUpdate contient un message et une référence vers un User.</para>
<para>Transformez votre UserDAO pour qu'il utilise aussi l'EntityManager.</para>
<para>Ajouter une collection de mises à jour de status à la classe User.</para>
<para>Testez unitairement la récupération d'un User dans le UserDAO en utilisant une implémentation stub de l'EntityManager. Vous pouvez vous référer à l'article de Martin Fowler <a href="http://martinfowler.com/articles/mocksArentStubs.html">Mocks Aren't Stubs</a>.</para>
</topic>
<topic>
<title>AOP</title>
<para>Créez un aspect de logging qui va tracer chaque appel.</para>
<para>Créez un aspect qui affiche des messages de félicitation à chaque fois qu'un utilisateur a envoyé 10 mises à jour de son status.</para>
<para>Utilisez les transactions pour prendre en compte des entrées invalides (c'est à dire mettez à jour le status de l'Utilisateur, mettez à jour le compteur, et lancez une exception sur l'insertion du status, assurez vous alors que l'incrémentation du compteur a bien été annulée).</para>
<para>Utilisez une annotation @Secure pour permettre l'accès à la classe uniquement aux utilisateurs connectés. Essayez avec un scope prototype et un BeanPostProcessor, puis avec un aspect. Vous aurez aussi sûrement besoin d'un ThreadLocal.</para>
<para>Optionnel : Créez un aspect qui envoie le StatusUpdate à Twitter. Cherchez l'une des nombreuses librairies Java qui fait cela pour vous aider.</para>
</topic>
<topic>
<title>Optionnel</title>
<para>Les sujets suivants ne sont pas couverts dans le cours, mais sont intéressants à résoudre si vous avez le temps.</para>
<para><b>@Configurable</b></para>
<para>Utilisez l'annotation @Configurable pour vous débarrasser du UserDAO et avoir des méthodes d'instance telles que User.save(), User.delete(), etc... et des méthodes statiques telles que User.get(Long), User.findByName(). Référez vous à <a href="http://static.springsource.org/spring/docs/2.5.x/reference/aop.html#aop-atconfigurable">l'utilisation d'AspectJ pour faire de l'injection de dépendances des objets du domaine avec Spring</a>.</para>
</topic>
</topic>
<topic>
<title>Fast Food</title>
<para>Dans cet exercice de l'atelier, nous allons construit un FastFood pour produire des burgers (miam miam). Il y aura 3 itérations principales :</para>
<ul>
    <li>La construction et la liaison entre les classes</li>
    <li>L'ajout du support de JPA</li>
    <li>L'ajout de l'audit avec de l'AOP</li>
</ul>
<para>Commençons par mettre en place nos objets métier.</para>
<topic>
<title>Classes et Responsabilités</title>
<para><b>BurgerType</b></para>
<para>BurgerType est une énumération qui décrit les différents types de burgers que l'on produit.</para>
<para>Les valeurs possibles de l'énumération sont :</para>
<ul>
    <li>FISH_FILET</li>
    <li>CHICKEN_SUPREME</li>
    <li>ROYAL_WITH_CHEESE</li>
    <li>SUPREME_POUNDER</li>
    <li>BACON_QUARTER_POUNDER</li>
</ul>
<para><b>FastFood</b></para>
<para>FastFood est une interface qui définit 2 méthodes publiques</para>
<ul>
    <li>public Burger deliverBurger(BurgerType) : basée sur un BurgerType donné, elle créée une instance d'objet Burger</li>
    <li>public List<Burger> prepareOrder() : retourne la liste des objets Burger</li>
</ul>
<para>Les détails sur l'implémentation des méthodes se trouvent dans FastFoodImpl.</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/12302337/fastfood.jpg'/></div>
<para><b>FastFoodImpl</b></para>
<para>FastFoodImpl est la classe centrale de notre application. Elle est injectée avec</para>
<ul>
    <li>un SecretRecipeBook (livre de recettes secret)</li>
    <li>une CookTeam (équipe de cuisine)</li>
</ul>
<para>Dans la méthode public Burger deliverBurger(BurgerType) vous allez :</para>
<ul>
    <li>Récupérer la liste des ingrédients requis et leurs quantités en utilisant le livre de recettes</li>
    <li>Demande à la CookTeam de préparer le burger</li>
</ul>
<para>Dans la méthode public List<Burger> prepareOrder() vous allez parser un fichier burger.txt qui contient la description de l'Order (la commande). Le format est simplement BURGER_TYPE nombre. Par exemple :</para>
<para>         FISH_FILET 1</para>
<para>         CHICKEN_SUPREME 2</para>
<para>         ROYAL_WITH_CHEESE 1</para>
<para>         SUPREME_POUNDER 5</para>
<para>         BACON_QUARTER_POUNDER 6</para>
<para>Note: utilisez la méthode valueOf() inclue par défaut dans votre énumération pour convertir un String en instance de l'énumération.</para>
<para>Déléguez aussi la création du burger à la méthode deliverBurger(BurgerType). Cela signifie que si le fichier contient CHICKEN_SUPREME 2 vous allez devoir appeler deliverBurger() deux fois avec le type BurgerType.CHICKEN_SUPREME en argument.</para>
<para>Assurez vous que votre FastFoodImpl contient une instance de Logger Log4j qui affiche quelque chose sur la sortie standard à chaque fois qu'un burger est demandé ou livré.</para>
<para><b>SecretRecipeBook</b></para>
<para>Le SecretRecipeBook est utilisé par le FastFoodImpl pour savoir combien d'ingrédients sont requis pour un type de burger donné :</para>
<para>        public Map<Ingredient, Integer> getIngredientsAndQuantities(BurgerType)</para>
<para>Vous déciderez de son implémentation interne.</para>
<para><b>Ingredient</b></para>
<para>Ingredient est une simple classe qui définit l'ensemble d'ingrédients utilisés pour cuisiner un burger. Elle contient simplement :</para>
<ul>
    <li>String : Type</li>
    <li>String : Description</li>
</ul>
<para><b>CookTeam</b></para>
<para>La CookTeam est responsable de la préparation et cuisson des burgers. On y injecte le StockServive et l'OrderService (voir ci-dessous). La méthode métier principale de cette classe est :</para>
<para>    public Burger prepareBurger(Map<Ingredients, Integer>, BurgerType type)</para>
<para>La CookTeam va récupérer les ingrédients en utilisant la classe StockService. Si malheureusement, le StockService ne contient pas assez d'ingrédients, la CookTeam va recevoir une Exception (voir la description de la classe StockService) et est responsable de l'ajout de l'Order (la commande) nécessaire dans le OrderService (voir ci-dessous).</para>
<para><b>StockService</b></para>
<para>StockService expose cette méthode :</para>
<para>    public void verifyAndUpdateStock(Map<Ingredient, Integer>) throws a NotEnoughIngredientsException()</para>
<para>Pour l'instant, vous êtes responsable de l'implémentation interne de la classe.</para>
<para><b>NotEnoughIngredientsException</b></para>
<para>Une checked exception qui contient les ingrédients manquants. On peut obtenir les ingrédients manquants en appelant :</para>
<para>   public Map<Ingredient, Integer> getMissingIngredient()</para>
<para><b>OrderService</b></para>
<para>Le OrderService mémorise ce que devra contenir la prochaine Order, la classe expose deux méthodes :</para>
<ul>
    <li>public void addToNextOrder(Map<Ingredient, Integer>)</li>
    <li>public void placeOrder()</li>
</ul>
<para>Pour le moment, on garde l'implémentation simple</para>
<ul>
    <li>addToNextOrder ajoute simplement à une Map en interne le nombre d'ingrédients de la prochaine commande.</li>
    <li>placeOrder utilise un Logger pour afficher le contenu de la commande sur la sortie standart.</li>
    <li></li>
</ul>
<para>Maintenant que vous avez découvert les classes principales, écrivez les et relier les avec Spring. Vous pouvez utiliser la méthode que vous préférez (définition des beans en xml ou avec des annotations).</para>
<para>Quand vous aurez fini, créez une classe exécutable (avec une méthode <code type='inline'>public static void main(String[] args))</code> qui récupère le bean FastFood et prépare la commande demandée.</para>
</topic>
<topic>
<title>Ajout de JPA</title>
<para>Nouveau besoin : nous voulons ajouter l'utilisation d'une base de données pour notre application en utilisant JPA.</para>
<para><b>Ingredients</b></para>
<para>Les Ingredients sont maintenant des entités. Même si le champ type aurait pu faire une bonne clé primaire, nous allons ajouter un champ id de type Long à la place.</para>
<para><b>Recipe et RecipeBook</b></para>
<para>RecipeBook retourne maintenant une entité Recipe (une nouvelle classe) qui est récupérée en base de données. La classe Recipe contient :</para>
<ul>
    <li>un champ id de type Long</li>
    <li>une champ description de type String</li>
    <li>une Map<Ingredient, Integer> (vois les Extensions Hibernate pour de l'aide). Si vous ne vous sentez pas à l'aise avec le mapping d'une Map, utilisez un Set<Ingredient></li>
</ul>
<para>Note: La méthode de CookTeam prepareBurger() est adaptée pour prendre en paramètre une Recipe :</para>
<para>    public Burger prepareBurger(Recipe recipe)</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/12302337/fastfood-jpa-changes.jpg'/></div>
<para><b>Gestion du Stock</b></para>
<para>Le status du Stock est maintenant mémorisé dans la base de données, une nouvelle classe appelée StockItem est créée. StockItem contient :</para>
<ul>
    <li>un Ingredient</li>
    <li>une quantité (Integer)</li>
</ul>
<para><b>Gestion des Commandes</b></para>
<para>Les Orders sont aussi stockés en base de données. Créez une nouvelle entité Order qui contient :</para>
<ul>
    <li>un id</li>
    <li>un Set de Orderlines</li>
</ul>
Un objet Orderline contient :<ul>
    <li>un Ingredient</li>
    <li>une quantité (Integer)</li>
</ul>
<para><b>DAOs</b></para>
<para>Après avoir créé les entités, créez les DAOs associés et injectez les dans les services correspondants.</para>
<para>Quand vous aurez fini, testez votre programme de nouveau.</para>
</topic>
<topic>
<title>IRSService</title>
<para>L'IRS porte une attention toute particulière à notre FastFood et veut être alerté à chaque fois qu'un burger est livré.</para>
<para>Écrivez une classe IRSService et ajoutez un aspect qui déclenche l'appel de IRSService.burgerDelivered(Burger burger), cette méthode pourrait alors appeler un service distant pour prévenir effectivement l'IRS, mais nous allons simplement utiliser un Logger pour afficher du texte sur la sortie standard.</para>
</topic>
<topic>
<title>AuditService</title>
<para>Nouveau besoin : il semblerait que des objets disparaissent du Stock plus vite que ce qu'il serait normalement nécessaire pour fabriquer les burgers commandés ! Un audit est nécessaire. Nous avons besoin de tracer tout accès au SecretRecipeBook et au Stock.</para>
<para>La classe AuditDAO définit une méthode void addAuditTrace(AuditTrace trace) qui va stocker un AuditTrace en base de données.</para>
<para>Une entité AuditTrace est composée de :</para>
<ul>
    <li>Un id de type Long</li>
    <li>Une énumération de catégorie (STOCK, RECIPE_BOOK, ...)</li>
    <li>Une description de type String</li>
    <li>Un TimeStamp pour enregistrer la moment où l’événement s'est produit</li>
</ul>
<para>De plus, une fois qu'une AuditTrace a été enregistrée, cela n'aurait aucun sens qu'elle puisse être modifiée. Rendez la classe AuditTrace immuable. Rappelez vous que les constructeurs peuvent être protected.</para>
<para>Utilisez un aspect qui délègue à l'AuditTraceDAO pour s'assurer que l'audit peut être activé ou désactivé facilement.</para>
</topic>
</topic>
</topic>
