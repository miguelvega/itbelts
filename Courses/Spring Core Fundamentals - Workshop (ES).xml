<topic>
<title>Workshop - Fundamentos de Spring Core</title>
<para>Este workshop the guia a través de la creación de pequeñas aplicaciones para poner en practica todos los conceptos vistos en el curso Fundamentos de Spring Core. De esta manera, podrás asimilar como funciona todo en conjunto.</para>
<para>Para cada aplicación, crea un nuevo proyecto en Eclipse con la estructura apropiada.</para>
<topic>
<title>Administración de usuarios</title>
<para>En este primer ejercicio del workshop vas a crear un sistema de administración de User/Status con el framework de Spring.</para>
<topic>
<title>Configuración</title>
<para>Empieza por configurar tu ambiente</para>
<ul>
    <li>Configura un ApplicationContext básico</li>
    <li>Configura un data source de HSQLDB.</li>
</ul>
</topic>
<topic>
<title>Persistencia básica</title>
<para>Usa un JdbcTemplate en el UserDAO para poblar la BD con objetos User. Define que elementos componen a un User.</para>
<para>Opcional: desde la consola pide la información para agregar Users. <a href="http://java.sun.com/javase/6/docs/api/java/io/Console.html">Console</a> o <a href="http://java.sun.com/javase/6/docs/api/java/util/Scanner.html">Scanner</a> para la versión 1.5 en adelante de Java, las maneras de leer la entrada desde la consola.</para>
<para>Inyecta la clase DAO en una nueva clase Service.</para>
</topic>
<topic>
<title>I18N</title>
<para>Configura i18n con un MessageSource y usa un formato de datos i18nService obtenido desde la BD.</para>
<para>Permite que el usuario indique su Locale cuando se crea el objeto.</para>
</topic>
<topic>
<title>JPA</title>
<para>Crea una clase de dominio StatusUpdate y un StatusUpdateDAO que utilice un EntityManager. Cada StatusUpdate contiene un mensaje una referencia a un User.</para>
<para>Migra el UserDAO al EntityManager.</para>
<para>Agrega una colección a la clase User con las actualicaciones del status.</para>
<para>Crea una prueba unitaria para obtener un User en el UserDAO al usar la implementación del stub del EntityManager. cf. Martin Fowler's <a href="http://martinfowler.com/articles/mocksArentStubs.html">Mocks Aren't Stubs</a> </para>
</topic>
<topic>
<title>AOP</title>
<para>Crea un aspecto de logging que registre cada una de las llamadas que se hacen.</para>
<para>Crea un aspecto que imprima un mensaje de felicitaciones cada vez que un usuario envie 10 actualizaciones de status.</para>
<para>Usa transacciones para manejar las entradas inválidas (ejemplo, actualizar el contador de las actualizaciones de status del User, entonces al insertar la actualización del status falla lanzando una excepción, asegurate que el incremento del contador haga un rolled back)</para>
<para>Usa la anotación @Secured que permite el acceso a la clase solamente a los usuarios que están logeados. Intentalo con un alcance prototype y la clase BeanPostProcessor, después con un aspecto. Probablemente necesites también de un ThreadLocal.</para>
<para>Opcional: Crea un aspecto que envie el StatusUpdate a Twitter. Busca una librería de Java que haga esto para ayudarte. Existen muchas.</para>
</topic>
<topic>
<title>Opcional</title>
<para>El siguiente tema no se vio en el curso, pero es interesante si es que tienes tiempo.</para>
<para><b>@Configurable</b></para>
<para>Usa la anotación @Configurable para deshacerte de UserDAO y tener un métodos de instancia como User.save(), User.delete(), etc. y un método estático como User.get(Long), User.findByName(). cf. <a href="http://static.springsource.org/spring/docs/2.5.x/reference/aop.html#aop-atconfigurable">Using AspectJ to dependency inject domain objects with Spring</a>.</para>
</topic>
</topic>
<topic>
<title>Fast Food</title>
<para>En este ejercicio del workshop vamos a crear un FastFood para producir hamburgesas (yum yum). Hay 3 iteraciones principales:</para>
<ul>
    <li>Construir y vincular las clases</li>
    <li>Agregar soporte JPA</li>
    <li>Agregar auditoría mediante AOP</li>
</ul>
<para>Comencemos creando el dominio de objetos.</para>
<topic>
<title>Clases y Responsabilidades</title>
<para><b>BurgerType</b></para>
<para>BurgerType es una enumeración que describe los diferentes tipo s de hamburguesas que producimos.</para>
<para>Los valores del enum pueden ser:</para>
<ul>
    <li>FISH_FILET</li>
    <li>CHICKEN_SUPREME</li>
    <li>ROYAL_WITH_CHEESE</li>
    <li>SUPREME_POUNDER</li>
    <li>BACON_QUARTER_POUNDER</li>
</ul>
<para><b>FastFood</b></para>
<para>FastFood es una interface que define 2 métodos públicos:</para>
<ul>
    <li>public Burger deliverBurger(BurgerType) : Dependiendo del BurgerType que se pasa como parámetro es el objeto Burger que se crea</li>
    <li>public List<Burger> prepareOrder() : regresa una lista de objetos Burger</li>
</ul>
<para>más detalles de la implementación del método en FastFoodImpl</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/12302337/fastfood.jpg'/></div><span style='color:red;'>Format error: This tag is empty, it has no name. You can either enclose your text in between [CODE escape='true' inline='true'] and [/CODE] or else you can escape your brackets with html '&amp;#91;' and '&amp;#93;' escape characters (respectively for '[' and ']' characters).</span>
<para><b>FastFoodImpl</b></para>
<para>FastFoodImpl es la clase central de nuestro aplicación. Es inyectada en:</para>
<ul>
    <li>a SecretRecipeBook</li>
    <li>a CookTeam</li>
</ul>
<para>En el método public Burger deliverBurger(BurgerType) harás lo siguiente:</para>
<ul>
    <li>Obtener la lista de los ingredientes necesarios y sus cantidades usando el libro de recetas</li>
    <li>Pedirle al CookTeam que prepare la hamburguesa</li>
</ul>
<para>En el método public List<Burger> prepareOrder() parsearas el archivo burger.txt que contiene la descripción de la Order. El formato simplemente es el BURGER_TYPE cantidad. Ejemplo:</para>
<para>         FISH_FILET 1</para>
<para>         CHICKEN_SUPREME 2</para>
<para>         ROYAL_WITH_CHEESE 1</para>
<para>         SUPREME_POUNDER 5</para>
<para>         BACON_QUARTER_POUNDER 6</para>
<para>Nota: usa el método valueOf() de los enums para convertir de un String a una instancia de tipo enum.</para>
<para>También debes de delegar la creación de la hamburguesa al método deliverBurger(BurgerType). Lo que significa que si el archivo tiene CHICKEN_SUPREME 2 deberás de llamar al método deliverBurger() dos veces con BurgerType.CHICKEN_SUPREME como argumento.</para>
<para>Asegurate de que FastFoodImpl contenga una instancia de Log4j Logger que imprima algo a la salida estándar cada vez que se pida una hamburguesa y se entregue.</para>
<para><b>SecretRecipeBook</b></para>
<para>El SecretRecipeBook es usada por FastFoodImpl para saber la cantidad de ingredientes que se necesitan para cierto tipo de hamburguesa:</para>
<para>        public Map<Ingredient, Integer> getIngredientsAndQuantities(BurgerType)</para>
<para>Esta implementación interna queda a tu consideración.</para>
<para><b>Ingredient</b></para>
<para>Ingredient es una clase simple que define el set de ingredientes usados para cocinar una hamburguesa. Simplemente contiene:</para>
<ul>
    <li>String : Type</li>
    <li>String : Description</li>
</ul>
<para><b>CookTeam</b></para>
<para>El CookTeam es responsable de cocinar la hamburguesa. CookTeam es inyectado en StockService y OrderService (ve lo siguiente). El método principal de negocio de esta clase es:</para>
<para>    public Burger prepareBurger(Map<Ingredients, Integer>, BurgerType type)</para>
<para>El CookTeam obtendrá los ingredientes usando la clase StockService. Si desafortunadamente StockService no tiene los ingredientes suficientes CookTeam recibirá una Exception (ve la descripción de StockService) y será responsable de agregar la Order necesaria en el OrderService (ve más abajo).  </para>
<para><b>StockService</b></para>
<para>StockService expone este método</para>
<para>    public void verifyAndUpdateStock(Map<Ingredient, Integer>) lanza una NotEnoughIngredientsException()</para>
<para>Por el momento la implementación interna de esta clase queda a tu consideración.</para>
<para><b>NotEnoughIngredientsException</b></para>
<para>Es una checked exception que contiene los ingrendientes faltantes, puedes obtener los ingredientes que faltan llamando al método: </para>
<para>   public Map<Ingredient, Integer> getMissingIngredient()</para>
<para><b>OrderService</b></para>
<para>OrderService recuerda que se enviará en la siguiente Order, la clase expone 2 métodos:</para>
<ul>
    <li>public void addToNextOrder(Map<Ingredient, Integer>)</li>
    <li>public void placeOrder()</li>
</ul>
<para>Por el momento la implementación se mantiene sencilla</para>
<ul>
    <li>addToNextOrder simplemente agrega a un Map interno la cantidad de ingredientes para la siguiente orden.</li>
    <li>placeOrder usa un Logger para imprimir el contenido de la orden en la salida estándar.</li>
    <li></li>
</ul>
<para>Ahora que has descubierto las clases principales codificalas y vinculalas con Spring. Puedes usar el método que prefieras (definición de beans con xml o anotaciones). </para>
<para>Cuando hayas terminado, crea una clase ejecutable (con el método public static void main(String[] args)) que obtenga el bean FastFood y prepare la orden hecha.</para>
</topic>
<topic>
<title>Agregando JPA</title>
<para>Nuevo requerimiento: queremos que agregues a nuestra aplicación soporte para base de datos usando JPA. </para>
<para><b>Ingredients</b></para>
<para>Ingredients son ahora entidades. Aunque el campo type pudo haber sido un buen candidato para la llave primaria, vamos a agregar un campo id de tipo Long que será la llave primaria.</para>
<para><b>Recipe and RecipeBook</b></para>
<para>RecipeBook ahora regresa entidades Recipe (una nueva clase) que vienen de la base de datos. La clase Recipe esta formada por:</para>
<ul>
    <li>un campo id de tipo Long</li>
    <li>un campo description de tipo String</li>
    <li>un Map<Ingredient, Integer> (ve Hibernate Extensions para obtener ayuda). Si no te sientes agusto usando Map puedes usar un Set<Ingredient></li>
</ul>
<para>Nota: El método prepareBurger() de CookTeam se debe de adaptar para que reciba una Recipe como argumento:</para>
<para>    public Burger prepareBurger(Recipe recipe)</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/12302337/fastfood-jpa-changes.jpg'/></div>
<para><b>Stock Management</b></para>
<para>El estatus del Stock ahora se almacena en la base de datos, una nueva clase llamada StockItem se debe de crear. StockItem esta formada por:</para>
<ul>
    <li>un Ingredient</li>
    <li>una quantity (Integer)</li>
</ul>
<para><b>Order Management</b></para>
<para>Orders también son guardadas en la base de datos. Crea una nueva entidad Order que contenga:</para>
<ul>
    <li>un id</li>
    <li>un set de Ordelines</li>
</ul>
<para>El objeto Orderline contiene:</para>
<ul>
    <li>un Ingredient</li>
    <li>un quantity (Integer)</li>
</ul>
<para><b>DAOs</b></para>
<para>Después de haber creado las entidades, crea los DAOs relacionados e inyectalos en los servicios correspondientes.</para>
<para>Cuando termines, prueba tu programa nuevamente.</para>
</topic>
<topic>
<title>IRSService</title>
<para>El IRS pone especial atención en nuestro FastFood y quiere estar alerta cada vez que entregamos una hamburguesa.</para>
<para>Escribe una nueva clase IRSService y un aspecto que active el método IRSService.burgerDelivered(Burger burger), este método puede llamar a un servicio remoto para realmente avisar al IRS, pero nosotros simplemente vamos a usar un Logger para imprimir algún texto en la salida estándar.</para>
</topic>
<topic>
<title>AuditService</title>
<para>Nuevo requerimiento: parece que los artículos en Stock desaparecen más rápido que las hamburguesas que se producen! Se necesita hacer auditoría. Necesitamos registrar cada acceso a SecretRecipeBook y al Stock.</para>
<para>La clase AuditDAO define un método void addAuditTrace(AuditTrace trace) que almacenará un AuditTrace en la base de datos.</para>
<para>La entidad AuditTrace esta compuesta de:</para>
<ul>
    <li>Un id de tipo Long</li>
    <li>Un enum de category (STOCK, RECIPE_BOOK, ...)</li>
    <li>Una description de tipo String</li>
    <li>Un TimeStamp para registrar cuando ocurre el evento</li>
</ul>
<para>Por otra parte, una vez que el AuditTrace haya sido registrado, no tendría sentido modificarlo. Haz la clase AuditTrace inmutable. Recuerda que el constructor no puede ser protected.</para>
<para>Usa un aspecto que le delegue a  AuditTraceDAO la posibilidad de que la auditoría se habilite o no.</para>
</topic>
</topic>
</topic>
