<topic>
<title>Spring Core Fundamentals - Workshop</title>
<para>In deze workshop ga je enkele kleine applicaties bouwen om alle zaken die je gezien hebt in de cursus in de praktijk te brengen. Op die manier ga je beter onthouden hoe alles samen werkt.</para>
<para>Voor elke applicatie maak je een nieuw Eclipse project met de geschikte structuur.</para>
<topic>
<title>Beheer van gebruikers</title>
<para>In deze eerste oefening van de workshop ga je met het Spring framework een beheerssysteem bouwen voor gebruikers en hun status.</para>
<topic>
<title>Setup</title>
<para>Start met de configuratie van je omgeving</para>
<ul>
    <li>Configureer een simpele ApplicationContext</li>
    <li>Doe de setup van een HSQLDB data source.</li>
</ul>
</topic>
<topic>
<title>Eenvoudige Persistence</title>
<para>Gebruik JdbcTemplate in een UserDAO om een DB te populeren met User objecten. Definieer voor jezelf wat je van een User wil bijhouden.</para>
<para>Optioneel: gebruik console input om Users toe te voegen. bv. <a href="http://java.sun.com/javase/6/docs/api/java/io/Console.html">Console</a> or <a href="http://java.sun.com/javase/6/docs/api/java/util/Scanner.html">Scanner</a> voor de Java 1.5+ manieren om console input te lezen.</para>
<para>Injecteer de DAO klasse in de nieuwe Service klasse.</para>
</topic>
<topic>
<title>I18N</title>
<para>Configureer i18n met een MessageSource en gebruik een i18nService om de data uit de DB te formatteren.</para>
<para>Laat de gebruiker een eigen Locale kiezen wanneer hij aangemaakt wordt.</para>
</topic>
<topic>
<title>JPA</title>
<para>Schrijf een StatusUpdate domeinklasse en een StatusUpdateDAO die een EntityManager gebruikt. Elke StatusUpdate bevat een boodschap en een referentie naar een User.</para>
<para>Migreer de UserDAO naar de EntityManager.</para>
<para>Voeg een Collection toe van status updates aan de User klasse.</para>
<para>Unit test het ophalen van een User in de UserDAO door gebruik te maken ven een stub EntityManager implementatie. cf. Martin Fowler's <a href="http://martinfowler.com/articles/mocksArentStubs.html">Mocks zijn geen Stubs</a> </para>
</topic>
<topic>
<title>AOP</title>
<para>Schrijf een loggingaspect dat elke call logt.</para>
<para>Schrijf een aspect dat een felicitatiebericht afdrukt telkens een gebruiker 10 status updates gekregen heeft.</para>
<para>Gebruik transacties om ongeldige input te verwerken (bv. verhoog de "update counter" van de User, dan laat je de insert van de status update mislukken door het throwen van een exceptie.  Check of de verhoging van de teller correct teruggedraaid werd)</para>
<para>Gebruik een @Secured annotatie dat je service alleen openstelt voor aangelogde gebruikers. Probeer het met een "prototype" scope en een BeanPostProcessor, daarna met een aspect. Je zou ook een ThreadLocal kunnen nodig hebben.</para>
<para>Optioneel: Schrijf een aspect dat de StatusUpdate naar Twitter stuurt. Zoek naar een Java library dat dit doet om je te helpen. Er bestaan er veel.</para>
</topic>
<topic>
<title>Optioneel</title>
<para>De volgende onderwerpen worden niet behandeld in de cursus maar ze zijn interessant om te doen als je tijd hebt.</para>
<para><b>@Configurable</b></para>
<para>Gebruik de @Configurable annotatie om de UserDAO kwijt te geraken en om instance methodes te hebben zoals User.save(), User.delete(), enz. en statische methodes zoals User.get( Long ), User.findByName(). cfr. <a href="http://static.springsource.org/spring/docs/2.5.x/reference/aop.html#aop-atconfigurable">Het gebruik van AspectJ om domeinobjecten te injecteren met Spring</a>.</para>
</topic>
</topic>
<topic>
<title>Fast Food</title>
<para>In deze workshopoefening gaan we een FastFood bouwen om hamburgers te produceren (lekker lekker). Er zijn 3 grote iteraties:</para>
<ul>
    <li>Schrijven en linken van de klasses</li>
    <li>Toevoegen van JPA support</li>
    <li>Toevoegen van auditing via AOP</li>
</ul>
<para>Laten we beginnen met het schrijven van de domeinobjecten.</para>
<topic>
<title>Klasses en verantwoordelijkheden</title>
<para><b>BurgerType</b></para>
<para>BurgerType is een enumeratie die de verschillende soorten hamburgers beschrijft die we maken.</para>
<para>Enum waarden zouden kunnen zijn:</para>
<ul>
    <li>FISH_FILET</li>
    <li>CHICKEN_SUPREME</li>
    <li>ROYAL_WITH_CHEESE</li>
    <li>SUPREME_POUNDER</li>
    <li>BACON_QUARTER_POUNDER</li>
</ul>
<para><b>FastFood</b></para>
<para>FastFood is een interface met 2 publieke methodes</para>
<ul>
    <li>public Burger deliverBurger( BurgerType ) : maakt een nieuwe instantie van Burger gebaseerd op het gegeven BurgerType</li>
    <li>public List<Burger> prepareOrder() : stuurt een lijst terug van Burger objecten</li>
</ul>
<para>Meer details volgen later bij de implementatie van FastFoodImpl</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/12302337/fastfood.jpg'/></div><span style='color:red;'>Format error: This tag is empty, it has no name. You can either enclose your text in between [CODE escape='true' inline='true'] and [/CODE] or else you can escape your brackets with html '&amp;#91;' and '&amp;#93;' escape characters (respectively for '[' and ']' characters).</span>
<para><b>FastFoodImpl</b></para>
<para>FastFoodImpl is de centrale klasse van onze applicatie. Het wordt geïnjecteerd met:</para>
<ul>
    <li>a SecretRecipeBook</li>
    <li>a CookTeam</li>
</ul>
<para>In de public Burger deliverBurger( BurgerType ) methode zal je:</para>
<ul>
    <li>Een lijst ophalen uit het receptenboek van de nodige ingrediënten en hun hoeveelheden</li>
    <li>Het CookTeam vragen om de bereiding te doen</li>
</ul>
<para>In de public List<Burger> prepareOrder() methode zal je een Order inlezen uit een bestand "burger.txt". Het formaat is eenvoudigweg: BURGER_TYPE aantal. Bv:</para>
<para>         FISH_FILET 1</para>
<para>         CHICKEN_SUPREME 2</para>
<para>         ROYAL_WITH_CHEESE 1</para>
<para>         SUPREME_POUNDER 5</para>
<para>         BACON_QUARTER_POUNDER 6</para>
<para>Opmerking: Gebruik de ingebouwde valueOf() methode uit je enum om de String om te zetten in een enum instantie.</para>
<para>Je delegeert ook het aanmaken van de Burger aan de methode deliverBurger( BurgerType ). Dit betekent dat je 2 calls zal hebben naar deliverBurger(), met BurgerType.CHICKEN_SUPREME als parameter, voor de lijn "CHICKEN_SUPREME 2".</para>
<para>Zorg ervoor dat je FastFoodImpl een Log4j Logger bevat om elke keer iets te loggen als er een hamburger besteld en geleverd werd.</para>
<para><b>SecretRecipeBook</b></para>
<para>Het SecretRecipeBook word gebruikt door de FastFoodImpl om te weten hoeveel en welke ingrediënten er nodig zijn voor een bepaald type hamburger:</para>
<para>        public Map<Ingredient, Integer> getIngredientsAndQuantities( BurgerType )</para>
<para>De implementatie hiervan laten we aan jou over.</para>
<para><b>Ingredient</b></para>
<para>Ingredient is een eenvoudige klasse die een ingrediënt definieert voor een hamburger. Het bevat simpelweg:</para>
<ul>
    <li>String : Type</li>
    <li>String : Description</li>
</ul>
<para><b>CookTeam</b></para>
<para>Het CookTeam is verantwoordelijk voor het maken van de hemburger. Het wordt geïnjecteerd met een StockService en een OrderService (zie verder). De belangrijkste business methode van deze klasse is:</para>
<para>    public Burger prepareBurger( Map<Ingredients, Integer>, BurgerType type )</para>
<para>Het CookTeam zal de ingrediënten ophalen via de StockService klasse. Als de StockService spijtig genoeg niet voldoende ingrediënten heeft dan krijgt de CookTeam een Exceptie (zie beschrijving van StockService).  Het is ook verantwoordelijk om een Order bij te plaatsen via de OrderService (zie verder).  </para>
<para><b>StockService</b></para>
<para>StockService bied deze methode aan</para>
<para>    public void verifyAndUpdateStock( Map<Ingredient, Integer> ) throws a NotEnoughIngredientsException()</para>
<para>Momenteel laten we de implementatie hiervan over aan jou.</para>
<para><b>NotEnoughIngredientsException</b></para>
<para>Een "checked" exceptie dat de ontbrekende ingrediënten bevat.  Je kan deze ophalen via een call  naar</para>
<para>   public Map<Ingredient, Integer> getMissingIngredient()</para>
<para><b>OrderService</b></para>
<para>De OrderService onthoud wat er in het volgende Order gaat zitten.  De klasse heeft 2 methodes:</para>
<ul>
    <li>public void addToNextOrder( Map<Ingredient, Integer> )</li>
    <li>public void placeOrder()</li>
</ul>
<para>Momenteel houden we de implementatie eenvoudig</para>
<ul>
    <li>addToNextOrder voegt simpelweg de hoeveelheid ingrediënten van het volgende Order bij in een interne Map.</li>
    <li>placeOrder gebruikt een Logger om de inhoud van het Order te loggen.</li>
    <li></li>
</ul>
<para>Nu dat je de Java code hebt voor je domeinobjecten, kan je ze aan elkaar linken via Spring.  Je kan kiezen of je hiervoor XML of annotaties gebruikt.</para>
<para>Als dit gedaan is, schrijf je een uitvoerbare klasse (met een public static void main( String[] args ) methode) die de FastFood bean ophaalt en het gevraagde order laat bereiden.</para>
</topic>
<topic>
<title>JPA toevoegen</title>
<para>Nieuwe requirement: we willen database support toevoegen in onze applicatie via JPA. </para>
<para><b>Ingredients</b></para>
<para>Ingredients worden nu "entities". Hoewel het type field best een goeie kandidaat zou zijn voor "primary key",gaan we toch een "id" field van het type Long toevoegen.</para>
<para><b>Recipe en RecipeBook</b></para>
<para>RecipeBook stuurt nu een Recipe terug (een nieuwe klasse) als entiteiten die van de database komen. De Recipe klasse bevat:</para>
<ul>
    <li>een "id" field van het type Long</li>
    <li>een "description" field van het type String</li>
    <li>een Map<Ingredient, Integer> (zie "Hibernate Extensions" voor extra informatie). Als je je niet gelukkig voelt met het mappen van een Map, gebruik dan een Set<Ingredient> in de plaats</li>
</ul>
<para>Opmerking: De CookTeam's prepareBurger() methode wordt aangepast om een Recipe als parameter binnen te krijgen:</para>
<para>    public Burger prepareBurger( Recipe recipe )</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/12302337/fastfood-jpa-changes.jpg'/></div>
<para><b>Stock Management</b></para>
<para>De Stock status zit nu ook in de database.  Schrijf een nieuwe klasse StockItem. StockItem bevat:</para>
<ul>
    <li>een Ingredient</li>
    <li>een hoeveelheid (Integer)</li>
</ul>
<para><b>Order Management</b></para>
<para>Orders worden opgeslagen in de database. Schrijf een nieuwe Order entiteit die de volgende zaken bevat:</para>
<ul>
    <li>een id</li>
    <li>een set van Ordelines</li>
</ul>
<para>Een Orderline object bevat</para>
<ul>
    <li>een Ingredient</li>
    <li>een hoeveelheid (Integer)</li>
</ul>
<para><b>DAOs</b></para>
<para>Na het schrijven van de entiteiten, schrijf je de overeenkomstige DAOs en injecteer je ze in de gepaste services.</para>
<para>Test je programma opnieuw, nadat je dit hebt gedaan.</para>
</topic>
<topic>
<title>IRSService</title>
<para>De IRS (belastingsdiensten) letten zeer nauwkeurig op bij FastFood en willen een signaal krijgen voor elke levering van een hamburger.</para>
<para>Schrijf een nieuwe IRSService klasse en een aspect dat de methode IRSService.burgerDelivered( Burger burger ) oproept.  Deze method zou een "remote" service kunnen oproepen om de belastingsdiensten te waarschuwen maar wij gaan eenvoudig een Logger gebruiken om een lijntje af te drukken op de console.</para>
</topic>
<topic>
<title>AuditService</title>
<para>Nieuwe requirement: Het lijkt erop dat de Stock items sneller verdwijnen dan dat er hamburgers geproduceerd worden! Er is wat auditing nodig. We moeten alle access naar het SecretRecipeBook en de Stock loggen.</para>
<para>De AuditDAO klasse heeft een void addAuditTrace( AuditTrace trace ) methode die een AuditTrace gaat opslaan in de database.</para>
<para>Een AuditTrace entiteit bevat</para>
<ul>
    <li>Een id van het type Long</li>
    <li>Een category enum (STOCK, RECIPE_BOOK, ...)</li>
    <li>Een omschrijving van het type String</li>
    <li>Een TimeStamp om op te slaan wanneer het event zich voordeed</li>
</ul>
<para>Bovendien, als een AuditTrace opgeslagen is, heeft het geen zin meer om die aan te passen. Maak de AuditTrace klasse immutable. Herinner je dat constructors protected kunnen zijn.</para>
<para>Gebruik een aspect dat de AuditTraceDAO gebruikt om ervoor te zorgen dat je de tracing eenvoudig kan op- en afzetten.</para>
</topic>
</topic>
</topic>
