<topic>
<title>Grails Fundamentals</title>
<topic>
<title>Introduction</title>
<para>Grails main idea is to make web development easier and more productive using the same tools and frameworks that are available in Java platform. It takes its concepts from dynamic frameworks like Rails, Django and TurboGears which helped pave the way to a more modern way of thinking about web applications.</para>
<para>When starting a new web development project using in Java, you usually have to choose from variety from frameworks (like ORM, service, UI etc.). Then you have to provide quite a bit of configuration so each would work properly. And only then if don't run in some problems on the way (which usually you do), coding can be started. </para>
<para>In contrast Grails tries to avoid these missteps by providing everything out of the box. Configuration is minimal and it is already providing templates in places where it is needed and if it can be avoided it uses conventions instead (convention over configuration design paradigm). Also in case you need some extras it has its own plugin system which helps easily add/remove additional modules just by typing one command line. Besides that Grails provides scaffolding feature which is generating a fully working page for your entities, by just typing one line of code. Below we'll outline an overview of Grails architecture.</para>
<topic>
<title>Architecture Overview</title>
<para>Grails is a full stack framework and attempts to solve as many pieces of the web development puzzle through the core technology and its associated plugins. Included out the box are things like:</para>
<ul>
    <li>An easy to use Object Relational Mapping (ORM) layer built on Hibernate</li>
    <li>An expressive view technology called Groovy Server Pages (GSP)</li>
    <li>A controller layer built on Spring MVC</li>
    <li>A command line scripting environment built on the Groovy-powered Gant</li>
    <li>An embedded Tomcat container which is configured for on the fly reloading</li>
    <li>Dependency injection with the inbuilt Spring container</li>
    <li>Support for internationalization (i18n) built on Spring's core MessageSource concept</li>
    <li>A transactional service layer built on Spring's transaction abstraction</li>
</ul>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/15804411/architecture_grails.png'/></div>
<para><br/>
All of these are made easy to use through the power of the Groovy language and the extensive use of Domain Specific Languages (DSLs).</para>
<para>Later in the course we'll see how each part works in different areas of the framework. Most noticeably you'll see groovy code which is even used for configurations. GSP view code will look pretty familiar if you had experience with JSP or other web frameworks (JSF, Tapestry etc.). One thing which is not common for traditional web projects is grails command line unless you tried Spring Roo. This is a single entry point for running application or tests,  creating domain, view, controller templates, generating deployment packages and other things.</para>
</topic>
<topic>
<title>Grails vs Vaadin</title>
<para>Before comparing something it is good to have a solid definition. We defined grails architecture before. Here we'll define briefly how Vaadin framework works and compare it to Grails. Thought they could be used in sync, originally Vaadin was designed to be used as Java framework.</para>
<para>Vaadin uses GWT in a way that it has a set of components precompiled in GWT. The framework is server driven, so all logic is handled on the server side. UI code is written in Java in similar fashion that you would write a desktop application code (e.g. Swing).</para>
<para>The components has two parts, client and server file. The client side is just a dummy "view" for the component. Once you interact with it, it sends a message to the server that this or that was pressed/written/etc. The server then decides what should be done.</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/15804411/architecture-detailed-lo.png'/></div>
<para><br/>
When comparing both frameworks first we need to take into account for which purpose we intend to use them for. </para>
<para>Grails out of the box supports “traditional” web development. It has an predefined MVC model where UI code is separated in GSP files which correspond to controller files written in groovy language.</para>
<para>Vaadin is more suited for desktop like applications on the web. It does requests in the background without refreshing the whole page. Because most of components are JavaScript based on client side, they offer more dynamic behavior then traditional HTML pages.</para>
<para>Below we outline some pros/cons on both:</para>
<para><b>Grails</b></para>
<para>pros</para>
<ul>
    <li>REST support</li>
    <li>Separation of UI/server code</li>
    <li>Templating support</li>
    <li>Integrated with Spring/Hibernate</li>
    <li>Scaffolding</li>
    <li>No recompiling for view changes</li>
</ul>
<para>cons</para>
<ul>
    <li>Out of the box doesn't support interactive applications</li>
    <li>Whole page needs to be refreshed</li>
    <li>No designer for UI code</li>
    <li>Harder to debug due to dynamic behaviour</li>
</ul>
<para><b>Vaadin</b></para>
<para>pros</para>
<ul>
    <li>Offers tools for developing interactive applications</li>
    <li>Handles client side without needing to write additional HTML/JavaScript code</li>
    <li>Lots of components to choose from</li>
    <li>Designer available for UI code generation</li>
</ul>
<para>cons</para>
<ul>
    <li>Bigger code base to support, due to the nature of how desktop like Java UI code is written</li>
    <li>Recompiling is needed when changing UI code</li>
    <li>No navigation support</li>
</ul>
<para>In the end it depends what is the goal of application you are building. As mentioned before Vaadin can be used with Grails using plugin available here http://grails.org/plugin/vaadin. This makes sense when you want to get more interactive applications and still have all the benefits that Grails has to offer (dynamic nature, other framework integration, auto recompiling, plugins etc.)</para>
</topic>
<topic>
<title>Prerequisites</title>
<para>Before installing Grails you will need as a minimum a Java Development Kit (JDK) installed version 1.6 or above. For instructions read further on how to download and setup it.</para>
<para>As Grails runs on top of JVM it is compatible with all major OS's:</para>
<ul>
    <li>Windows</li>
    <li>Linux</li>
    <li>Mac OS X</li>   
</ul>
</topic>
</topic>
<topic>
<title>Getting Started</title>
<topic>
<title>Command Line</title>
<topic>
<title>Downloading and Installing</title>
<para>Grails application can be created with no extra tool support other than a command line shell and your favourite editor.</para>
<para>Before installing grails you must have installed Java. Sun/Oracle Java 1.6+ is recommended, however any Java system conforming to Sun Java 1.5 or higher should work. See the Java fundamentals course for details on intalling Java.</para>
<para>The grails distribution is obtained from http://grails.org/Download. First you need to unpack the Grails distribution into an appropriate directory such as “~/grails” for unix/linux users or c:\grails for windows users. Then you must add an environment variable “GRAILS_HOME” that points to the directory where you installed Grails. Finally you should update your PATH to include GRAILS_HOME/bin.</para>
<para>Now, from the command line, you should be able to run the command “grails”. If the output looks like the picture below you have installed Grails correctly. For more information on installing Grails you can refer to the installation instructions on the Grails web-site  at http://grails.org/Installation.</para>
<div style='overflow:auto' align='center'><img align='middle' src='/gen/course/15804411/grails.png'/></div>
</topic>
<topic>
<title>Hello World</title>
<topic>
<title>Creating an Application</title>
<para>Grails applications can be created with the "grails" command-line tool using create-app and the name of the new application as arguments.</para>
<para>Example:</para>
<para><code type='display'>grails create-app appName</para>
<para></code></para>
<para>The example above will create a new application with the name "appName" in the directory "appName". </para>
</topic>
<topic>
<title>Directory Structure</title>
</topic>
<topic>
<title>Running an Application</title>
</topic>
<topic>
<title>Testing an Application</title>
</topic>
<topic>
<title>Deploying an Application</title>
</topic>
</topic>
</topic>
<topic>
<title>STS IDE</title>
<topic>
<title>Downloading and Installing</title>
</topic>
<topic>
<title>Hello World</title>
<topic>
<title>Creating an Application</title>
</topic>
<topic>
<title>Directory Structure</title>
</topic>
<topic>
<title>Running an Application</title>
</topic>
<topic>
<title>Testing an Application</title>
</topic>
<topic>
<title>Deploying an Application</title>
</topic>
</topic>
</topic>
</topic>
<topic>
<title>Course Project</title>
</topic>
<topic>
<title>Understanding Domains</title>
<topic>
<title>Creating a Domain</title>
</topic>
<topic>
<title>Scaffolding</title>
</topic>
<topic>
<title>Constraints</title>
</topic>
<topic>
<title>CRUD</title>
</topic>
<topic>
<title>Relationships</title>
</topic>
<topic>
<title>Chapter Exercise</title>
</topic>
</topic>
<topic>
<title>Understanding Controllers</title>
<topic>
<title>Creating a Controller</title>
</topic>
<topic>
<title>Scaffolding</title>
</topic>
<topic>
<title>Actions</title>
</topic>
<topic>
<title>CRUD</title>
</topic>
<topic>
<title>Render vs Redirect</title>
</topic>
<topic>
<title>Chapter Exercise</title>
</topic>
</topic>
<topic>
<title>Understanding Views</title>
<topic>
<title>Creating a View</title>
</topic>
<topic>
<title>Scaffolding</title>
</topic>
<topic>
<title>CRUD</title>
</topic>
<topic>
<title>Built-in Tags</title>
</topic>
<topic>
<title>Pre-defined Variables</title>
</topic>
<topic>
<title>Chapter Exercise</title>
</topic>
</topic>
<topic>
<title>Command Line Reference</title>
</topic>
<topic>
<title>Solutions</title>
</topic>
</topic>
